﻿using System;
using System.Linq;
using System.IO;

using Bogus;

using Dermoskop.Database.Models;
using Dermoskop.Database.Enums;

namespace Dermoskop.Database
{
    public class DatabaseInjector
    {
        private static Faker<dbPatient> PatientFaker = new Faker<dbPatient>()
            .RuleFor(o => o.Name, f => f.Name.FirstName())
            .RuleFor(o => o.Surname, f => f.Name.LastName())
            .RuleFor(o => o.BirthDate, f => f.Date.Past())
            .RuleFor(o => o.Sex, f => f.PickRandomWithout<Sexes>(Sexes.None))
            .RuleFor(o => o.HealthInsuranceNumber, f => f.Random.Int(min: 0).ToString());
        private static Faker<dbUser> UserFaker = new Faker<dbUser>()
            .RuleFor(o => o.Name, f => f.Name.FirstName())
            .RuleFor(o => o.Surname, f => f.Name.LastName())
            .RuleFor(o => o.Username, (f, u) => f.Internet.UserName(u.Name, u.Surname))
            .RuleFor(o => o.Password, f => f.Internet.Password());
        private static Faker<dbExamination> ExFaker = new Faker<dbExamination>()
            .RuleFor(o => o.ExaminationTime, f => f.Date.Past())
            .RuleFor(o => o.Comment, f => f.Lorem.Sentences(f.Random.Int(1, 4)));
        private static Faker<dbImage> ImageFaker = new Faker<dbImage>()
            .RuleFor(o => o.FileName, f => f.System.FileName(".png"));
        private static Faker<dbBirthmark> BirthmarkFaker = new Faker<dbBirthmark>()
            .RuleFor(o => o.FirstBirthmarkTime, f => f.Date.Past())
            .RuleFor(o => o.yLocation, f => f.Random.Int(0, 300))
            .RuleFor(o => o.xLocation, f => f.Random.Int(0, 300));
        private static Faker<dbBirthmarkImage> BirthmarkImageFaker = new Faker<dbBirthmarkImage>()
            .RuleFor(o => o.BirthmarkTime, f => f.Date.Past())
            .RuleFor(o => o.Comment, f => f.Lorem.Sentences(f.Random.Int(1, 2)))
            .RuleFor(o => o.FileName, f => f.System.FileName(".png"));

        private static string[] positions_array = Enum.GetNames(typeof(ImagePositions));
        private static string[] image_names = null; // just provisional to mimic the behaviour of the application in later stages

        /// <summary>
        /// Creates fake instances in the database using Bogus library. For each examination all positional images are created.
        /// </summary>
        /// <param name="patientCount">Number of patient instances you want to insert.</param>
        /// <param name="exemCount">Number of examination instances you want to insert.</param>
        /// <param name="userCount">Number of user instances you want to insert.</param>
        public static void InjectData(int patientCount = 3, int exemCount = 3, int userCount = 1, int birthmarkCount = 2, int birthmarkImageCount = 2)
        {
            // this is just provisional, look at image_names initialization line
            if (Directory.Exists("./Images/"))
            {
                image_names = Directory.GetFiles("./Images/");
                for (int i = 0; i < image_names.Length; i++)
                    image_names[i] = Path.GetFileName(image_names[i]);
            }
            
            if ((image_names?.Length ?? 0) == 0)
                image_names = new string[] { "" };
            
            var random = new Randomizer();
            for(int i = 0; i < userCount; i++)
            {
                dbUser u = UserFaker.Generate();
                DatabaseConnector.Insert(u);
            }
            long[] userIds = DatabaseConnector.Query<dbUser>().Select(x => x.Id).ToArray();

            for (int patientId = 0; patientId < patientCount; patientId++)
            {
                // generate a fake patient
                dbPatient patient = PatientFaker.Generate();
                //insert it into database
                DatabaseConnector.Insert(patient);
                //generate examinations for the patient
                for(int exemId = 0; exemId < exemCount; exemId++)
                {
                    // create new examination
                    dbExamination examination = ExFaker.Generate();
                    examination.PatientId = patient.Id;
                    examination.UserId = userIds[random.Int(0, userIds.Length - 1)]; // choose a random user
                    //insert it to database
                    DatabaseConnector.Insert(examination);

                    for(int i = 1; i < positions_array.Length; i++)
                    {
                        dbImage image = ImageFaker.Generate();
                        image.ExaminationId = examination.Id;
                        image.FileName = image_names[random.Int(0, image_names.Length - 1)];
                        image.Position = (ImagePositions) Enum.Parse(typeof(ImagePositions), positions_array[i]);
                        DatabaseConnector.Insert(image);
                    }
                }
                //generate birthmarks for patients
                for (int birthmarkId = 0; birthmarkId < birthmarkCount; birthmarkId++)
                {
                    for (int i = 1; i < positions_array.Length; i++)
                    {
                        // create new birthmarks
                        dbBirthmark birthmark = BirthmarkFaker.Generate();
                        birthmark.PatientId = patient.Id;
                        birthmark.Position = (ImagePositions) Enum.Parse(typeof(ImagePositions), positions_array[i]);

                        //insert it to database
                        DatabaseConnector.Insert(birthmark);

                        for (int birthmarkImageId = 0; birthmarkImageId < birthmarkImageCount; birthmarkImageId++)
                        {
                            dbBirthmarkImage birthmarkImage = BirthmarkImageFaker.Generate();
                            birthmarkImage.BirthmarkId = birthmark.Id;
                            birthmarkImage.FileName = image_names[random.Int(0, image_names.Length - 1)];
                            DatabaseConnector.Insert(birthmarkImage);
                        }
                    }
                }
            }
        }
    }
}
