﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dermoskop.Database.Enums
{
    public enum Sexes
    {
        None = 0,
        M = 1,
        F = 2
    }
}
