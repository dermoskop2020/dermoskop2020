﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dermoskop.Database.Enums
{
    public enum ImagePositions
    {
        None = 0,
        Front = 1,
        Left = 2,
        Back = 3,
        Right = 4,
    }
}
