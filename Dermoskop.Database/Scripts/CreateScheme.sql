﻿CREATE TABLE IF NOT EXISTS [Patients] (
	[Id] INTEGER NOT NULL,
	[Name] TEXT(50),
	[Surname] TEXT(50),
	[Sex] TEXT(1),
	[BirthDate]	TEXT(30),
	[HealthInsuranceNumber]	TEXT,
	CONSTRAINT [PK_Patients] PRIMARY KEY ([Id])
);

CREATE TABLE IF NOT EXISTS [Users] (
	[Id] INTEGER NOT NULL,
	[Username] Text,
	[Password] Text,
	[Name] TEXT(50),
	[Surname] TEXT(50),
	CONSTRAINT [PK_Users] PRIMARY KEY ([Id])
);

CREATE TABLE IF NOT EXISTS [Examinations] (
	[Id] INTEGER NOT NULL,
	[PatientId] INTEGER NOT NULL,
	[UserId] INTEGER NOT NULL,
	[DateTime] TEXT(30),
	[Comment] TEXT,
	CONSTRAINT [PK_Examinations] PRIMARY KEY ([Id]),
	FOREIGN KEY ([PatientId]) REFERENCES [Patients]([Id]),
	FOREIGN KEY ([UserId]) REFERENCES [Users]([Id])
);

CREATE TABLE IF NOT EXISTS [Images] (
	[Id] INTEGER NOT NULL,
	[ExaminationId] INTEGER NOT NULL,
	[FileName] TEXT,
	[Position] TEXT(20),
	CONSTRAINT [PK_Images] PRIMARY KEY ([Id]),
	FOREIGN KEY ([Position]) REFERENCES [ImagePositions]([Name]),
	FOREIGN KEY ([ExaminationId]) REFERENCES [Examinations]([Id])
);

CREATE TABLE IF NOT EXISTS [ImagePositions] (
	[Name] TEXT(20),
	CONSTRAINT [unq_ImagePosition_name] UNIQUE ([Name])
);

INSERT OR IGNORE INTO [ImagePositions]([Name]) VALUES
		('Front'),
		('Back'),
		('Right'),
		('Left');


CREATE TABLE IF NOT EXISTS [Birthmarks] (
	[Id] INTEGER NOT NULL,
	[PatientId] INTEGER NOT NULL,
	[Position] TEXT(20),
	[xLocation] INTEGER,
	[yLocation] INTEGER,
	[DateTime] TEXT(30),
	CONSTRAINT [PK_Birthmarks] PRIMARY KEY ([Id]),
	FOREIGN KEY ([Position]) REFERENCES [ImagePositions]([Name]),
	FOREIGN KEY ([PatientId]) REFERENCES [Patients]([Id])

);

CREATE TABLE IF NOT EXISTS [BirthmarkImages] (
	[Id] INTEGER NOT NULL,
	[BirthmarkId] INTEGER NOT NULL,
	[FileName] TEXT,
	[Comment] TEXT,
	[DateTime] TEXT(30),
	CONSTRAINT [PK_Images] PRIMARY KEY ([Id]),
	FOREIGN KEY ([BirthmarkId]) REFERENCES [Examinations]([Id])
);