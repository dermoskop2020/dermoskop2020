﻿using System;
using System.Collections.Generic;
using System.Linq;

using System.Data.Linq.Mapping;

using Dermoskop.Database.Enums;

namespace Dermoskop.Database.Models
{
    [Table(Name = "Images")]
    public class dbImage : DatabaseClass
    {
        #region Columns
        
        [Column(Name = "Id", IsPrimaryKey = true)]
        public override long Id { get; set; }

        [Column(Name = "ExaminationId")]
        public long ExaminationId { get; set; }

        [Column(Name = "FileName")]
        public string FileName { get; set; }

        [Column(Name = "Position")]
        private string _position { get; set; }

        #endregion

        #region Custom Get Functions

        public ImagePositions Position
        {
            get
            {
                ImagePositions.TryParse(_position, true, out ImagePositions parsedFormat);
                return parsedFormat;
            }
            set
            {
                this._position = value.ToString();
            }
        }

        #endregion

        #region Override Functions

        public override (string, List<(string, string)>) Insert()
        {
            string sql_command = "INSERT INTO [Images] (ExaminationId, FileName, Position) VALUES (@ExaminationId, @FileName, @Position); SELECT last_insert_rowid();";

            var parameters = new List<(string, string)>
            {
                ("@ExaminationId", ExaminationId.ToString()), ("@FileName", FileName), ("@Position", _position)
            };

            return (sql_command, parameters);
        }

        public override (string, List<(string, string)>) Update()
        {
            string sql_command = "UPDATE [Images] SET ExaminationId=@ExaminationId, FileName=@FileName, Position=@Position WHERE Id=@Id";

            var parameters = new List<(string, string)>
            {
                ("@Id", Id.ToString()), ("@ExaminationId", ExaminationId.ToString()), ("@FileName", FileName), ("@Position", _position)
            };

            return (sql_command, parameters);
        }

        public override (string, List<(string, string)>) Delete()
        {
            string sql_command = "DELETE FROM [Images] WHERE Id=@Id";

            var parameters = new List<(string, string)>
            {
                ("@Id", Id.ToString())
            };

            return (sql_command, parameters);
        }

        #endregion

        #region Constructor Methods

        public dbImage()
        {
            this.ExaminationId = -1;
            this.FileName = null;
            this.Position = ImagePositions.None;
        }   //.Generate() requires a "parameterless" constructor

        public dbImage(long ExaminationId = -1, string FileName = null, ImagePositions Position = ImagePositions.None)
        {
            this.ExaminationId = ExaminationId;
            this.FileName = FileName;
            this.Position = Position;
        }

        #endregion
    }
}
