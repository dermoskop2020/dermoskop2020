﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;

using System.Data.Linq.Mapping;

using Dermoskop.Database.Enums;

namespace Dermoskop.Database.Models
{
    [Table(Name = "BirthmarkImages")]
    public class dbBirthmarkImage : DatabaseClass
    {
        #region Columns

        [Column(Name = "Id", IsPrimaryKey = true)]
        public override long Id { get; set; }

        [Column(Name = "BirthmarkId")]
        public long BirthmarkId { get; set; }

        [Column(Name = "FileName")]
        public string FileName { get; set; }

        [Column(Name = "Comment")]
        public string Comment { get; set; }

        [Column(Name = "DateTime")]
        private string _dateTime { get; set; }

        #endregion

        #region Custom Get Functions
        public DateTime? BirthmarkTime
        {
            get
            {
                DateTime.TryParseExact(_dateTime, DateTimePattern, null, DateTimeStyles.None, out DateTime parsedFormat);
                return parsedFormat;
            }
            set
            {
                this._dateTime = value?.ToString(DateTimePattern) ?? string.Empty;
            }
        }


        #endregion

        #region Override Functions

        public override (string, List<(string, string)>) Insert()
        {
            string sql_command = "INSERT INTO [BirthmarkImages] (BirthmarkId, FileName, Comment, DateTime) VALUES (@BirthmarkId, @FileName, @Comment, @DateTime); SELECT last_insert_rowid();";

            var parameters = new List<(string, string)>
            {
                ("@BirthmarkId", BirthmarkId.ToString()), ("@FileName", FileName), ("@Comment", Comment), ("@DateTime", _dateTime)
            };

            return (sql_command, parameters);
        }

        public override (string, List<(string, string)>) Update()
        {
            string sql_command = "UPDATE [BirthmarkImages] SET BirthmarkId=@BirthmarkId, FileName=@FileName, Comment=@Comment, DateTime=@DateTime  WHERE Id=@Id";

            var parameters = new List<(string, string)>
            {
                ("@Id", Id.ToString()), ("@BirthmarkId", BirthmarkId.ToString()), ("@FileName", FileName), ("@Comment", Comment), ("@DateTime", _dateTime)
            };

            return (sql_command, parameters);
        }

        public override (string, List<(string, string)>) Delete()
        {
            string sql_command = "DELETE FROM [BirthmarkImages] WHERE Id=@Id";

            var parameters = new List<(string, string)>
            {
                ("@Id", Id.ToString())
            };

            return (sql_command, parameters);
        }

        #endregion

        #region Constructor Methods

        public dbBirthmarkImage()
        {
            this.BirthmarkId = -1;
            this.FileName = null;
            this.Comment = null;
            this.BirthmarkTime = null;
        }   //.Generate() requires a "parameterless" constructor

        public dbBirthmarkImage(long BirthmarkId = -1, string FileName = null, string Comment = null, DateTime? BirthmarkTime = null)
        {
            this.BirthmarkId = BirthmarkId;
            this.FileName = FileName;
            this.Comment = Comment;
            this.BirthmarkTime = BirthmarkTime;
        }

        #endregion
    }
}
