﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Linq.Mapping;

namespace Dermoskop.Database.Models
{
    [Table(Name = "Users")]
    public class dbUser : DatabaseClass
    {
        #region Columns

        [Column(Name = "Id", IsPrimaryKey = true, IsDbGenerated = true, AutoSync = AutoSync.OnInsert)]
        public override long Id { get; set; }

        [Column(Name = "Username")]
        public string Username { get; set; }

        [Column(Name = "Password")]
        public string Password { get; set; }

        [Column(Name = "Name")]
        public string Name { get; set; }

        [Column(Name = "Surname")]
        public string Surname { get; set; }

        #endregion

        #region Custom Get Functions
        
        public string FullName
        {
            get
            {
                if (this.Name != null && this.Surname != null)
                    return $"{this.Name} {this.Surname}";
                return string.Empty;
            }
        }

        #endregion

        #region Override Functions

        public override (string, List<(string, string)>) Insert()
        {
            string sql_command = "INSERT INTO [Users] (Username, Password, Name, Surname) VALUES (@Username, @Password, @Name, @Surname); SELECT last_insert_rowid();";

            var parameters = new List<(string, string)>
            {
                ("@Username", Username), ("@Password", Password), ("@Name", Name), ("@Surname", Surname)
            };

            return (sql_command, parameters);
        }

        public override (string, List<(string, string)>) Update()
        {
            string sql_command = "UPDATE [Users] SET Username=@Username, Password=@Password, Name=@Name, Surname=@Surname WHERE Id=@Id";

            var parameters = new List<(string, string)>
            {
                ("@Id", Id.ToString()), ("@Username", Username), ("@Password", Password), ("@Name", Name), ("@Surname", Surname)
            };

            return (sql_command, parameters);
        }

        public override (string, List<(string, string)>) Delete()
        {
            string sql_command = "DELETE FROM [Users] WHERE Id=@Id";

            var parameters = new List<(string, string)>
            {
                ("@Id", Id.ToString())
            };

            return (sql_command, parameters);
        }

        #endregion

        #region Constructor Methods

        public dbUser()
        {
            this.Username = null;
            this.Password = null;
            this.Name = null;
            this.Surname = null;
        }   //.Generate() requires a "parameterless" constructor

        public dbUser(string Username = null, string Password = null, string Name = null, string Surname = null)
        {
            this.Username = Username;
            this.Password = Password;
            this.Name = Name;
            this.Surname = Surname;
        }

        #endregion
    }
}
