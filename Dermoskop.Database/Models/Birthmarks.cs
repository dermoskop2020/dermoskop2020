﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;

using System.Data.Linq.Mapping;

using Dermoskop.Database.Enums;


namespace Dermoskop.Database.Models
{
    [Table(Name = "Birthmarks")]
    public class dbBirthmark : DatabaseClass
    {
        #region Columns

        [Column(Name = "Id", IsPrimaryKey = true)]
        public override long Id { get; set; }

        [Column(Name = "PatientId")]
        public long PatientId { get; set; }

        [Column(Name = "Position")]
        private string _position { get; set; }

        [Column(Name = "xLocation")]
        public int xLocation { get; set; }

        [Column(Name = "yLocation")]
        public int yLocation { get; set; }

        [Column(Name = "DateTime")]
        private string _dateTime { get; set; }

        #endregion

        #region Custom Get Functions

        public DateTime? FirstBirthmarkTime
        {
            get
            {
                DateTime.TryParseExact(_dateTime, DateTimePattern, null, DateTimeStyles.None, out DateTime parsedFormat);
                return parsedFormat;
            }
            set
            {
                this._dateTime = value?.ToString(DateTimePattern) ?? string.Empty;
            }
        }
        public ImagePositions Position
        {
            get
            {
                ImagePositions.TryParse(_position, true, out ImagePositions parsedFormat);
                return parsedFormat;
            }
            set
            {
                this._position = value.ToString();
            }
        }
        #endregion

        #region Override Functions

        public override (string, List<(string, string)>) Insert()
        {
            string sql_command = "INSERT INTO [Birthmarks] (PatientId, Position, xLocation, yLocation, DateTime) VALUES (@PatientId, @Position, @xLocation, @yLocation, @DateTime); SELECT last_insert_rowid();";

            var parameters = new List<(string, string)>
            {
                ("@PatientId", PatientId.ToString()), ("@Position", _position), ("@xLocation", xLocation.ToString()), ("@yLocation", yLocation.ToString()), ("@DateTime", _dateTime)
            };

            return (sql_command, parameters);
        }

        public override (string, List<(string, string)>) Update()
        {
            string sql_command = "UPDATE [Birthmarks] SET PatientId=@PatientId, Position=@Position, xLocation=@xLocation, yLocation=@yLocation, DateTime=@DateTime WHERE Id=@Id";

            var parameters = new List<(string, string)>
            {
                ("@Id", Id.ToString()), ("@PatientId", PatientId.ToString()), ("@Position", _position), ("@xLocation", xLocation.ToString()), ("@yLocation", yLocation.ToString()), ("@DateTime", _dateTime)
            };

            return (sql_command, parameters);
        }

        public override (string, List<(string, string)>) Delete()
        {
            string sql_command = "DELETE FROM [Birthmarks] WHERE Id=@Id";

            var parameters = new List<(string, string)>
            {
                ("@Id", Id.ToString())
            };

            return (sql_command, parameters);
        }

        #endregion

        #region Constructor Methods
        public dbBirthmark()
        {
            this.PatientId = -1;
            this.Position = ImagePositions.None;
            this.xLocation = -1;
            this.yLocation = -1;
            this.FirstBirthmarkTime = null;
        }   //.Generate() requires a "parameterless" constructor

        public dbBirthmark(long PatientId = -1, ImagePositions Position = ImagePositions.None, int xLocation = -1, int yLocation = -1, DateTime? FirstBirthmarkTime = null)
        {
            this.PatientId = PatientId;
            this.Position = Position;
            this.xLocation = xLocation;
            this.yLocation = yLocation;
            this.FirstBirthmarkTime = FirstBirthmarkTime;
        }

        #endregion

    }
}

