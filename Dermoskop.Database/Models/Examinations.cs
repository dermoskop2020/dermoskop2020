﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;

using System.Data.Linq.Mapping;

namespace Dermoskop.Database.Models
{
    [Table(Name = "Examinations")]
    public class dbExamination : DatabaseClass
    {
        #region Columns
        
        [Column(Name = "Id", IsPrimaryKey = true)]
        public override long Id { get; set; }

        [Column(Name = "PatientId")]
        public long PatientId { get; set; }

        [Column(Name = "UserId")]
        public long UserId { get; set; }

        [Column(Name = "DateTime")]
        private string _dateTime { get; set; }

        [Column(Name = "Comment")]
        public string Comment { get; set; }

        #endregion

        #region Custom Get Functions

        public DateTime? ExaminationTime
        {
            get
            {
                DateTime.TryParseExact(_dateTime, DateTimePattern, null, DateTimeStyles.None, out DateTime parsedFormat);
                return parsedFormat;
            }
            set
            {
                this._dateTime = value?.ToString(DateTimePattern) ?? string.Empty;
            }
        }

        #endregion

        #region Override Functions

        public override (string, List<(string, string)>) Insert()
        {
            string sql_command = "INSERT INTO [Examinations] (PatientId, UserId, DateTime, Comment) VALUES (@PatientId, @UserId, @DateTime, @Comment); SELECT last_insert_rowid();";

            var parameters = new List<(string, string)>
            {
                ("@PatientId", PatientId.ToString()), ("@UserId", UserId.ToString()), ("@DateTime", _dateTime), ("@Comment", Comment)
            };

            return (sql_command, parameters);
        }

        public override (string, List<(string, string)>) Update()
        {
            string sql_command = "UPDATE [Examinations] SET PatientId=@PatientId, UserId=@UserId, DateTime=@DateTime, Comment=@Comment WHERE Id=@Id";

            var parameters = new List<(string, string)>
            {
                ("@Id", Id.ToString()), ("@PatientId", PatientId.ToString()), ("@UserId", UserId.ToString()), ("@DateTime", _dateTime), ("@Comment", Comment)
            };

            return (sql_command, parameters);
        }

        public override (string, List<(string, string)>) Delete()
        {
            string sql_command = "DELETE FROM [Examinations] WHERE Id=@Id";

            var parameters = new List<(string, string)>
            {
                ("@Id", Id.ToString())
            };

            return (sql_command, parameters);
        }

        #endregion

        #region Constructor Methods

        public dbExamination() 
        {
            this.PatientId = -1;
            this.UserId = -1;
            this.ExaminationTime = null;
            this.Comment = null;
        }   //.Generate() requires a "parameterless" constructor

        public dbExamination(long PatientId = -1, long UserId = -1, DateTime? ExaminationTime = null, string Comment = null)
        {
            this.PatientId = PatientId;
            this.UserId = UserId;
            this.ExaminationTime = ExaminationTime;
            this.Comment = Comment;
        }

        #endregion
    }
}
