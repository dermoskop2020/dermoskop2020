﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

using System.Data.Linq.Mapping;

using Dermoskop.Database.Enums;

namespace Dermoskop.Database.Models
{
    [Table(Name = "Patients")]
    public class dbPatient : DatabaseClass
    {
        #region Columns

        [Column(Name = "Id", IsPrimaryKey = true)]
        public override long Id { get; set; }

        [Column(Name = "Name")]
        public string Name { get; set; }

        [Column(Name = "Surname")]
        public string Surname { get; set; }

        [Column(Name = "Sex")]
        private string _sex { get; set; }

        [Column(Name = "BirthDate")]
        private string _birthDate { get; set; }

        [Column(Name = "HealthInsuranceNumber")]
        public string HealthInsuranceNumber { get; set; }

        #endregion

        #region Custom Get Functions

        public DateTime? BirthDate
        {
            get
            {
                DateTime.TryParseExact(_birthDate, DateTimePattern, null, DateTimeStyles.None, out DateTime parsedFormat);
                return parsedFormat;
            }
            set
            {
                this._birthDate = value?.ToString(DateTimePattern) ?? string.Empty;
            }
        }
        
        public string FullName
        {
            get
            {
                if (this.Name != null && this.Surname != null)
                    return $"{this.Name} {this.Surname}";
                return string.Empty;
            }
        }

        public Sexes Sex
        {
            get
            {
                Sexes.TryParse(_sex, true, out Sexes parsedFormat);
                return parsedFormat;
            }
            set
            {
                this._sex = value.ToString();
            }
        }

        public int Age
        {
            get
            {
                int now = int.Parse(DateTime.Now.ToString("yyyyMMdd"));
                int dob = int.Parse(BirthDate?.ToString("yyyyMMdd") ?? string.Empty);
                return (now - dob) / 10000;
            }
        }

        #endregion

        #region Override Functions

        public override (string, List<(string, string)>) Insert()
        {
            string sql_command = "INSERT INTO [Patients] (Name, Surname, Sex, BirthDate, HealthInsuranceNumber) VALUES (@Name, @Surname, @Sex, @BirthDate, @HealthInsuranceNumber); SELECT last_insert_rowid();";

            var parameters = new List<(string, string)>
            {
                ("@Name", Name), ("@Surname", Surname), ("@Sex", _sex), ("@BirthDate", _birthDate), ("@HealthInsuranceNumber", HealthInsuranceNumber)
            };

            return (sql_command, parameters);
        }

        public override (string, List<(string, string)>) Update()
        {
            string sql_command = "UPDATE [Patients] SET Name=@Name, Surname=@Surname, Sex=@Sex, BirthDate=@BirthDate, HealthInsuranceNumber=@HealthInsuranceNumber WHERE Id=@Id";

            var parameters = new List<(string, string)>
            {
                ("@Id", Id.ToString()), ("@Name", Name), ("@Surname", Surname), ("@Sex", _sex), ("@BirthDate", _birthDate), ("@HealthInsuranceNumber", HealthInsuranceNumber)
            };

            return (sql_command, parameters);
        }

        public override (string, List<(string, string)>) Delete()
        {
            string sql_command = "DELETE FROM [Patients] WHERE Id=@Id";

            var parameters = new List<(string, string)>
            {
                ("@Id", Id.ToString())
            };

            return (sql_command, parameters);
        }

        #endregion

        #region Constructor Methods

        public dbPatient()
        {
            this.Name = null;
            this.Surname = null;
            this.Sex = Sexes.None;
            this.BirthDate = null;
            this.HealthInsuranceNumber = null;
        }   //.Generate() requires a "parameterless" constructor

        public dbPatient(long Id = -1)
        {
            this.Id = Id;
        }

        public dbPatient(string Name = null, string Surname = null, Sexes Sex = Sexes.None, DateTime? BirthDate = null, string HealthInsuranceNumber = null)
        {
            this.Name = Name;
            this.Surname = Surname;
            this.Sex = Sex;
            this.BirthDate = BirthDate;
            this.HealthInsuranceNumber = HealthInsuranceNumber;
        }

        #endregion
    }
}
