﻿using System;
using System.Collections.Generic;

namespace Dermoskop.Database.Models
{
    /// <summary>
    /// This class should contain all the common things of all Database Model classes.
    /// Also every Model should be derived out of this class.
    /// </summary>
    public abstract class DatabaseClass
    {
        protected const string DateTimePattern = "yyyy-MM-dd HH:mm:ss";

        public abstract long Id { get; set; }

        /// <summary>
        /// A function that reaturns you a valid sql command with parameters for Object (record) to be inserted.
        /// </summary>
        /// <returns>Tuple of sql command and list with parameters that need to be added to it.
        /// List's first element is key, second is value.</returns>
        public abstract (string, List<(string, string)>) Insert();

        /// <summary>
        /// A function that reaturns you a valid sql command with parameters for Object (record) to be updated.
        /// </summary>
        /// <returns>Tuple of sql command and list with parameters that need to be added to it.
        /// List's first element is key, second is value.</returns>
        public abstract (string, List<(string, string)>) Update();

        /// <summary>
        /// A function that reaturns you a valid sql command with parameters for Object (record) to be deleted.
        /// </summary>
        /// <returns>Tuple of sql command and list with parameters that need to be added to it.
        /// List's first element is key, second is value.</returns>
        public abstract (string, List<(string, string)>) Delete();
    }
}
