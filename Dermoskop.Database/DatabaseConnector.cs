﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using System.Data.SQLite;
using System.Data.Linq;

using Dermoskop.Database.Models;
using Dermoskop.Database.Enums;

namespace Dermoskop.Database
{
    
    public class DatabaseConnector
    {

        private const string DatabaseFile = "dermoskop.db";
        // this kind of string concatenation is bad practice, but couldn't be avoided. Please use formated strings $ in the future.
        private const string ConnectionString = "Data Source=" + DatabaseFile + ";Version=3;";

        /// <summary>
        /// Create a database file if it doesn't exist.
        /// </summary>
        public static void CreateDatabase()
        {
            if (!File.Exists(DatabaseFile))
                SQLiteConnection.CreateFile(DatabaseFile);
        }

        /// <summary>
        /// Deletes database file if it exists.
        /// </summary>
        public static void DeleteDatabase()
        {
            if (File.Exists(DatabaseFile))
                File.Delete(DatabaseFile);
        }

        /// <summary>
        /// Returns a connection string to the Database.
        /// </summary>
        public static string GetConnectionString()
        {
            return ConnectionString;
        }

        /// <summary>
        /// Creates a database scheme from SQL script file
        /// </summary>
        public static void CreateDatabaseScheme()
        {
            using (var connection = new SQLiteConnection(ConnectionString))
            {
                connection.Open();

                var command = new SQLiteCommand(connection);
                // read creation script from file
                string sql_script = File.ReadAllText("Scripts/CreateScheme.sql");
#if DEBUG
                Console.Write(sql_script);
#endif
                command.CommandText = sql_script;
                command.ExecuteNonQuery();

                connection.Close();
            }
        }

        /// <summary>
        /// Creates Super User with  Username "Dermoskop" and Password "1234"
        /// </summary>
        public static void CreateSuperUser()
        {
            var checkUser = DatabaseConnector.Query<dbUser>().Where(x => x.Username == "Dermoskop" & x.Password == HashClass.HashModel("1234"));
            if (!checkUser.Any())
            {
                dbUser SuperUser = new dbUser
                {

                    Username = "Dermoskop",
                    Password = HashClass.HashModel("1234"),
                    Name = "Dermo",
                    Surname = "Skop",
                };
                DatabaseConnector.Insert(SuperUser);
            }
        }


        /// <summary>
        /// Just a test function to demonstrate the behaviour of database connection
        /// TODO: remove when demo file is created
        /// </summary>
        public static void TestModels()
        {
            using (var connection = new SQLiteConnection(ConnectionString))
            {
                var patients = DatabaseConnector.Query<dbPatient>().Where(x => x.Name == "Adam");

                foreach (dbPatient p in patients)
                {
                    Console.WriteLine($"{p.FullName}, {p.BirthDate}, {p.Sex}");
                }
                Console.WriteLine("====================================");
                
                var users = DatabaseConnector.Query<dbUser>();

                foreach (dbUser u in users)
                {
                    Console.WriteLine($"{u.Name}, {u.Surname}");
                }
                Console.WriteLine("====================================");

                var images = DatabaseConnector.Query<dbImage>();

                foreach (dbImage image in images)
                {
                    Console.WriteLine($"{image.FileName}, {image.ExaminationId}");
                }
                Console.WriteLine("====================================");

                var exem = DatabaseConnector.Query<dbExamination>();

                foreach (dbExamination ex in exem)
                {
                    Console.WriteLine($"{ex.Comment}, {ex.ExaminationTime}");
                }
                Console.WriteLine("====================================");

                //testing patients
                dbPatient dummy = new dbPatient
                {
                    Name = "Ben",
                    Surname = "Bunny",
                    Sex = Sexes.M,
                    HealthInsuranceNumber = "123",
                    BirthDate = DateTime.Now
                };
                DatabaseConnector.Insert(dummy);

                dbPatient dummy2 = new dbPatient(Name: "Fen", Surname: "Funny", BirthDate: DateTime.Now);
                DatabaseConnector.Insert(dummy2);

                dbPatient nullpatient = new dbPatient();
                DatabaseConnector.Insert(nullpatient);

                var patients_all = DatabaseConnector.Query<dbPatient>().Where(x => x != null).OrderBy(x => x.Id);

                foreach (dbPatient p in patients_all)
                {
                    Console.WriteLine($"{p.Id}, {p.FullName}, {p.BirthDate}, {p.Sex}, {p.HealthInsuranceNumber}");
                }
                Console.WriteLine("====================================");

                Console.WriteLine($"New inserted Id: {dummy.Id}");
                dummy.Sex = Sexes.F;

                DatabaseConnector.Update(dummy);

                patients_all = DatabaseConnector.Query<dbPatient>().Where(x => x != null).OrderBy(x => x.Id);

                foreach (dbPatient p in patients_all)
                {
                    Console.WriteLine($"{p.Id}, {p.FullName}, {p.BirthDate}, {p.Sex}, {p.HealthInsuranceNumber}");
                }
                Console.WriteLine("====================================");

                DatabaseConnector.Delete(dummy);

                patients_all = DatabaseConnector.Query<dbPatient>().Where(x => x != null).OrderBy(x => x.Id);

                foreach (dbPatient p in patients_all)
                {
                    Console.WriteLine($"{p.Id}, {p.FullName}, {p.BirthDate}, {p.Sex}, {p.HealthInsuranceNumber}");
                }
                Console.WriteLine("====================================");

                // test updating and deleting of non-existing record
                dbPatient pTest = new dbPatient
                {
                    Name = "Carvalhio"
                };
                DatabaseConnector.Update(pTest);
                DatabaseConnector.Delete(pTest);

                //testing users
                dbUser nulluser = new dbUser();
                DatabaseConnector.Insert(nulluser);

                var users_all = DatabaseConnector.Query<dbUser>().Where(x => x != null).OrderBy(x => x.Id);

                foreach (dbUser u in users_all)
                {
                    Console.WriteLine($"{u.Id}, {u.Username}, {u.Password}, {u.FullName}");
                }
                Console.WriteLine("====================================");

                //testing examinations
                dbExamination nullexam = new dbExamination();
                DatabaseConnector.Insert(nullexam);

                var examinations_all = DatabaseConnector.Query<dbExamination>().Where(x => x != null).OrderBy(x => x.Id);

                foreach (dbExamination e in examinations_all)
                {
                    Console.WriteLine($"{e.Id}, {e.PatientId}, {e.UserId}, {e.ExaminationTime}, {e.Comment}");
                }
                Console.WriteLine("====================================");

                //testing images
                dbImage nullimg = new dbImage();
                DatabaseConnector.Insert(nullimg);

                var images_all = DatabaseConnector.Query<dbImage>().Where(x => x != null).OrderBy(x => x.Id);

                foreach (dbImage i in images_all)
                {
                    Console.WriteLine($"{i.Id}, {i.ExaminationId}, {i.FileName}, {i.Position}");
                }
                Console.WriteLine("====================================");

                //testing birthmarks
                dbBirthmark birthmark = new dbBirthmark();
                DatabaseConnector.Insert(birthmark);
                DatabaseConnector.Delete(birthmark);
                
                birthmark.xLocation = 234;
                birthmark.yLocation = 123;
                birthmark.Position = ImagePositions.Front;

                DatabaseConnector.Insert(birthmark);

                var birthmark_all = DatabaseConnector.Query<dbBirthmark>();

                foreach(var bm in birthmark_all)
                {
                    Console.WriteLine($"{bm.FirstBirthmarkTime}");
                }
                Console.WriteLine("====================================");
            }
        }

        /// <summary>
        /// Returns all records in a certain table. To edit them further use Linq expressions.
        /// Table is Described by T type prescribed by the function.
        /// </summary>
        /// <typeparam name="T">DatabaseClass type</typeparam>
        /// <returns></returns>
        public static List<T> Query<T>() where T : DatabaseClass
        {
            using (var connection = new SQLiteConnection(ConnectionString))
                using (var context = new DataContext(connection))
                    return context.GetTable<T>().AsQueryable().ToList();
        }

        /// <summary>
        /// Inserts a record, given as a parameter into correlating database table.
        /// </summary>
        /// <param name="dbc">Record that we want to insert into database</param>
        public static void Insert(DatabaseClass dbc)
        {
            (string cmd_string, List<(string, string)> parameters) = dbc.Insert();

            using(SQLiteConnection connection = new SQLiteConnection(ConnectionString))
            {
                SQLiteCommand cmd = new SQLiteCommand(cmd_string, connection);

                foreach ((string key, string value) in parameters)
                {
                    cmd.Parameters.Add(key, System.Data.DbType.String).Value = value;
                }

                try
                {
                    connection.Open();
                    object insertedId = cmd.ExecuteScalar();
                    dbc.Id = (long)insertedId;
#if DEBUG
                    Console.WriteLine($"Inserted Id: {insertedId}");
#endif
                }
                catch (Exception ex)
                {
#if DEBUG
                    Console.WriteLine(ex.Message);
#endif
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        /// <summary>
        /// Updates a given record in the database. As a primary key it chooses Id (updates record where Id is given).
        /// ATTENTION: Updates values in all the rows, so be vary of that behaviour.
        /// </summary>
        /// <param name="dbc">Record we want to update</param>
        public static void Update(DatabaseClass dbc)
        {
            (string cmd_string, List<(string, string)> parameters) = dbc.Update();

            using (SQLiteConnection connection = new SQLiteConnection(ConnectionString))
            {
                SQLiteCommand cmd = new SQLiteCommand(cmd_string, connection);

                foreach ((string key, string value) in parameters)
                {
                    cmd.Parameters.Add(key, System.Data.DbType.String).Value = value;
                }

                try
                {
                    connection.Open();
                    object rowsAffected = cmd.ExecuteNonQuery();
#if DEBUG
                    Console.WriteLine($"Rows affected: {rowsAffected}");
#endif
                }
                catch (Exception ex)
                {
#if DEBUG
                    Console.WriteLine(ex.Message);
#endif
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        /// <summary>
        /// Deletes a record that is given as a parameter. Record is choosen via Id value.
        /// </summary>
        /// <param name="dbc">Record we want to delete</param>
        public static void Delete(DatabaseClass dbc)
        {
            (string cmd_string, List<(string, string)> parameters) = dbc.Delete();

            using (SQLiteConnection connection = new SQLiteConnection(ConnectionString))
            {
                SQLiteCommand cmd = new SQLiteCommand(cmd_string, connection);

                foreach ((string key, string value) in parameters)
                {
                    cmd.Parameters.Add(key, System.Data.DbType.String).Value = value;
                }

                try
                {
                    connection.Open();
                    object rowsAffected = cmd.ExecuteNonQuery();
#if DEBUG
                    Console.WriteLine($"Rows affected: {rowsAffected}");
#endif
                }
                catch (Exception ex)
                {
#if DEBUG
                    Console.WriteLine(ex.Message);
#endif
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        /// <summary>
        /// Check if user name and password match between user input and database
        /// </summary>
        /// <param name="InputUser">Username</param>
        /// <param name="InputPassword">Hashed Password</param>
        public static dbUser LoginModel(string InputUser, string InputPassword)
        {
            var users = DatabaseConnector.Query<dbUser>().Where(x => x.Username == InputUser & x.Password == InputPassword);
            dbUser user = null;
            if (users.Any())
            {
                user = users.First();
            }
            return user;
        }
    }
}
