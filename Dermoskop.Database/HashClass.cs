﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using Bogus.DataSets;

namespace Dermoskop.Database
{
    public class HashClass
    {
        /// <summary>
        /// Hash input string with SHA256
        /// </summary>
        /// <param name="HashString">string to hash</param>
        /// <returns>Returns hashed string</returns>
        public static string HashModel(string HashString)
        {
            SHA256 sHA2 = SHA256.Create();
            byte[] b = Encoding.ASCII.GetBytes(HashString);
            byte[] hash_sha2 = sHA2.ComputeHash(b);
            StringBuilder sb = new StringBuilder();
            foreach (var a in hash_sha2)
                sb.Append(a.ToString("X2"));
            return sb.ToString();
        }
    }
}
