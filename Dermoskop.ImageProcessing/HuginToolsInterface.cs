﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dermoskop.ImageProcessing
{
    public class HuginToolsInterface
    {
        private Process process = new Process();

        public event EventHandler<HuginExitedEventArgs> Exited = delegate { };
        public event EventHandler<HuginExitedEventArgs> OutputDataReceived = delegate { };

        private string output = "";

        public HuginToolsInterface()
        {
            process.EnableRaisingEvents = true;
            process.Exited += Process_Exited;
#if DEBUG
            process.OutputDataReceived += Process_OutputDataReceived;
#endif
        }

        public void StartStitching(string tempFolder, string outputFile)
        {
            ProcessStartInfo processInfo;

            string commad = "run_hugin_tools.bat";

            processInfo = new ProcessStartInfo("cmd.exe", $"/c {commad} {tempFolder} {outputFile}");
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;
            // *** Redirect the output ***
            processInfo.RedirectStandardError = true;
#if DEBUG
            processInfo.RedirectStandardOutput = true;
#endif

            // Start process
            process.StartInfo = processInfo;
            
            process.Start();
#if DEBUG
            process.BeginOutputReadLine();
#endif
        }

        public void StopStitching()
        {
            process.Close();
        }

        private void Process_Exited(object sender, EventArgs e)
        {
            string error = process.StandardError.ReadToEnd();

            int exitCode = process.ExitCode;

            this.Exited.Invoke(sender, new HuginExitedEventArgs(output, error, exitCode));

#if DEBUG
            process.CancelOutputRead();
#endif
            process.Close();

            output = "";
        }

        private void Process_OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            output += $"\n{e.Data}";
            Console.WriteLine($"{e.Data}");
        }
    }
}
