﻿using System.Drawing;

namespace Dermoskop.ImageProcessing
{
    public static class ImageProcessor
    {
        public static Bitmap ScaleImage(Bitmap image, float scale)
        {
            return new Bitmap(image, (int)(image.Width * scale), (int)(image.Height * scale));
        }
    }
}