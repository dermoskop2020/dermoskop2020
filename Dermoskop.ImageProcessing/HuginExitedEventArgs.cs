﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dermoskop.ImageProcessing
{
    public class HuginExitedEventArgs : EventArgs
    {
        public string Error { get; protected set; }
        public string Output { get; protected set; }
        public int ErrorCode { get; protected set; }
        public HuginExitedEventArgs(string output, string error, int errorCode)
        {
            Output = output;
            Error = error;
            ErrorCode = errorCode;
        }
    }
}
