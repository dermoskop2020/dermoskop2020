echo.
echo PostBuild.bat running...
echo     Solution directory..... %1
echo     Configuration name..... %2
echo     Platform name.......... %3
echo.

echo Copying additional libraries from %1Thirdparty\%3...
xcopy %1Thirdparty\%3 %1Dermoskop\bin\%3\%2\ /Y /E /R /I /D
if NOT [%errorlevel%] == [0] (
	echo.
	echo Error copying libraries from %1Thirdparty\%3\ to %1Dermoskop\bin\%3\%2\
	echo.
	GOTO :end
)