#ifndef DERMOSKOP_H
#define DERMOSKOP_H

#include "AccelStepper/AccelStepper.h"

#define PI_UINT16 31415

typedef unsigned long uint32;
typedef signed long int32;
typedef unsigned int uint16; // On arduino native int is 16 bit not 32.
typedef signed int int16; 
typedef unsigned char uint8; // Same as byte
typedef signed char int8;
typedef double f32;
typedef float f16;

enum State : byte
{
  STATE_STANDBY = 0,
  STATE_READY,
  STATE_MOVING,
  STATE_CALIBRATING,
  STATE_RESETTING,
};

enum Direction : byte
{
  DIRECTION_UP = 0,
  DIRECTION_DOWN,
};

enum Command : byte
{
  CMD_ID = 1,
  CMD_CALIBRATE = 2,
  CMD_MOVE_NEXT = 3,
  CMD_RESET = 4,
  CMD_FULLSTOP = 5,
  CMD_CAPTURE_START = 6,
  CMD_CAPTURE_STOP = 7,
  CMD_GET_PROPERTY = 8,
};

enum Response : byte
{
  RESPONSE_OK = 1,
  RESPONSE_PROGRESS = 2,
  RESPONSE_READY = 3,
  RESPONSE_DONE_MOVE = 4,
  RESPONSE_DONE_CAPTURE = 5,
  RESPONSE_STATE = 6,
  RESPONSE_BUSY = 7,
  RESPONES_CALIBRATION_DONE = 8,
  RESPONES_RESET_DONE = 9,
  RESPONES_PROPERTY_NUM_OF_STEPS = 10,
  RESPONSE_UNKNOWN_CMD = 254,
  RESPONSE_ERROR = 255,
};

enum Properties
{
  PROP_NUM_OF_POS = 0x01,
};

struct Device
{
  State state = STATE_STANDBY;
  Direction direction = DIRECTION_UP;

  uint8 currentPosition = 0;
  uint8 numOfPositions = 5; // Total number of positions
  int32 numOfSteps = 0; // Total number of steps

  AccelStepper stepper;
  f16 speed = 300;
  f16 maxSpeed = 1250*2;
  f16 maxCalibratinSpeed = 600;
  f16 acceleration = 700*4;
  uint16 minPulseWidth = 10;

  uint8 stepPin = 4;
  uint8 directionPin = 7;
  uint8 enableMotorPin = 8;
  uint8 topSwitchPin = 12;
  uint8 bottomSwitchPin = 13;

  uint8 lightsPin = 11;
};

#endif // DERMOSKOP_H
