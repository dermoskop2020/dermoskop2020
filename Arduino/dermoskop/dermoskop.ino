#include "dermoskop.h"
#include "AccelStepper/AccelStepper.cpp"

// Timer
uint32 currentTime = 0;
uint32 previousTime = 0;
int16 updateTime = 100; // ms
int16 timeSinceLastUpdate = 0;
// Progress variables
uint32 progressTotalSteps = 0;
// Deivce
Device dev;

void sendresponse(uint8 cmdByte, uint8 dataByte = 1)
{
  uint8 outputBytes[2] = { cmdByte, dataByte };
  Serial.write(outputBytes, 2);
  /*
  Serial.println("\nOutput:");
  Serial.println("responseByte: " + (String)outputBytes[0]);
  Serial.println("dataByte: " + (String)outputBytes[1]);
  */
}

void calibrate()
{
  if (dev.numOfSteps > 0)
    return;
  /*
   * When calibrating, we first move our camera to the top (until we hit the top switch).
   * After, we move it to the bottom while counting steps.
  */
  dev.state = STATE_CALIBRATING;
  /* 
   * Set maximum calibration speed.
   * Should be lower tha normal move max speed, becouse we dont know when to decelerate.
  */
  dev.stepper.setMaxSpeed(dev.maxCalibratinSpeed);
  dev.stepper.setSpeed(dev.speed);
  
  dev.stepper.setCurrentPosition(0);
  dev.stepper.moveTo(INT32_MAX);
  
  dev.direction = DIRECTION_UP;
  dev.numOfSteps = 0;
  dev.currentPosition = 0;

  dev.stepper.enableOutputs();
}

void setup()
{
  Serial.begin(9600);
  Serial.setTimeout(500);
  pinMode(dev.topSwitchPin, INPUT);
  pinMode(dev.bottomSwitchPin, INPUT);
  pinMode(dev.lightsPin, OUTPUT);

  dev.stepper = AccelStepper(AccelStepper::DRIVER, dev.stepPin, dev.directionPin);
  dev.stepper.disableOutputs();
  dev.stepper.setEnablePin(dev.enableMotorPin);
  
  //directionInvert = true, stepInvert = false, enableInvert = true
  dev.stepper.setPinsInverted(true, false, true);

  dev.stepper.setCurrentPosition(0);
  dev.stepper.setMaxSpeed(dev.maxSpeed);
  dev.stepper.setSpeed(dev.speed);
  dev.stepper.setAcceleration(dev.acceleration);
  dev.stepper.setMinPulseWidth(dev.minPulseWidth);

  calibrate();
}

void loop()
{
  // Get delta time
  currentTime = millis();
  uint32 deltaTime = currentTime - previousTime;
  // Check for overflow
  if (deltaTime > 1000)
  {
    deltaTime = 0;
  }
  
  // Read input
  if (Serial.available())
  {
    uint8 incomingBytes[2];
    Serial.readBytes(incomingBytes, 2);
    
    /*
    Serial.println("\nInput:");
    Serial.println("cmdByte: " + (String)incomingBytes[0]);
    Serial.println("dataByte: " + (String)incomingBytes[1]);
    */
    switch (incomingBytes[0])
    {
      case CMD_ID:
        if (dev.state == STATE_STANDBY) {
          dev.state = STATE_READY;
        }
        // As a response to ID request we send back number PI.
        sendresponse(PI_UINT16 & 0xFF, (PI_UINT16 >> 8) & 0xFF);
        break;
      case CMD_CALIBRATE:
        if (dev.state == STATE_READY)
        {
          calibrate();
          sendresponse(RESPONSE_OK, 1);
        }
        else
        {
          sendresponse(RESPONSE_BUSY, 1);
        }
        break;
      case CMD_MOVE_NEXT:
        if (dev.state == STATE_READY)
        {
          /*
           * Calculate next position
           */
          int8 nextPosition;
          if (dev.direction == DIRECTION_UP)
          {
            nextPosition = dev.currentPosition + 1;
          }
          else
          {
            nextPosition = dev.currentPosition - 1;
          }
          
          /*
           * If we reched final position reverse the direction of movement and report we are done.
           */
          if (nextPosition < 0 || nextPosition > dev.numOfPositions)
          {
            dev.direction = (dev.direction == DIRECTION_UP) ? DIRECTION_DOWN : DIRECTION_UP;
            sendresponse(RESPONSE_DONE_CAPTURE, 0);
          }
          else
          {
            int32 numOfStepsPerPosition = dev.numOfSteps / dev.numOfPositions;
            // Step we need to get to.
            int32 stepToReach = numOfStepsPerPosition * nextPosition;
            // Bound if numOfStepsPerPosition gets rounded up.
            if (stepToReach > dev.numOfSteps) { stepToReach = dev.numOfSteps; }
            dev.stepper.setMaxSpeed(dev.maxSpeed);
            dev.stepper.moveTo(stepToReach);
            progressTotalSteps = dev.numOfSteps;
            dev.state = STATE_MOVING;
            sendresponse(RESPONSE_OK, 1);
          }
        }
        else
        {
          sendresponse(RESPONSE_BUSY, 1);
        }
        break;
      case CMD_RESET:
      case CMD_CAPTURE_STOP:
        if (dev.state != STATE_CALIBRATING && dev.state != STATE_STANDBY)
        {
          dev.direction = DIRECTION_DOWN;
          dev.stepper.setMaxSpeed(dev.maxCalibratinSpeed);
          dev.stepper.moveTo(0);
          progressTotalSteps = dev.stepper.currentPosition();
          dev.state = STATE_RESETTING;
          digitalWrite(dev.lightsPin, LOW);
          
          sendresponse(RESPONSE_OK, 1);
        }
        else
        {
          sendresponse(RESPONSE_BUSY, 1);
        }
        break;
      case CMD_CAPTURE_START:
        if (dev.state != STATE_CALIBRATING && dev.state != STATE_RESETTING)
        {
          digitalWrite(dev.lightsPin, HIGH);
          dev.stepper.enableOutputs();
          dev.stepper.setMaxSpeed(dev.maxSpeed);
          sendresponse(RESPONSE_OK, 1);
        }
        else
        {
          sendresponse(RESPONSE_BUSY, 1);
        }
        break;
      case CMD_GET_PROPERTY:
        //sendresponse(RESPONES_PROPERTY, dev.numOfPositions);
        switch (incomingBytes[1])
        {
          case PROP_NUM_OF_POS:
            sendresponse(RESPONES_PROPERTY_NUM_OF_STEPS, dev.numOfPositions);
            break;
        }
        break;
      case CMD_FULLSTOP:
        dev.state = STATE_READY;
        sendresponse(RESPONSE_OK, 1);
        break;
      default:
        sendresponse(RESPONSE_UNKNOWN_CMD, 1);
        break;
    }
  }

  if (dev.state == STATE_CALIBRATING)
  {
    /*
     * If we are moving up, check if we hit the top switch.
    */
    if (dev.direction == DIRECTION_UP)
    {
      uint8 topSwitch = !digitalRead(dev.topSwitchPin);
      if (topSwitch)
      {
        /*
         * If we hit the top switch we need to reverse the direction.
        */
        dev.direction = DIRECTION_DOWN;
        dev.stepper.setCurrentPosition(0);
        dev.stepper.move(INT32_MIN);
      }
      else
      {
        dev.stepper.run();
      }
    }
    else
    {
      /*
       * If we are moving down, count the steps we took to reach the bottom switch.
      */
      uint8 bottomSwitch = !digitalRead(dev.bottomSwitchPin);
      if (bottomSwitch)
      {
        dev.direction = DIRECTION_UP;
        dev.state = STATE_READY;
        dev.numOfSteps = abs(dev.stepper.currentPosition());
        dev.stepper.setCurrentPosition(0);
        dev.stepper.disableOutputs();
        sendresponse(RESPONES_CALIBRATION_DONE, 1);
      }
      else
      {
        dev.stepper.run();
      }
    }
  }
  else if (dev.state == STATE_MOVING || dev.state == STATE_RESETTING)
  {
    bool switchHit = (!digitalRead(dev.topSwitchPin) && dev.direction == DIRECTION_UP) || (!digitalRead(dev.bottomSwitchPin) && dev.direction == DIRECTION_DOWN);

    if (dev.stepper.distanceToGo() == 0)
    {
      if (dev.state == STATE_RESETTING)
      {
        dev.currentPosition = 0;
        dev.direction = DIRECTION_UP;
        dev.stepper.disableOutputs();
        sendresponse(RESPONES_RESET_DONE, 1);
      }
      else
      {
        dev.currentPosition = (dev.direction == DIRECTION_UP) ? (dev.currentPosition + 1) : (dev.currentPosition - 1);
        delay(500);
        sendresponse(RESPONSE_DONE_MOVE, 1);
      }
      dev.state = STATE_READY;
    }
    else if(switchHit)
    {
      if (dev.state == STATE_RESETTING)
      {
        dev.currentPosition = 0;
        dev.stepper.setCurrentPosition(0);
        dev.direction = DIRECTION_UP;
        dev.stepper.disableOutputs();
        sendresponse(RESPONES_RESET_DONE, 1);
      }
      else
      {
        dev.currentPosition = (dev.direction == DIRECTION_UP) ? dev.numOfPositions : 0;
        dev.stepper.setCurrentPosition((dev.direction == DIRECTION_UP) ? dev.numOfSteps : 0);
        delay(500);
        sendresponse(RESPONSE_DONE_MOVE, 1);
      }
      dev.state = STATE_READY;
    }
    else
    {
      dev.stepper.run();
    }

    // Update progress
    timeSinceLastUpdate -= deltaTime;
    if (timeSinceLastUpdate < 0)
    {
      uint8 progress = 1;
      if (dev.direction == DIRECTION_UP)
      {
        progress = (100 * dev.stepper.currentPosition()) / progressTotalSteps;
      }
      else
      {
        progress = (100 * (progressTotalSteps - dev.stepper.currentPosition())) / progressTotalSteps;
      }
      sendresponse(RESPONSE_PROGRESS, progress);
      timeSinceLastUpdate = updateTime;
    }
  }

  previousTime = currentTime;
}
