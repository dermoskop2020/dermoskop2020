﻿using System;

namespace Dermoskop.Devices
{
    public class CameraPropertyDescChangedEventArgs : EventArgs
    {
        public IntPtr PropertyId { get; private set; }

        public CameraPropertyDescChangedEventArgs(IntPtr propertyId)
        {
            this.PropertyId = propertyId;
        }
    }
}
