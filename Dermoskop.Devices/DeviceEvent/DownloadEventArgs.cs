﻿using System;
using System.Drawing;

namespace Dermoskop.Devices
{
    public class DownloadEventArgs : EventArgs
    {
        public enum DownloadStatus
        {
            STARTED,
            COMPLETED
        }

        public DownloadStatus Status { get; private set; }
        public IntPtr Stream { get; private set; }
        public Bitmap Bitmap { get; private set; }

        public DownloadEventArgs(DownloadStatus status, Bitmap bitmap)
        {
            this.Status = status;
            this.Bitmap = bitmap;
        }
    }
}
