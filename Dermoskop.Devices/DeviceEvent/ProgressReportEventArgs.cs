﻿using System;

namespace Dermoskop.Devices
{
    public class ProgressReportEventArgs : EventArgs
    {
        public int Progress { get; }
        private bool cancel;

        public ProgressReportEventArgs(int progress)
        {
            this.Progress = progress;
        }

        public ProgressReportEventArgs(int progress, ref bool outCancel) : this(progress)
        {
            this.cancel = outCancel;
        }

        public void CancelProgress()
        {
            this.cancel = true;
        }
    }
}
