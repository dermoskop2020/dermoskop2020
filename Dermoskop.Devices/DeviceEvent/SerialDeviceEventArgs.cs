﻿using System;

namespace Dermoskop.Devices
{
    public class SerialDeviceEventArgs : EventArgs
    {
        public byte Message { get; }

        public SerialDeviceEventArgs(byte msg)
        {
            this.Message = msg;
        }
    }
}
