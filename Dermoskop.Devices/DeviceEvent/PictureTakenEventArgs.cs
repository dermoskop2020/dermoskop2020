﻿using System;
using System.Drawing;

namespace Dermoskop.Devices
{
    public class PictureTakenEventArgs : EventArgs
    {
        public Bitmap Bitmap { get; }

        public PictureTakenEventArgs(Bitmap bitmap)
        {
            this.Bitmap = bitmap;
        }
    }
}
