﻿using System;

namespace Dermoskop.Devices
{
    public class NativeDeviceEventArgs : EventArgs
    {
        public IntPtr Pointer { get; }

        public NativeDeviceEventArgs(IntPtr pointer)
        {
            this.Pointer = pointer;
        }
    }
}
