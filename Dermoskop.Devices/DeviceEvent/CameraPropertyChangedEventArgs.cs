﻿using System;

namespace Dermoskop.Devices
{
    public class CameraPropertyChangedEventArgs : EventArgs
    {
        public IntPtr PropertyId { get; private set; }

        public CameraPropertyChangedEventArgs(IntPtr propertyId)
        {
            this.PropertyId = propertyId;
        }
    }
}
