﻿using System;

namespace Dermoskop.Devices
{
    public class DeviceErrorEventArgs : EventArgs
    {
        public string Message { get; }

        public DeviceErrorEventArgs(string message)
        {
            this.Message = message;
        }
    }
}
