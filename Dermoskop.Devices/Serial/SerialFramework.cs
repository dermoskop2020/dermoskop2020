﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dermoskop.Devices
{
    /// <summary>
    /// Global serial devices helper class.
    /// </summary>
    public static class SerialFramework
    {
        public enum ErrorCode : byte
        {
            OK = 0x01,
            BUSY = 0x07,
            TIME_OUT = 0x0A,
            UNKNOWN_COMMAND = 0xFE,
        };

        public enum Event : byte
        {
            PROGRESS = 0x02,
            MOVE_DONE = 0x04,
            CAPTURE_DONE = 0x05,
            CALIBRATION_DONE = 0x08,
            RESET_DONE = 0x09,
            GET_PROPERTY_NUM_OF_STEPS = 0x0A,
            ERROR = 0xFF,
        }

        public enum Command : byte
        {
            ID = 0x01,
            CALIBRATE = 0x02,
            MOVE = 0x03,
            RESET = 0x04,
            FULLSTOP = 0x05,
            CAPTURE_START = 0x06,
            CAPTURE_STOP = 0x07,
            GET_PROPERTY = 0x08,
        }

        public enum PropertyId : byte
        {
            NUM_OF_POS = 0x01,
        }

        /// <summary>
        /// Gets invoked each time we find a new serial device.
        /// </summary>
        public static event EventHandler SerialDevicesFound = delegate { };
        public static event EventHandler SearchDone = delegate { };

        private static CommandProcessor processor = new CommandProcessor();

        public static bool IsSearchInProgress()
        {
            return !(processor.IsEmpty());
        }

        /// <summary>
        /// Begin async search for serial devices.
        /// </summary>
        public static void FindSerialDevice()
        {
            if (!processor.IsEmpty())
                return;

            string[] serialPortNames = SerialPort.GetPortNames();
            foreach (string portName in serialPortNames)
            {
                SerialDevice dev = new SerialDevice(portName);
                try
                {
                    dev.Port.Open();
                }
                catch
                {
                    continue;
                }
                dev.IdEvent += Id_Event;
                processor.PostCommand(new SerialIdCommand(dev));
            }
            if (!processor.IsEmpty())
                processor.Start();
        }

        private static void Id_Event(object sender, SerialDeviceEventArgs e)
        {
            SerialDevice serialDevice = (SerialDevice)sender;
            if (e.Message == (byte)ErrorCode.OK)
            {
                SerialDevicesFound.Invoke(serialDevice, EventArgs.Empty);
            }
            else
            {
                serialDevice.Port.Close();
            }

            if (processor.IsEmpty())
            {
                processor.Stop();
                SearchDone.Invoke(null, EventArgs.Empty);
            }
        }
    }
}
