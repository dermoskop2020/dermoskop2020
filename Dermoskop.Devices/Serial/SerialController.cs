﻿using System;
using System.IO.Ports;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading;

namespace Dermoskop.Devices
{
    /// <summary>
    /// Executes commands.
    /// </summary>
    public class SerialController : DeviceController
    {
        public SerialController(SerialDevice device) : base(device)
        {
            SerialPort port = ((SerialDevice)Device).Port;
            port.DataReceived += this.DataReceived;
        }

        /// <summary>
        /// Enable the device and begin execution of the commands.
        /// </summary>
        public override void Run()
        {
            SerialDevice dev = (SerialDevice)Device;
            if (!dev.Port.IsOpen)
                dev.Port.Open();
            base.Run();
        }

        /// <summary>
        /// Request an action to be executed by the device.
        /// </summary>
        public override void RequestAction(ActionRequest a)
        {
            ActionRequest.Command command = a.Cmd;
            SerialDevice device = (SerialDevice)Device;
            switch (command)
            {
                case ActionRequest.Command.SERIAL_ID:
                    processor.PostCommand(new SerialIdCommand(device));
                    break;
                case ActionRequest.Command.SERIAL_CALIBRATE:
                    processor.PostCommand(new SerialCalibrateCommand(device));
                    break;
                case ActionRequest.Command.SERIAL_MOVE:
                    processor.PostCommand(new SerialMoveCommand(device));
                    break;
                case ActionRequest.Command.SERIAL_RESET:
                    processor.PostCommand(new SerialResetCommand(device));
                    break;
                case ActionRequest.Command.SERIAL_STOP:
                    processor.PostCommand(new SerialStopCommand(device));
                    break;
                case ActionRequest.Command.SERIAL_CAPTURE_START:
                    processor.PostCommand(new SerialCaptureStart(device));
                    break;
                case ActionRequest.Command.SERIAL_CAPTURE_STOP:
                    processor.PostCommand(new SerialCaptureStop(device));
                    break;
                case ActionRequest.Command.SERIAL_GET_PROPERTY:
                    processor.PostCommand(new SerialGetProperty(device, ((SerialActionRequestArgs)a.Arg).Data));
                    break;
                case ActionRequest.Command.CLOSING:
                    processor.Stop();
                    break;
                case ActionRequest.Command.SHUT_DOWN:
                    processor.Stop();
                    ((SerialDevice)Device).Port.Close();
                    Device.OnShutDown(EventArgs.Empty);
                    break;
                default:
                    break;
            }
        }

        private void DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort port = sender as SerialPort;

            byte eventByte;
            byte dataByte = 0;
            try
            {
                eventByte = (byte)port.ReadByte();
                if (port.IsOpen)
                    dataByte = (byte)port.ReadByte();
            }
            catch (TimeoutException)
            {
                ((SerialDevice)Device).OnError(new DeviceErrorEventArgs("Serial device timeout. (DataReceived)"));
                return;
            }

            switch (eventByte)
            {
                case (byte)SerialFramework.Event.CALIBRATION_DONE:
                    ((SerialDevice)Device).OnCalibrationDone(EventArgs.Empty);
                    break;

                case (byte)SerialFramework.Event.MOVE_DONE:
                    ((SerialDevice)Device).OnMoveDone(EventArgs.Empty);
                    break;

                case (byte)SerialFramework.Event.RESET_DONE:
                    ((SerialDevice)Device).OnResetDone(EventArgs.Empty);
                    break;

                case (byte)SerialFramework.Event.ERROR:
                    ((SerialDevice)Device).OnError(new DeviceErrorEventArgs("Serial device error. Code: " + dataByte.ToString("X")));
                    break;

                case (byte)SerialFramework.Event.PROGRESS:
                    ((SerialDevice)Device).OnProgressReport(new ProgressReportEventArgs((int)dataByte));
                    break;
            }
        }
    }
}
