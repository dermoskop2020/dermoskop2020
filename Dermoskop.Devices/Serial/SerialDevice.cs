﻿using System;
using System.IO.Ports;
using System.Runtime.InteropServices;

namespace Dermoskop.Devices
{
    public class SerialDevice : Device
    {
        public SerialPort Port { get; private set; }

        public event EventHandler<SerialDeviceEventArgs> SerialEvent = delegate { };
        public event EventHandler<SerialDeviceEventArgs> IdEvent = delegate { };
        public event EventHandler<EventArgs> CalibrationStarted = delegate { };
        public event EventHandler<EventArgs> CalibrationDone = delegate { };
        public event EventHandler<EventArgs> MoveStarted = delegate { };
        public event EventHandler<EventArgs> MoveDone = delegate { };
        public event EventHandler<EventArgs> CaptureDone = delegate { };
        public event EventHandler<EventArgs> ResetStarted = delegate { };
        public event EventHandler<EventArgs> ResetDone = delegate { };
        public event EventHandler<EventArgs> Stopped = delegate { };
        public event EventHandler<SerialDeviceEventArgs> GetProperty = delegate { };

        public SerialDevice(string serialPortName) : base(Device.Type.SERIAL)
        {
            this.Port = new SerialPort(serialPortName, 9600, Parity.None, 8, StopBits.One);
            this.Port.RtsEnable = true;
            this.Port.ReadTimeout = 500;
            this.Port.WriteTimeout = 500;
            this.Port.ReceivedBytesThreshold = 2;
        }

        public void OnIdEvent(SerialDeviceEventArgs e)
        {
            this.IdEvent.Invoke(this, e);
        }

        public void OnCalibrationStarted(EventArgs e)
        {
            this.CalibrationStarted.Invoke(this, e);
        }

        public void OnCalibrationDone(EventArgs e)
        {
            this.CalibrationDone.Invoke(this, e);
        }

        public void OnMoveStarted(EventArgs e)
        {
            this.MoveStarted.Invoke(this, e);
        }

        public void OnMoveDone(EventArgs e)
        {
            this.MoveDone.Invoke(this, e);
        }

        public void OnCaptureDone(EventArgs e)
        {
            this.CaptureDone.Invoke(this, e);
        }

        public void OnResetStarted(EventArgs e)
        {
            this.MoveStarted.Invoke(this, e);
        }

        public void OnResetDone(EventArgs e)
        {
            this.MoveDone.Invoke(this, e);
        }
        public void OnStopped(EventArgs e)
        {
            this.Stopped.Invoke(this, e);
        }

        public void OnGetProperty(SerialDeviceEventArgs e)
        {
            this.GetProperty.Invoke(this, e);
        }
    }
}
