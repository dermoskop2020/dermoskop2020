﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dermoskop.Devices
{
    public class CameraDevice : Device
    {
        // Event handlers
        public event EventHandler<EventArgs> PictureTaken = delegate { };
        public event EventHandler<DownloadEventArgs> Download = delegate { };

        public CameraDevice() : base(Device.Type.CAMERA)
        {
        }

        public virtual void OnPictureTaken()
        {
            this.PictureTaken(this, EventArgs.Empty);
        }

        public virtual void OnDownload(DownloadEventArgs e)
        {
            this.Download(this, e);
        }
    }
}
