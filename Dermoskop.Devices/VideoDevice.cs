﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dermoskop.Devices
{
    public class VideoDevice : CameraDevice
    {
        public event EventHandler<DownloadEventArgs> NewVideoFrame = delegate { };

        public VideoDevice() : base() {}

        public void OnNewVideoFrame(DownloadEventArgs e)
        {
            this.NewVideoFrame.Invoke(this, e);
        }
    }
}
