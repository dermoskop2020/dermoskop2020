﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Dermoskop.Devices
{
    /// <summary>
    /// Global Eos (canon) helper class.
    /// </summary>
    public static class EosFramework
    {
        private static bool isSDKLoaded = false;

        private static readonly EDSDKLib.EDSDK.EdsPropertyEventHandler handlePropertyEvent = new EDSDKLib.EDSDK.EdsPropertyEventHandler(EosCameraEventListener.HandlePropertyEvent);
        private static readonly EDSDKLib.EDSDK.EdsObjectEventHandler handleObjectEvent = new EDSDKLib.EDSDK.EdsObjectEventHandler(EosCameraEventListener.HandleObjectEvent);
        private static readonly EDSDKLib.EDSDK.EdsStateEventHandler handleStateEvent = new EDSDKLib.EDSDK.EdsStateEventHandler(EosCameraEventListener.HandleStateEvent);

        private static GCHandle handle;

        /// <summary>
        /// Initialize EDSDK.
        /// </summary>
        /// <returns>Returns EDSDK error code.</returns>
        public static uint Initialize()
        {
            uint err = EDSDKLib.EDSDK.EdsInitializeSDK();

            if (err == EDSDKLib.EDSDK.EDS_ERR_OK)
            {
                isSDKLoaded = true;
            }
            else
            {
                throw new EosException(err);
            }

            return err;
        }

        /// <summary>
        /// Check if EDSDK is initialized.
        /// /// </summary>
        /// <returns>Returns true if EDSDK is initialized.</returns>
        public static bool IsInitialized()
        {
            return isSDKLoaded;
        }

        /// <summary>
        /// Get object representing list of connected cameras.
        /// </summary>
        /// <param name="cameraList"></param>
        /// <returns>Returns EDSDK error code.</returns>
        public static uint GetCameraList(out IntPtr cameraList)
        {
            uint err = EDSDKLib.EDSDK.EdsGetCameraList(out cameraList);

            if (err != EDSDKLib.EDSDK.EDS_ERR_OK)
            {
                throw new EosException(err);
            }

            return err;
        }

        /// <summary>
        /// Get number of connected cameras.
        /// </summary>
        /// <param name="cameraList"></param>
        /// <param name="count"></param>
        /// <returns>Returns EDSDK error code.</returns>
        public static uint GetNumberOfCameras(IntPtr cameraList, out int count)
        {
            uint err = EDSDKLib.EDSDK.EdsGetChildCount(cameraList, out count);
            if (count == 0)
            {
                err = EDSDKLib.EDSDK.EDS_ERR_DEVICE_NOT_FOUND;
            }

            if (err != EDSDKLib.EDSDK.EDS_ERR_OK)
            {
                throw new EosException(err);
            }

            return err;
        }

        /// <summary>
        /// Gets an indexed camera object of the designated camera list.
        /// </summary>
        /// <param name="index"></param>
        /// <param name="cameraList"></param>
        /// <param name="camera"></param>
        /// <returns>Returns EDSDK error code.</returns>
        public static uint GetCameraAtIndex(int index, IntPtr cameraList, out IntPtr camera)
        {
            uint err = EDSDKLib.EDSDK.EdsGetChildAtIndex(cameraList, index, out camera);

            if (err != EDSDKLib.EDSDK.EDS_ERR_OK)
            {
                throw new EosException(err);
            }

            return err;
        }

        /// <summary>
        /// Gets device information, such as the device name.  
        /// </summary>
        /// <param name="camera"></param>
        /// <param name="deviceInfo"></param>
        /// <returns>Returns EDSDK error code.</returns>
        public static uint GetDeviceInfo(IntPtr camera, out EDSDKLib.EDSDK.EdsDeviceInfo deviceInfo)
        {
            uint err = EDSDKLib.EDSDK.EdsGetDeviceInfo(camera, out deviceInfo);
            if (err == EDSDKLib.EDSDK.EDS_ERR_OK && camera == IntPtr.Zero)
            {
                err = EDSDKLib.EDSDK.EDS_ERR_DEVICE_NOT_FOUND;
            }

            if (err != EDSDKLib.EDSDK.EDS_ERR_OK)
            {
                throw new EosException(err);
            }

            return err;
        }

        /// <summary>
        /// Set camera controller to receive camera events.
        /// </summary>
        /// <param name="controller"></param>
        /// <returns>Returns EDSDK error code.</returns>
        public static uint SetCameraController(EosCameraController controller)
        {
            // Create CameraController handle
            handle = GCHandle.Alloc(controller);
            IntPtr ptr = GCHandle.ToIntPtr(handle);

            EosCameraDevice camera = (EosCameraDevice)controller.Device;

            // Set Property Evend Handler
            uint err = EDSDKLib.EDSDK.EdsSetPropertyEventHandler(camera.Camera, EDSDKLib.EDSDK.PropertyEvent_All, handlePropertyEvent, ptr);

            // Set Object Event Handler
            if (err == EDSDKLib.EDSDK.EDS_ERR_OK)
            {
                err = EDSDKLib.EDSDK.EdsSetObjectEventHandler(camera.Camera, EDSDKLib.EDSDK.ObjectEvent_All, handleObjectEvent, ptr);
            }

            // Set State Event Handler
            if (err == EDSDKLib.EDSDK.EDS_ERR_OK)
            {
                err = EDSDKLib.EDSDK.EdsSetCameraStateEventHandler(camera.Camera, EDSDKLib.EDSDK.StateEvent_All, handleStateEvent, ptr);
            }

            if (err != EDSDKLib.EDSDK.EDS_ERR_OK)
            {
                throw new EosException(err);
            }

            return err;
        }

        /// <summary>
        /// Release camera controller.
        /// </summary>
        public static void ReleaseCameraController()
        {
            if (handle.IsAllocated)
            {
                handle.Free();
            }
        }

        /// <summary>
        /// Release camera list.
        /// </summary>
        /// <param name="cameraList"></param>
        public static void ReleaseCameraList(IntPtr cameraList)
        {
            if (cameraList != IntPtr.Zero)
            {
                EDSDKLib.EDSDK.EdsRelease(cameraList);
            }
        }

        /// <summary>
        /// Release camera.
        /// </summary>
        /// <param name="camera"></param>
        public static void ReleaseCamera(IntPtr camera)
        {
            if (camera != IntPtr.Zero)
            {
                EDSDKLib.EDSDK.EdsRelease(camera);
            }
        }

        /// <summary>
        /// Terminate EDSDK.
        /// </summary>
        public static void Terminate()
        {
            if (isSDKLoaded)
            {
                EDSDKLib.EDSDK.EdsTerminateSDK();
            }
        }

        public static string ConvertErrorCodeToString(uint err)
        {
            // TODO: convert error codes to human readable messages
            /*switch(err)
            {
            }*/
            return "Unknown EDSDK error.";
        }

        /// <summary>
        /// Get all Eos cameras cameras.
        /// </summary>
        /// <returns>Returns List of EosCameraDevice.</returns>
        public static List<EosCameraDevice> GetCameraDevices()
        {
            List<EosCameraDevice> cameraDevices = new List<EosCameraDevice>();

            IntPtr cameraList;
            EosFramework.GetCameraList(out cameraList);
            EosFramework.GetNumberOfCameras(cameraList, out int count);
            for (int i = 0; i < count; i++)
            {
                EosFramework.GetCameraAtIndex(0, cameraList, out IntPtr camera);
                EosFramework.GetDeviceInfo(camera, out EDSDKLib.EDSDK.EdsDeviceInfo deviceInfo);
                EosCameraDevice cameraDev = new EosCameraDevice(camera);
                cameraDevices.Add(cameraDev);
            }
            EosFramework.ReleaseCameraList(cameraList);

            return cameraDevices;
        }
    }
}
