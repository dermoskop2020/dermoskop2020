﻿using System;

namespace Dermoskop.Devices
{
    public class EosCameraDevice : CameraDevice
    {
        public IntPtr Camera { get; set; }
        public string ModelName { get; set; }
        public bool IsTypeDS { get; set; }

        // Taking a picture parameter
        public uint AEMode { get; set; }
        public uint AFMode { get; set; }
        public uint DriveMode { get; set; }
        public uint WhiteBalance { get; set; }
        public uint Av { get; set; }
        public uint Tv { get; set; }
        public uint Iso { get; set; }
        public uint MeteringMode { get; set; }
        public uint ExposureCompensation { get; set; }
        public uint ImageQuality { get; set; }
        public uint AvailableShot { get; set; }
        public uint EvfMode { get; set; }
        public uint StartupEvfOutputDevice { get; set; }
        public uint EvfOutputDevice { get; set; }
        public uint EvfDepthOfFieldPreview { get; set; }
        public uint EvfAFMode { get; set; }
        public EDSDKLib.EDSDK.EdsFocusInfo FocusInfo { get; set; }
        public uint BatteryLebel { get; set; }
        public uint Zoom { get; set; }
        public EDSDKLib.EDSDK.EdsRect ZoomRect { get; set; }
        public uint FlashMode { get; set; }
        public uint TempStatus { get; set; }
        public uint RollPitch { get; set; }
        public uint MovieQuality { get; set; }
        public uint FixedMovie { get; set; }
        public uint MirrorUpSetting { get; set; }
        public uint MirrorLockUpState { get; set; }
        public byte[] ClickWB { get; set; }
        public int ClickPoint { get; set; }

        public EDSDKLib.EDSDK.EdsPropertyDesc AEModeDesc { get; set; }
        public EDSDKLib.EDSDK.EdsPropertyDesc DriveModeDesc { get; set; }
        public EDSDKLib.EDSDK.EdsPropertyDesc WhiteBalanceDesc { get; set; }
        public EDSDKLib.EDSDK.EdsPropertyDesc AvDesc { get; set; }
        public EDSDKLib.EDSDK.EdsPropertyDesc TvDesc { get; set; }
        public EDSDKLib.EDSDK.EdsPropertyDesc IsoDesc { get; set; }
        public EDSDKLib.EDSDK.EdsPropertyDesc MeteringModeDesc { get; set; }
        public EDSDKLib.EDSDK.EdsPropertyDesc ExposureCompensationDesc { get; set; }
        public EDSDKLib.EDSDK.EdsPropertyDesc ImageQualityDesc { get; set; }
        public EDSDKLib.EDSDK.EdsPropertyDesc EvfAFModeDesc { get; set; }
        public EDSDKLib.EDSDK.EdsPropertyDesc ZoomDesc { get; set; }
        public EDSDKLib.EDSDK.EdsPropertyDesc FlashModeDesc { get; set; }
        public EDSDKLib.EDSDK.EdsPropertyDesc MovieQualityDesc { get; set; }

        // Event delegates
        public event EventHandler<CameraPropertyChangedEventArgs> PropertyChanged = delegate { };
        public event EventHandler<CameraPropertyDescChangedEventArgs> PropertyDescChanged = delegate { };


        public EosCameraDevice(IntPtr camera) : base()
        {
            this.Camera = camera;
            this.Id = camera;
        }

        public void SetPropertyString(uint propertyID, String str)
        {
            switch (propertyID)
            {
                case EDSDKLib.EDSDK.PropID_ProductName: this.ModelName = str; this.IsTypeDS = str.Contains("EOS"); break;
            }
        }

        public void SetPropertyUInt32(uint propertyID, uint value)
        {
            switch (propertyID)
            {
                case EDSDKLib.EDSDK.PropID_AEModeSelect: this.AEMode = value; break;
                case EDSDKLib.EDSDK.PropID_AFMode: this.AFMode = value; break;
                case EDSDKLib.EDSDK.PropID_DriveMode: this.DriveMode = value; break;
                case EDSDKLib.EDSDK.PropID_Tv: this.Tv = value; break;
                case EDSDKLib.EDSDK.PropID_Av: this.Av = value; break;
                case EDSDKLib.EDSDK.PropID_ISOSpeed: this.Iso = value; break;
                case EDSDKLib.EDSDK.PropID_MeteringMode: this.MeteringMode = value; break;
                case EDSDKLib.EDSDK.PropID_ExposureCompensation: this.ExposureCompensation = value; break;
                case EDSDKLib.EDSDK.PropID_ImageQuality: this.ImageQuality = value; break;
                case EDSDKLib.EDSDK.PropID_Evf_Mode: this.EvfMode = value; break;
                case EDSDKLib.EDSDK.PropID_Evf_OutputDevice: if (this.EvfOutputDevice == 0xffffffff) StartupEvfOutputDevice = value;  this.EvfOutputDevice = value; break;
                case EDSDKLib.EDSDK.PropID_Evf_DepthOfFieldPreview: this.EvfDepthOfFieldPreview = value; break;
                case EDSDKLib.EDSDK.PropID_Evf_AFMode: this.EvfAFMode = value; break;
                case EDSDKLib.EDSDK.PropID_AvailableShots: this.AvailableShot = value; break;
                case EDSDKLib.EDSDK.PropID_DC_Zoom: this.Zoom = value; break;
                case EDSDKLib.EDSDK.PropID_DC_Strobe: this.FlashMode = value; break;
                case EDSDKLib.EDSDK.PropID_TempStatus: this.TempStatus = value; break;
                case EDSDKLib.EDSDK.PropID_FixedMovie: this.FixedMovie = value; break;
                case EDSDKLib.EDSDK.PropID_MirrorUpSetting: this.MirrorUpSetting = value; break;
                case EDSDKLib.EDSDK.PropID_MirrorLockUpState: this.MirrorLockUpState = value; break;
            }
        }

        public void SetPropertyInt32(uint propertyID, uint value)
        {
            switch (propertyID)
            {
                case EDSDKLib.EDSDK.PropID_WhiteBalance: this.WhiteBalance = value; break;
                case EDSDKLib.EDSDK.PropID_BatteryLevel: this.BatteryLebel = value; break;
            }
        }

        public void SetPropertyFocusInfo(uint propertyID, EDSDKLib.EDSDK.EdsFocusInfo info)
        {
            switch (propertyID)
            {
                case EDSDKLib.EDSDK.PropID_FocusInfo: this.FocusInfo = info; break;
            }
        }

        public void SetPropertyByteBlock(uint propertyID, byte[] data)
        {
            switch (propertyID)
            {
                case EDSDKLib.EDSDK.PropID_MovieParam: this.MovieQuality = BitConverter.ToUInt32(data, 0); break;
                case EDSDKLib.EDSDK.PropID_Evf_ClickWBCoeffs: this.ClickWB = data; break;
            }
        }

        // Setting of value list that can set taking a picture parameter
        public void SetPropertyDesc(uint propertyID, ref EDSDKLib.EDSDK.EdsPropertyDesc desc)
        {
            switch (propertyID)
            {
                case EDSDKLib.EDSDK.PropID_AEModeSelect: this.AEModeDesc = desc; break;
                case EDSDKLib.EDSDK.PropID_DriveMode: this.DriveModeDesc = desc; break;
                case EDSDKLib.EDSDK.PropID_WhiteBalance: this.WhiteBalanceDesc = desc; break;
                case EDSDKLib.EDSDK.PropID_Tv: this.TvDesc = desc; break;
                case EDSDKLib.EDSDK.PropID_Av: this.AvDesc = desc; break;
                case EDSDKLib.EDSDK.PropID_ISOSpeed: this.IsoDesc = desc; break;
                case EDSDKLib.EDSDK.PropID_MeteringMode: this.MeteringModeDesc = desc; break;
                case EDSDKLib.EDSDK.PropID_ExposureCompensation: this.ExposureCompensationDesc = desc; break;
                case EDSDKLib.EDSDK.PropID_ImageQuality: this.ImageQualityDesc = desc; break;
                case EDSDKLib.EDSDK.PropID_Evf_AFMode: this.EvfAFModeDesc = desc; break;
                case EDSDKLib.EDSDK.PropID_DC_Zoom: this.ZoomDesc = desc; break;
                case EDSDKLib.EDSDK.PropID_DC_Strobe: this.FlashModeDesc = desc; break;
                case EDSDKLib.EDSDK.PropID_MovieParam: this.MovieQualityDesc = desc; break;
            }
        }

        // Acquisition of value list that can set taking a picture parameter
        public EDSDKLib.EDSDK.EdsPropertyDesc GetPropertyDesc(uint propertyID)
        {
            EDSDKLib.EDSDK.EdsPropertyDesc desc = new EDSDKLib.EDSDK.EdsPropertyDesc { };
            switch (propertyID)
            {
                case EDSDKLib.EDSDK.PropID_AEModeSelect: desc = this.AEModeDesc; break;
                case EDSDKLib.EDSDK.PropID_DriveMode: desc = this.DriveModeDesc; break;
                case EDSDKLib.EDSDK.PropID_WhiteBalance: desc = this.WhiteBalanceDesc; break;
                case EDSDKLib.EDSDK.PropID_Tv: desc = this.TvDesc; break;
                case EDSDKLib.EDSDK.PropID_Av: desc = this.AvDesc; break;
                case EDSDKLib.EDSDK.PropID_ISOSpeed: desc = this.IsoDesc; break;
                case EDSDKLib.EDSDK.PropID_MeteringMode: desc = this.MeteringModeDesc; break;
                case EDSDKLib.EDSDK.PropID_ExposureCompensation: desc = this.ExposureCompensationDesc; break;
                case EDSDKLib.EDSDK.PropID_ImageQuality: desc = this.ImageQualityDesc; break;
                case EDSDKLib.EDSDK.PropID_Evf_AFMode: desc = this.EvfAFModeDesc; break;
                case EDSDKLib.EDSDK.PropID_DC_Zoom: desc = this.ZoomDesc; break;
                case EDSDKLib.EDSDK.PropID_DC_Strobe: desc = this.FlashModeDesc; break;
                case EDSDKLib.EDSDK.PropID_MovieParam: desc = this.MovieQualityDesc; break;
            }
            return desc;
        }

        public void OnPropertyChanged(CameraPropertyChangedEventArgs e)
        {
            this.PropertyChanged(this, e);
        }

        public void OnPropertyDescChanged(CameraPropertyDescChangedEventArgs e)
        {
            this.PropertyDescChanged(this, e);
        }
    }
}
