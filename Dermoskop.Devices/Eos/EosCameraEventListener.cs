﻿using System;
using System.Runtime.InteropServices;

namespace Dermoskop.Devices
{
    class EosCameraEventListener
    {
        public static uint HandleObjectEvent(uint inEvent, IntPtr inRef, IntPtr inContext)
        {
            GCHandle handle = GCHandle.FromIntPtr(inContext);
            EosCameraController controller = (EosCameraController)handle.Target;

            switch (inEvent)
            {
                //case EDSDKLib.EDSDK.ObjectEvent_DirItemCreated :
                case EDSDKLib.EDSDK.ObjectEvent_DirItemRequestTransfer:

                    FireEvent(ref controller, ActionRequest.Command.CAMERA_DOWNLOAD, inRef);
                    break;

                default:
                    //Object without the necessity is released
                    if (inRef != IntPtr.Zero)
                    {
                        EDSDKLib.EDSDK.EdsRelease(inRef);
                    }
                    break;
            }
            return EDSDKLib.EDSDK.EDS_ERR_OK;
        }


        public static uint HandlePropertyEvent(uint inEvent, uint inPropertyID, uint inParam, IntPtr inContext)
        {
            GCHandle handle = GCHandle.FromIntPtr(inContext);
            EosCameraController controller = (EosCameraController)handle.Target;

            switch (inEvent)
            {
                case EDSDKLib.EDSDK.PropertyEvent_PropertyChanged:
                    FireEvent(ref controller, ActionRequest.Command.CAMERA_GET_PROPERTY, (IntPtr)inPropertyID);
                    break;

                case EDSDKLib.EDSDK.PropertyEvent_PropertyDescChanged:
                    FireEvent(ref controller, ActionRequest.Command.CAMERA_GET_PROPERTYDESC, (IntPtr)inPropertyID);
                    break;
            }
            return EDSDKLib.EDSDK.EDS_ERR_OK;
        }


        public static uint HandleStateEvent(uint inEvent, uint inParameter, IntPtr inContext)
        {
            GCHandle handle = GCHandle.FromIntPtr(inContext);
            EosCameraController controller = (EosCameraController)handle.Target;

            switch (inEvent)
            {
                case EDSDKLib.EDSDK.StateEvent_Shutdown:
                    FireEvent(ref controller, ActionRequest.Command.SHUT_DOWN, IntPtr.Zero);
                    break;
            }
            return EDSDKLib.EDSDK.EDS_ERR_OK;
        }

        private static void FireEvent(ref EosCameraController controller, ActionRequest.Command command, IntPtr arg)
        {
            ActionRequest e = new ActionRequest(command, new NativeActionRequestArgs(arg));
            controller.RequestAction(e);
        }
    }
}
