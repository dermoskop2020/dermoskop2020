﻿using System;

namespace Dermoskop.Devices
{
    public class EosException : Exception
    {
        uint errorCode;

        public EosException(uint errorCode) : base(EosFramework.ConvertErrorCodeToString(errorCode))
        {
            this.errorCode = errorCode;
        }
    }
}
