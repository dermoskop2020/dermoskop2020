﻿using System;

namespace Dermoskop.Devices
{
    /// <summary>
    /// Executes commands.
    /// </summary>
    public class EosCameraController : CameraController
    {
        public EosCameraController(EosCameraDevice camera) : base(camera) { }

        /// <summary>
        /// Enable the device and begin execution of the commands.
        /// </summary>
        public override void Run()
        {
            base.Run();

            processor.PostCommand(new EosOpenSessionCommand((EosCameraDevice)Device));

            processor.PostCommand(new EosGetPropertyCommand((EosCameraDevice)Device, EDSDKLib.EDSDK.PropID_ProductName));

            if (((EosCameraDevice)Device).IsTypeDS)
            {
                processor.PostCommand(new EosSetRemoteShootingCommand((EosCameraDevice)Device, (uint)IntPtr.Zero));
            }

        }

        /// <summary>
        /// Request an action to be executed by the device.
        /// </summary>
        public override void RequestAction(ActionRequest a)
        {
            ActionRequest.Command command = a.Cmd;

            uint inPropertyID;
            switch (command)
            {
                case ActionRequest.Command.CAMERA_GET_PROPERTY:
                    inPropertyID = (uint)((NativeActionRequestArgs)a.Arg).Pointer;
                    processor.PostCommand(new EosGetPropertyCommand((EosCameraDevice)Device, inPropertyID));
                    break;

                case ActionRequest.Command.CAMERA_GET_PROPERTYDESC:
                    inPropertyID = (uint)((NativeActionRequestArgs)a.Arg).Pointer;
                    processor.PostCommand(new EosGetPropertyDescCommand((EosCameraDevice)Device, inPropertyID));
                    break;

                case ActionRequest.Command.CAMERA_TAKE_PICTURE:
                    processor.PostCommand(new EosTakePictureCommand((EosCameraDevice)Device));
                    break;

                case ActionRequest.Command.CAMERA_DOWNLOAD:
                    IntPtr inRef = ((NativeActionRequestArgs)a.Arg).Pointer;
                    processor.PostCommand(new EosDownloadCommand((EosCameraDevice)Device, ref inRef));
                    break;

                case ActionRequest.Command.CLOSING:
                    processor.PostCommand(new EosCloseSessionCommand((EosCameraDevice)Device));
                    processor.Stop();
                    break;

                case ActionRequest.Command.SHUT_DOWN:
                    processor.Stop();
                    Device.OnShutDown(EventArgs.Empty);
                    break;

                case ActionRequest.Command.CAMERA_REMOTESHOOTING_START:
                    processor.PostCommand(new EosSetRemoteShootingCommand((EosCameraDevice)Device, (uint)EDSDKLib.EDSDK.DcRemoteShootingMode.DcRemoteShootingModeStart));
                    break;

                case ActionRequest.Command.CAMERA_REMOTESHOOTING_STOP:
                    processor.PostCommand(new EosSetRemoteShootingCommand((EosCameraDevice)Device, (uint)EDSDKLib.EDSDK.DcRemoteShootingMode.DcRemoteShootingModeStop));
                    break;
            }
        }
    }
}
