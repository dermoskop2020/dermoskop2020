﻿using System;
using System.Drawing.Design;
using System.Reflection;

namespace Dermoskop.Devices
{
    /// <summary>
    /// Executes commands.
    /// </summary>
    public class DeviceController
    {
        private protected CommandProcessor processor = new CommandProcessor();
        public Device Device { get; protected set; }

        public DeviceController(Device device)
        {
            this.Device = device;
        }

        /// <summary>
        /// Enable the device and begin execution of the commands.
        /// </summary>
        public virtual void Run()
        {
            processor.Start();
        }

        /// <summary>
        /// Request an action to be executed by the device.
        /// </summary>
        public virtual void RequestAction(ActionRequest a) { }
    }
}
