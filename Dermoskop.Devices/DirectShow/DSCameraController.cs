﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dermoskop.Devices
{
    /// <summary>
    /// Executes commands.
    /// </summary>
    public class DSCameraController : VideoController
    {
        public DSCameraController(DSCameraDevice camera) : base(camera) { }

        /// <summary>
        /// Enable the device and begin execution of the commands.
        /// </summary>
        public override void Run()
        {
            base.Run();

            processor.PostCommand(new DSOpenSessionCommand((DSCameraDevice)Device));
        }

        /// <summary>
        /// Request an action to be executed by the device.
        /// </summary>
        public override void RequestAction(ActionRequest a)
        {
            ActionRequest.Command command = a.Cmd;
            DSCameraDevice camera = (DSCameraDevice)Device;

            switch (command)
            {
                case ActionRequest.Command.CAMERA_TAKE_PICTURE:
                    processor.PostCommand(new DSTakePictureCommand((DSCameraDevice)Device));
                    break;

                case ActionRequest.Command.CAMERA_VIDEO_START:
                    processor.PostCommand(new DSOpenSessionCommand((DSCameraDevice)Device));
                    break;

                case ActionRequest.Command.CAMERA_VIDEO_STOP:
                    processor.PostCommand(new DSCloseSessionCommand((DSCameraDevice)Device));
                    break;

                case ActionRequest.Command.CLOSING:
                    processor.PostCommand(new DSCloseSessionCommand((DSCameraDevice)Device));
                    processor.Stop();
                    break;

                case ActionRequest.Command.SHUT_DOWN:
                    camera.VideoDevice.Stop();
                    break;
            }
        }
    }
}
