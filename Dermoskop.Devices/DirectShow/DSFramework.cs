﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accord.Video.DirectShow;

namespace Dermoskop.Devices
{
    /// <summary>
    /// Global DirectShow helper class.
    /// </summary>
    /// 
    public static class DSFramework
    {
        /// <summary>
        /// Get all DirectShow cameras that support snapshots.
        /// </summary>
        /// <returns>Returns List of DSCameraDevices.</returns>
        public static List<DSCameraDevice> GetCameraDevices()
        {
            List<DSCameraDevice> devices = new List<DSCameraDevice>();

            // enumerate video devices
            FilterInfoCollection videoDevicesInfo = new FilterInfoCollection(FilterCategory.VideoInputDevice);

            foreach (FilterInfo device in videoDevicesInfo)
            {
                VideoCaptureDevice videoDevice = new VideoCaptureDevice(device.MonikerString);
                VideoCapabilities[] snapshot = videoDevice.SnapshotCapabilities;
                if (snapshot.Length > 0)
                {
                    devices.Add(new DSCameraDevice(videoDevice));
                }
            }

            if (devices.Count == 0)
            {
                // TODO
                throw new Exception("");
            }

            return devices;
        }

        public static List<DSCameraDevice> GetCameraDevicesForm()
        {
            List<DSCameraDevice> devices = new List<DSCameraDevice>();
            VideoCaptureDeviceForm videoCaptureDeviceForm = new VideoCaptureDeviceForm();
            videoCaptureDeviceForm.ConfigureSnapshots = true;
            
            if (videoCaptureDeviceForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                devices.Add(new DSCameraDevice(videoCaptureDeviceForm.VideoDevice));
            }

            if (devices.Count == 0)
            {
                // TODO
                throw new Exception("");
            }

            return devices;
        }
    }
}
