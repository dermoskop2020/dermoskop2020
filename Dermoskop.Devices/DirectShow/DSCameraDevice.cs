﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Accord.Video;
using Accord.Video.DirectShow;

namespace Dermoskop.Devices
{
    public class DSCameraDevice : VideoDevice
    {
        public VideoCaptureDevice VideoDevice { get; }

        public DSCameraDevice(VideoCaptureDevice videoDevice) : base()
        {
            this.VideoDevice = videoDevice;
            this.VideoDevice.ProvideSnapshots = true;
            this.VideoDevice.NewFrame += VideoCaptureDevice_OnNewFrame;
            this.VideoDevice.SnapshotFrame += VideoCaptureDevice_OnSnapshotFrame;
        }

        private void VideoCaptureDevice_OnNewFrame(object sender, NewFrameEventArgs e)
        {
            this.OnNewVideoFrame(new DownloadEventArgs(DownloadEventArgs.DownloadStatus.COMPLETED, (Bitmap)e.Frame.Clone()));
        }

        private void VideoCaptureDevice_OnSnapshotFrame(object sender, NewFrameEventArgs e)
        {
            this.OnDownload(new DownloadEventArgs(DownloadEventArgs.DownloadStatus.COMPLETED, (Bitmap)e.Frame.Clone()));
        }
    }
}
