using System;

namespace Dermoskop.Devices
{
    class EosGetPropertyDescCommand : Command
    {

        private uint propertyID;

        public EosGetPropertyDescCommand(EosCameraDevice device, uint propertyID)
            : base(device)
        {
            this.propertyID = propertyID;

        }

        public override bool Execute()
        {
            uint err = EDSDKLib.EDSDK.EDS_ERR_OK;

            // Get property
            err = this.GetPropertyDesc(propertyID);


            // Notification of error
            if (err != EDSDKLib.EDSDK.EDS_ERR_OK)
            {

                // Retry getting image data if EDS_ERR_OBJECT_NOTREADY is returned when the image data is not ready yet.
                if (err == EDSDKLib.EDSDK.EDS_ERR_OBJECT_NOTREADY)
                {
                    return false;
                }

                // It retries it at device busy
                if (err == EDSDKLib.EDSDK.EDS_ERR_DEVICE_BUSY)
                {
                    device.OnDeviceBusy(new DeviceErrorEventArgs("Device is busy"));
                    return false;
                }

                {
                    //device.OnError(new DeviceErrorEventArgs("Get property desc command: " + EosFramework.ConvertErrorCodeToString(err)));
                }
            }
            return true;
        }



        private uint GetPropertyDesc(uint propertyID)
        {
            uint err = EDSDKLib.EDSDK.EDS_ERR_OK;
            EDSDKLib.EDSDK.EdsPropertyDesc propertyDesc = new EDSDKLib.EDSDK.EdsPropertyDesc();

            if (propertyID == EDSDKLib.EDSDK.PropID_Unknown)
            {
                // If unknown is returned for the property ID , the required property must be retrieved again
                if (err == EDSDKLib.EDSDK.EDS_ERR_OK) err = this.GetPropertyDesc(EDSDKLib.EDSDK.PropID_AEModeSelect);
                if (err == EDSDKLib.EDSDK.EDS_ERR_OK) err = this.GetPropertyDesc(EDSDKLib.EDSDK.PropID_DriveMode);
                if (err == EDSDKLib.EDSDK.EDS_ERR_OK) err = this.GetPropertyDesc(EDSDKLib.EDSDK.PropID_WhiteBalance);
                if (err == EDSDKLib.EDSDK.EDS_ERR_OK) err = this.GetPropertyDesc(EDSDKLib.EDSDK.PropID_Tv);
                if (err == EDSDKLib.EDSDK.EDS_ERR_OK) err = this.GetPropertyDesc(EDSDKLib.EDSDK.PropID_Av);
                if (err == EDSDKLib.EDSDK.EDS_ERR_OK) err = this.GetPropertyDesc(EDSDKLib.EDSDK.PropID_ISOSpeed);
                if (err == EDSDKLib.EDSDK.EDS_ERR_OK) err = this.GetPropertyDesc(EDSDKLib.EDSDK.PropID_MeteringMode);
                if (err == EDSDKLib.EDSDK.EDS_ERR_OK) err = this.GetPropertyDesc(EDSDKLib.EDSDK.PropID_ExposureCompensation);
                if (err == EDSDKLib.EDSDK.EDS_ERR_OK) err = this.GetPropertyDesc(EDSDKLib.EDSDK.PropID_ImageQuality);
                if (err == EDSDKLib.EDSDK.EDS_ERR_OK) err = this.GetPropertyDesc(EDSDKLib.EDSDK.PropID_Evf_AFMode);
                if (err == EDSDKLib.EDSDK.EDS_ERR_OK) err = this.GetPropertyDesc(EDSDKLib.EDSDK.PropID_MovieParam);

                return err;
            }

            EosCameraDevice camera = (EosCameraDevice)device;

            // Acquisition of value list that can be set
            if (err == EDSDKLib.EDSDK.EDS_ERR_OK)
            {
                err = EDSDKLib.EDSDK.EdsGetPropertyDesc(camera.Camera, propertyID, out propertyDesc);
            }

            // The value list that can be the acquired setting it is set		
            if (err == EDSDKLib.EDSDK.EDS_ERR_OK)
            {
                camera.SetPropertyDesc(propertyID, ref propertyDesc);
            }

            // Update notification
            if (err == EDSDKLib.EDSDK.EDS_ERR_OK)
            {
                camera.OnPropertyDescChanged(new CameraPropertyDescChangedEventArgs((IntPtr)propertyID));

            }
            return err;
        }
    }
}

