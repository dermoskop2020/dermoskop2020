﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dermoskop.Devices
{
    class SerialCaptureStop : SerialCommand
    {
        public SerialCaptureStop(SerialDevice device) : base(device, new byte[] { (byte)SerialFramework.Command.CAPTURE_STOP, 0x00 }) { }
    }
}
