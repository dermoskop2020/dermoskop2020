﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dermoskop.Devices
{
    class SerialStopCommand : SerialCommand
    {
        public SerialStopCommand(SerialDevice device) : base(device, new byte[] { (byte)SerialFramework.Command.FULLSTOP, 0x00 }) { }

        public override bool Execute()
        {
            SerialDevice serialDevice = (SerialDevice)device;
            try
            {
                byte response, data;
                WriteBufferToPort();
                ReadFromPort(out response, out data);

                if (response == (byte)SerialFramework.ErrorCode.OK)
                {
                    serialDevice.OnStopped(EventArgs.Empty);
                    return true;
                }
            }
            catch (TimeoutException)
            {
                serialDevice.OnError(new DeviceErrorEventArgs("Serial device timeout."));
                return true;
            }

            return true;
        }
    }
}
