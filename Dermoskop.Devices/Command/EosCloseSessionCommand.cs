using System;

namespace Dermoskop.Devices
{
    class EosCloseSessionCommand : Command
    {
        public EosCloseSessionCommand(EosCameraDevice device) : base(device) { }

        public override bool Execute()
        {
            // The communication with the camera ends
            uint err = EDSDKLib.EDSDK.EdsCloseSession(((EosCameraDevice)device).Camera);

            // Notification of error
            if (err != EDSDKLib.EDSDK.EDS_ERR_OK)
            {
                device.OnError(new DeviceErrorEventArgs("Close session command: " + EosFramework.ConvertErrorCodeToString(err)));
            }
            return true;
        }
    }
}
