﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dermoskop.Devices
{
    class SerialMoveCommand : SerialCommand
    {
        public SerialMoveCommand(SerialDevice device) : base(device, new byte[] { (byte)SerialFramework.Command.MOVE, 0x00 }) { }

        public override bool Execute()
        {
            SerialDevice serialDevice = (SerialDevice)device;

            try
            {
                byte response, data;
                WriteBufferToPort();
                ReadFromPort(out response, out data);

                if (response == (byte)SerialFramework.ErrorCode.BUSY)
                {
                    serialDevice.OnDeviceBusy(new DeviceErrorEventArgs("Serial device is busy."));
                    return true;
                }
                else if(response == (byte)SerialFramework.Event.CAPTURE_DONE)
                {
                    serialDevice.OnCaptureDone(EventArgs.Empty);
                    return true;
                }
            }
            catch (TimeoutException)
            {
                serialDevice.OnError(new DeviceErrorEventArgs("Serial device timeout."));
                return true;
            }

            return true;
        }
    }
}
