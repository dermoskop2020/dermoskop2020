﻿using System;

namespace Dermoskop.Devices
{
    class Command
    {
        protected Device device;
        public Command(Device device)
        {
            this.device = device;
        }

        public Device GetDevice()
        {
            return device;
        }

        public virtual bool Execute()
        {
            return true;
        }
    }
}
