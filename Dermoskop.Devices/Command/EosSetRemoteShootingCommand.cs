﻿using System;

namespace Dermoskop.Devices
{
    class EosSetRemoteShootingCommand : Command
    {
        private uint parameter;

        public EosSetRemoteShootingCommand(EosCameraDevice device, uint parameter) : base(device)
        {
            this.parameter = parameter;
        }

        // Execute command	
        public override bool Execute()
        {
            EosCameraDevice camera = (EosCameraDevice)device;
            uint outPropertyData;
            uint err = EDSDKLib.EDSDK.EdsGetPropertyData(camera.Camera, EDSDKLib.EDSDK.PropID_LensBarrelStatus, 0, out outPropertyData);
            if (err != EDSDKLib.EDSDK.EDS_ERR_OK || outPropertyData == (uint)EDSDKLib.EDSDK.DcRemoteShootingMode.DcRemoteShootingModeStart)
            {
                return true;
            }

            err = EDSDKLib.EDSDK.EdsSendCommand(camera.Camera, EDSDKLib.EDSDK.CameraCommand_SetRemoteShootingMode, (int)parameter);

            // Notification of error
            if (err != EDSDKLib.EDSDK.EDS_ERR_OK)
            {
                // It retries it at device busy
                if (err == EDSDKLib.EDSDK.EDS_ERR_DEVICE_BUSY)
                {
                    device.OnDeviceBusy(new DeviceErrorEventArgs("Device is busy."));
                    return false;
                }
                else
                {
                    device.OnError(new DeviceErrorEventArgs("Set remote shooting command: " + EosFramework.ConvertErrorCodeToString(err)));
                    return true;
                }
            }

            return true;
        }
    }
}
