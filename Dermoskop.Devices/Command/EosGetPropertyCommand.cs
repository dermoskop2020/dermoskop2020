using System;

namespace Dermoskop.Devices
{
    class EosGetPropertyCommand : Command
    {
        private uint propertyID;

        public EosGetPropertyCommand(EosCameraDevice device, uint propertyID): base(device)
        {
            this.propertyID = propertyID;
        }

        public override bool Execute()
        {
            uint err = EDSDKLib.EDSDK.EDS_ERR_OK;

            // Get property value
            err = GetProperty(propertyID);


            if (err != EDSDKLib.EDSDK.EDS_ERR_OK)
            {
                // It retries it at device busy
                if (err == EDSDKLib.EDSDK.EDS_ERR_DEVICE_BUSY)
                {
                    device.OnDeviceBusy(new DeviceErrorEventArgs("Device is busy"));
                    return false;
                }
                else
                {
                    //device.OnError(new DeviceErrorEventArgs("Get property command: " + EosFramework.ConvertErrorCodeToString(err) + " property id: " + propertyID.ToString()));
                    return true;
                }
            }
            return true;
        }

        private uint GetProperty(uint propertyID)
        {
            uint err = EDSDKLib.EDSDK.EDS_ERR_OK;
            EDSDKLib.EDSDK.EdsDataType dataType = EDSDKLib.EDSDK.EdsDataType.Unknown;
            int dataSize = 0;

            if (propertyID == EDSDKLib.EDSDK.PropID_Unknown)
            {
                // If unknown is returned for the property ID , the required property must be retrieved again
                if (err == EDSDKLib.EDSDK.EDS_ERR_OK) err = GetProperty(EDSDKLib.EDSDK.PropID_AEModeSelect);
                if (err == EDSDKLib.EDSDK.EDS_ERR_OK) err = GetProperty(EDSDKLib.EDSDK.PropID_DriveMode);
                if (err == EDSDKLib.EDSDK.EDS_ERR_OK) err = GetProperty(EDSDKLib.EDSDK.PropID_WhiteBalance);
                if (err == EDSDKLib.EDSDK.EDS_ERR_OK) err = GetProperty(EDSDKLib.EDSDK.PropID_Tv);
                if (err == EDSDKLib.EDSDK.EDS_ERR_OK) err = GetProperty(EDSDKLib.EDSDK.PropID_Av);
                if (err == EDSDKLib.EDSDK.EDS_ERR_OK) err = GetProperty(EDSDKLib.EDSDK.PropID_ISOSpeed);
                if (err == EDSDKLib.EDSDK.EDS_ERR_OK) err = GetProperty(EDSDKLib.EDSDK.PropID_MeteringMode);
                if (err == EDSDKLib.EDSDK.EDS_ERR_OK) err = GetProperty(EDSDKLib.EDSDK.PropID_ExposureCompensation);
                if (err == EDSDKLib.EDSDK.EDS_ERR_OK) err = GetProperty(EDSDKLib.EDSDK.PropID_ImageQuality);
                if (err == EDSDKLib.EDSDK.EDS_ERR_OK) err = GetProperty(EDSDKLib.EDSDK.PropID_AvailableShots);
                if (err == EDSDKLib.EDSDK.EDS_ERR_OK) err = GetProperty(EDSDKLib.EDSDK.PropID_BatteryLevel);
                if (err == EDSDKLib.EDSDK.EDS_ERR_OK) err = GetProperty(EDSDKLib.EDSDK.PropID_TempStatus);
                if (err == EDSDKLib.EDSDK.EDS_ERR_OK) err = GetProperty(EDSDKLib.EDSDK.PropID_MovieParam);
                if (err == EDSDKLib.EDSDK.EDS_ERR_OK) err = GetProperty(EDSDKLib.EDSDK.PropID_Evf_ClickWBCoeffs);
                if (err == EDSDKLib.EDSDK.EDS_ERR_OK) err = GetProperty(EDSDKLib.EDSDK.PropID_FixedMovie);
                if (err == EDSDKLib.EDSDK.EDS_ERR_OK) err = GetProperty(EDSDKLib.EDSDK.PropID_MirrorUpSetting);
                if (err == EDSDKLib.EDSDK.EDS_ERR_OK) err = GetProperty(EDSDKLib.EDSDK.PropID_MirrorLockUpState);

                return err;
            }

            EosCameraDevice camera = (EosCameraDevice)device;
                
            err = EDSDKLib.EDSDK.EdsGetPropertySize(camera.Camera, propertyID, 0, out dataType, out dataSize);

            if (err == EDSDKLib.EDSDK.EDS_ERR_OK)
            {
                if (dataType == EDSDKLib.EDSDK.EdsDataType.UInt32)
                {
                    uint data;

                    // Acquisition of the property
                    err = EDSDKLib.EDSDK.EdsGetPropertyData(camera.Camera, propertyID, 0, out data);

                    // Acquired property value is set
                    if (err == EDSDKLib.EDSDK.EDS_ERR_OK)
                    {
                        camera.SetPropertyUInt32(propertyID, data);
                    }
                }

                else if (dataType == EDSDKLib.EDSDK.EdsDataType.Int32)
                {
                    uint data;

                    // Acquisition of the property
                    err = EDSDKLib.EDSDK.EdsGetPropertyData(camera.Camera, propertyID, 0, out data);

                    // Acquired property value is set
                    if (err == EDSDKLib.EDSDK.EDS_ERR_OK)
                    {
                        camera.SetPropertyInt32(propertyID, data);
                    }
                }

                else if (dataType == EDSDKLib.EDSDK.EdsDataType.String)
                {
                    String str;

                    // Acquisition of the property
                    err = EDSDKLib.EDSDK.EdsGetPropertyData(camera.Camera, propertyID, 0, out str);

                    // Acquired property value is set
                    if (err == EDSDKLib.EDSDK.EDS_ERR_OK)
                    {
                        camera.SetPropertyString(propertyID, str);
                    }
                }

                else if (dataType == EDSDKLib.EDSDK.EdsDataType.FocusInfo)
                {
                    EDSDKLib.EDSDK.EdsFocusInfo focusInfo;

                    // Acquisition of the property
                    err = EDSDKLib.EDSDK.EdsGetPropertyData(camera.Camera, propertyID, 0, out focusInfo);

                    // Acquired property value is set
                    if (err == EDSDKLib.EDSDK.EDS_ERR_OK)
                    {
                        camera.SetPropertyFocusInfo(propertyID, focusInfo);
                    }
                }

                else if (dataType == EDSDKLib.EDSDK.EdsDataType.ByteBlock)
                {
                    byte[] data = new byte[dataSize];

                    // Acquisition of the property
                    err = EDSDKLib.EDSDK.EdsGetPropertyData(camera.Camera, propertyID, 0, out data);

                    // Acquired property value is set
                    if (err == EDSDKLib.EDSDK.EDS_ERR_OK)
                    {
                        camera.SetPropertyByteBlock(propertyID, data);
                    }
                }
            }

            else if (dataType == EDSDKLib.EDSDK.EdsDataType.FocusInfo || err == EDSDKLib.EDSDK.EDS_ERR_PROPERTIES_UNAVAILABLE)
            {
                camera.FocusInfo = default(EDSDKLib.EDSDK.EdsFocusInfo);
                err = EDSDKLib.EDSDK.EDS_ERR_OK;
            }

            // Update notification
            if (err == EDSDKLib.EDSDK.EDS_ERR_OK)
            {
                camera.OnPropertyChanged(new CameraPropertyChangedEventArgs((IntPtr)propertyID));
            }

            return err;
        }
    }
}
