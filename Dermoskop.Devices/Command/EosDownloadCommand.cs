using System;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;

namespace Dermoskop.Devices
{
    class EosDownloadCommand : Command
    {
        private IntPtr directoryItem = IntPtr.Zero;

        public EosDownloadCommand(EosCameraDevice device, ref IntPtr inRef) : base(device)
        {
            directoryItem = inRef;
        }

        ~EosDownloadCommand() 
        {
            if (directoryItem != IntPtr.Zero)
            {
                EDSDKLib.EDSDK.EdsRelease(directoryItem);
                directoryItem = IntPtr.Zero;
            }
        }

		public override bool Execute()
		{
			EosCameraDevice camera = (EosCameraDevice)device;
			uint err = EDSDKLib.EDSDK.EDS_ERR_OK;
			IntPtr stream = IntPtr.Zero;
			IntPtr imagePtr = IntPtr.Zero;
			UInt64 imageLen = 0;
			Bitmap bitmap = null;

			// Acquisition of the downloaded image information
			EDSDKLib.EDSDK.EdsDirectoryItemInfo dirItemInfo;
			err = EDSDKLib.EDSDK.EdsGetDirectoryItemInfo(directoryItem, out dirItemInfo);

			// Forwarding beginning notification	
			if (err == EDSDKLib.EDSDK.EDS_ERR_OK)
			{
				camera.OnDownload(new DownloadEventArgs(DownloadEventArgs.DownloadStatus.STARTED, bitmap));
			}

			// Make the memory stream at the forwarding destination
			if (err == EDSDKLib.EDSDK.EDS_ERR_OK)
			{
				// Buffer size set to 0, it should be automaticaly resized
				err = EDSDKLib.EDSDK.EdsCreateMemoryStream(0, out stream);
			}

			// Set Progress
			if (err == EDSDKLib.EDSDK.EDS_ERR_OK)
			{
				err = EDSDKLib.EDSDK.EdsSetProgressCallback(stream, ProgressFunc, EDSDKLib.EDSDK.EdsProgressOption.Periodically, stream);
			}

			// Download image
			if (err == EDSDKLib.EDSDK.EDS_ERR_OK)
			{
				err = EDSDKLib.EDSDK.EdsDownload(directoryItem, dirItemInfo.Size, stream);
			}

			// Forwarding completion
			if (err == EDSDKLib.EDSDK.EDS_ERR_OK)
			{
				err = EDSDKLib.EDSDK.EdsDownloadComplete(directoryItem);
			}

			// Get pointer to the image data
			if (err == EDSDKLib.EDSDK.EDS_ERR_OK)
			{
				err = EDSDKLib.EDSDK.EdsGetPointer(stream, out imagePtr);
			}

			// Get image data lenght
			if (err == EDSDKLib.EDSDK.EDS_ERR_OK)
			{
				err = EDSDKLib.EDSDK.EdsGetLength(stream, out imageLen);
			}

			// Convert image data to Bitmap
			if (err == EDSDKLib.EDSDK.EDS_ERR_OK)
			{
				byte[] imageData = new byte[(int)imageLen];
				Marshal.Copy(imagePtr, imageData, 0, (int)imageLen);
				using (MemoryStream memoryStream = new MemoryStream(imageData))
				{
					bitmap = new Bitmap(memoryStream);
				}
			}

			// Forwarding completion notification
			if (bitmap != null)
			{
				camera.OnDownload(new DownloadEventArgs(DownloadEventArgs.DownloadStatus.COMPLETED, bitmap));
			}

			// Release Item
			if (directoryItem != IntPtr.Zero)
			{
				err = EDSDKLib.EDSDK.EdsRelease(directoryItem);
				directoryItem = IntPtr.Zero;
			}

			// Release stream
			if (stream != IntPtr.Zero)
			{
				err = EDSDKLib.EDSDK.EdsRelease(stream);
				stream = IntPtr.Zero;
			}

			// Notification of error
			if (err != EDSDKLib.EDSDK.EDS_ERR_OK || bitmap == null)
			{
				device.OnError(new DeviceErrorEventArgs("Download failed: " + EosFramework.ConvertErrorCodeToString(err)));
			}
			return true;
	    }

        private uint ProgressFunc(uint inPercent, IntPtr inContext, ref bool outCancel)
        {
			device.OnProgressReport(new ProgressReportEventArgs((int)inPercent, ref outCancel));
            return 0;
        }
    }
}
