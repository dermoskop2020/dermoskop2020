﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dermoskop.Devices
{
    class SerialIdCommand : SerialCommand
    {
        public SerialIdCommand(SerialDevice device) : base(device, new byte[] { (byte)SerialFramework.Command.ID, 0x00 }) { }

        public override bool Execute()
        {
            SerialDevice serialDevice = (SerialDevice)device;
            try
            {
                byte response, data;
                WriteBufferToPort();
                ReadFromPort(out response, out data);
                short decoded = BitConverter.ToInt16(new byte[] { response, data }, 0);
                if (decoded != 31415)
                {
                    serialDevice.OnIdEvent(new SerialDeviceEventArgs((byte)0));
                }
                else
                {
                    serialDevice.OnIdEvent(new SerialDeviceEventArgs((byte)SerialFramework.ErrorCode.OK));
                }
            }
            catch (TimeoutException)
            {
                serialDevice.OnIdEvent(new SerialDeviceEventArgs((byte)SerialFramework.ErrorCode.TIME_OUT));
            }
 
            return true;
        }
    }
}
