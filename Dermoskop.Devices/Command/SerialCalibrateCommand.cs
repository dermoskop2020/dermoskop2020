﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dermoskop.Devices
{
    class SerialCalibrateCommand : SerialCommand
    {
        public SerialCalibrateCommand(SerialDevice device) : base(device, new byte[] { (byte)SerialFramework.Command.CALIBRATE, 0x00 }) { }
    }
}
