﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dermoskop.Devices
{
    class DSOpenSessionCommand : Command
    {
        public DSOpenSessionCommand(DSCameraDevice device) : base(device) { }

        public override bool Execute()
        {
            DSCameraDevice camera = (DSCameraDevice)device;

            camera.VideoDevice.Start();

            return true;
        }
    }
}
