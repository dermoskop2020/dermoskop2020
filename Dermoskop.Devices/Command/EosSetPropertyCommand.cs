using System;
using System.Runtime.InteropServices;

namespace Dermoskop.Devices
{
    class EosSetPropertyCommand : Command
    {
        private uint propertyID;
        private object data;

        public EosSetPropertyCommand(EosCameraDevice device, uint propertyID, object data) : base(device)
        {
            this.propertyID = propertyID;
            this.data = data;
        }

        public override bool Execute()
        {
            uint err = EDSDKLib.EDSDK.EDS_ERR_OK;

            err = EDSDKLib.EDSDK.EdsSetPropertyData(((EosCameraDevice)device).Camera, propertyID, 0, Marshal.SizeOf(data), data);

            if (err != EDSDKLib.EDSDK.EDS_ERR_OK)
            {
                // It retries it at device busy
                if (err == EDSDKLib.EDSDK.EDS_ERR_DEVICE_BUSY)
                {
                    device.OnDeviceBusy(new DeviceErrorEventArgs("Device is busy"));
                    return false;
                }

                {
                    device.OnError(new DeviceErrorEventArgs("Set property command: " + EosFramework.ConvertErrorCodeToString(err)));
                }
            }
            return true;
        }
    }
}
