﻿using System;

namespace Dermoskop.Devices
{
    class EosTakePictureCommand : Command
    {
        public EosTakePictureCommand(EosCameraDevice device) : base(device) { }

        public override bool Execute()
        {
            EosCameraDevice camera = (EosCameraDevice)device;

            // Taking a picture
            uint err = EDSDKLib.EDSDK.EdsSendCommand(camera.Camera, EDSDKLib.EDSDK.CameraCommand_PressShutterButton, (int)EDSDKLib.EDSDK.EdsShutterButton.CameraCommand_ShutterButton_Completely);
            err = EDSDKLib.EDSDK.EdsSendCommand(camera.Camera, EDSDKLib.EDSDK.CameraCommand_PressShutterButton, (int)EDSDKLib.EDSDK.EdsShutterButton.CameraCommand_ShutterButton_OFF);

            // Notification of error
            if (err != EDSDKLib.EDSDK.EDS_ERR_OK)
            {
                // It retries it at device busy
                if (err == EDSDKLib.EDSDK.EDS_ERR_DEVICE_BUSY)
                {
                    device.OnDeviceBusy(new DeviceErrorEventArgs("Device is busy."));
                    return false;
                }
                else
                {
                    device.OnError(new DeviceErrorEventArgs("Take picture command: " + EosFramework.ConvertErrorCodeToString(err)));
                    return true;
                }
            }
            camera.OnPictureTaken();

            return true;
        }


    }
}
