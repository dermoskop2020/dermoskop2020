﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dermoskop.Devices
{
    class SerialGetProperty : SerialCommand
    {
        public SerialGetProperty(SerialDevice device, byte propertyId) : base(device, new byte[] { (byte)SerialFramework.Command.GET_PROPERTY, propertyId }) { }

        public override bool Execute()
        {
            SerialDevice serialDevice = (SerialDevice)device;

            try
            {
                byte response, data;
                WriteBufferToPort();
                ReadFromPort(out response, out data);

                if (response == (byte)SerialFramework.ErrorCode.BUSY)
                {
                    serialDevice.OnDeviceBusy(new DeviceErrorEventArgs("Serial device is busy."));
                    return true;
                }
                else if (response == (byte)SerialFramework.Event.GET_PROPERTY_NUM_OF_STEPS)
                {
                    serialDevice.OnGetProperty(new SerialDeviceEventArgs(data));
                    return true;
                }
            }
            catch (TimeoutException)
            {
                serialDevice.OnError(new DeviceErrorEventArgs("Serial device timeout."));
                return true;
            }

            return true;
        }
    }
}
