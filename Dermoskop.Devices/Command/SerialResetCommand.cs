﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dermoskop.Devices
{
    class SerialResetCommand : SerialCommand
    {
        public SerialResetCommand(SerialDevice device) : base(device, new byte[] { (byte)SerialFramework.Command.RESET, 0x00 }) { }
    }
}
