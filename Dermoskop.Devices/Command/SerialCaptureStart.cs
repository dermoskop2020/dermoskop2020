﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dermoskop.Devices
{
    class SerialCaptureStart : SerialCommand
    {
        public SerialCaptureStart(SerialDevice device) : base(device, new byte[] { (byte)SerialFramework.Command.CAPTURE_START, 0x00 }) { }
    }
}
