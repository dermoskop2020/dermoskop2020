﻿using System;
using System.Threading;

namespace Dermoskop.Devices
{
    class SerialCommand : Command
    {
        protected byte[] buffer;

        public SerialCommand(SerialDevice device, byte[] bytes) : base(device)
        {
            this.buffer = bytes;
        }

        public override bool Execute()
        {
            SerialDevice serialDevice = (SerialDevice)device;

            try
            {
                byte response, data;
                WriteBufferToPort();
                ReadFromPort(out response, out data);

                if (response == (byte)SerialFramework.ErrorCode.BUSY)
                {
                    serialDevice.OnDeviceBusy(new DeviceErrorEventArgs("Serial device is busy. (SerialCommand: " + buffer[0].ToString() + ")"));
                    return true;
                }
            }
            catch (TimeoutException)
            {
                serialDevice.OnError(new DeviceErrorEventArgs("Serial device timeout. (SerialCommand: " + buffer[0].ToString() + ")"));
                return true;
            }

            return true;
        }

        protected void WriteBufferToPort()
        {
            ((SerialDevice)device).Port.Write(buffer, 0, 2);
        }

        protected void ReadFromPort(out byte response, out byte data)
        {
            response = (byte)((SerialDevice)device).Port.ReadByte();
            data = (byte)((SerialDevice)device).Port.ReadByte();
        }
    }
}
