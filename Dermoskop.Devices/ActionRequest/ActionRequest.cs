﻿using System;

namespace Dermoskop.Devices
{
    /// <summary>
    /// Represents wrapper for requested action to be performed by the device controller.
    /// </summary>
    public class ActionRequest
    {
        public enum Command
        {
            NONE,

            CAMERA_DOWNLOAD,
            CAMERA_GET_PROPERTY,
            CAMERA_GET_PROPERTYDESC,
            CAMERA_TAKE_PICTURE,
            CAMERA_REMOTESHOOTING_START,
            CAMERA_REMOTESHOOTING_STOP,
            CAMERA_VIDEO_START,
            CAMERA_VIDEO_STOP,

            SERIAL_WRITE,
            SERIAL_ID,
            SERIAL_CALIBRATE,
            SERIAL_CAPTURE_START,
            SERIAL_CAPTURE_STOP,
            SERIAL_MOVE,
            SERIAL_RESET,
            SERIAL_STOP,
            SERIAL_GET_PROPERTY,

            MICROSCOPE_START,
            MICROSCOPE_TAKE_SNAPESHOT,

            CLOSING,
            SHUT_DOWN,
        }

        public Command Cmd { get; } = Command.NONE;
        public ActionRequestArgs Arg { get; }

        public ActionRequest(Command command, ActionRequestArgs arg)
        {
            this.Cmd = command;
            this.Arg = arg;
        }
    }
}
