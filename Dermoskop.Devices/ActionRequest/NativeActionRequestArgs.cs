﻿using System;

namespace Dermoskop.Devices
{
    public class NativeActionRequestArgs : ActionRequestArgs
    {
        public IntPtr Pointer { get; }

        public NativeActionRequestArgs(IntPtr pointer)
        {
            this.Pointer = pointer;
        }
    }
}
