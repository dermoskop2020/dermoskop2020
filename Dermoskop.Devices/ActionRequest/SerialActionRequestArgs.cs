﻿using System;

namespace Dermoskop.Devices
{
    public class SerialActionRequestArgs : ActionRequestArgs
    {
        public byte Data { get; }

        public SerialActionRequestArgs(byte data)
        {
            this.Data = data;
        }
    }
}
