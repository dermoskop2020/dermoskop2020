﻿using System;

namespace Dermoskop.Devices
{
    public class Device
    {
        public enum Type
        {
            CAMERA,
            SERIAL,
        }

        public Type DeviceType { get; }

        public IntPtr Id { get; protected set; }

        public event EventHandler<DeviceErrorEventArgs> Error = delegate { };
        public event EventHandler<ProgressReportEventArgs> ProgressReport = delegate { };
        public event EventHandler<DeviceErrorEventArgs> DeviceBusy = delegate { };
        public event EventHandler ShutDown = delegate { };

        public Device(Type deviceType)
        {
            this.DeviceType = DeviceType;
        }

        public virtual void OnError(DeviceErrorEventArgs e)
        {
            this.Error(this, e);
        }

        public virtual void OnDeviceBusy(DeviceErrorEventArgs e)
        {
            this.DeviceBusy(this, e);
        }

        public virtual void OnProgressReport(ProgressReportEventArgs e)
        {
            this.ProgressReport(this, e);
        }

        public virtual void OnShutDown(EventArgs e)
        {
            this.ShutDown(this, e);
        }
    }
}
