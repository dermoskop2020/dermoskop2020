﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dermoskop.Devices
{
    class CommandProcessor
    {
        private ConcurrentQueue<Command> commandQueue = new ConcurrentQueue<Command>();
        private bool running = false;
        private Task task = null;

        public void Start()
        {
            running = true;

            task = Task.Run(() =>
            {
                while (running)
                {
                    Thread.Sleep(1);

                    Command command = null;
                    commandQueue.TryDequeue(out command);
                    if (command != null)
                    {
                        if (command.Execute() == false)
                        {
                            /* If command that was issued fails and retry is required, enqueue command again.
                             * Warning: Some cameras may become unstable if multiple commands are issued in
                             * succession without an intervening interval. So leave an interval of about 500 ms 
                             * before commands are reissued. */
                            Thread.Sleep(500);
                            commandQueue.Enqueue(command);
                        }
                    }
                }
            });
        }

        /// <summary>
        /// Stop the processor immediately.
        /// </summary>
        public void Stop()
        {
            if (running)
            {
                running = false;
                try
                {
                    task.Wait();
                }
                catch (AggregateException)
                {
                    // TODO: Handle exception
                }

                task.Dispose();
                while (commandQueue.Count > 0)
                {
                    Command command = null;
                    commandQueue.TryDequeue(out command);
                }
            }
        }

        /// <summary>
        /// Checks whether the processor command queue is empty.
        /// </summary>
        /// <returns>Returns true if process command queue is empty</returns>
        public bool IsEmpty()
        {
            return commandQueue.IsEmpty;
        }

        public void PostCommand(Command command)
        {
            commandQueue.Enqueue(command);
        }
    }
}
