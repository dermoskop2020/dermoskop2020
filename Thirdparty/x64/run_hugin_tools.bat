@echo off

set TEMP=%1
set OUTPUT=%2

call pto_gen -f 15.3 -p 0 -o %TEMP%/project.pto %TEMP%/image*.tif
IF /I "%ERRORLEVEL%" NEQ "0" (goto exit)
call cpfind --linearmatch -o %TEMP%\project.pto %TEMP%\project.pto
IF /I "%ERRORLEVEL%" NEQ "0" (goto exit)
call cpclean -o %TEMP%\project.pto %TEMP%\project.pto
IF /I "%ERRORLEVEL%" NEQ "0" (goto exit)
::call pto_var --opt=y,p,r :: Set parameters to be optimized
call autooptimiser -a -m -n -o %TEMP%\project.pto %TEMP%\project.pto
IF /I "%ERRORLEVEL%" NEQ "0" (goto exit)
call pano_modify -o %TEMP%\project.pto --center --straighten --fov=AUTO --canvas=AUTO --crop=AUTO %TEMP%\project.pto
call nona -g -v -m TIFF_multilayer -o %TEMP%\project %TEMP%\project.pto
IF /I "%ERRORLEVEL%" NEQ "0" (goto exit)
call enblend -v -a --compression=jpeg -o %TEMP%\%OUTPUT% %TEMP%\project.tif
IF /I "%ERRORLEVEL%" NEQ "0" (goto exit)
call jpegtran -rotate 270 %TEMP%\%OUTPUT% %TEMP%\%OUTPUT%

:exit
exit %ERRORLEVEL%