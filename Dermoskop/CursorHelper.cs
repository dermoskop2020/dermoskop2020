﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dermoskop
{
    /// <summary>
    /// Helper class to facilitate easier cursor handeling.
    /// </summary>
    static class CursorHelper
    {
        static private bool show = true;

        /// <summary>
        /// Show cursor, if it is hidden.
        /// </summary>
        /// /// <remarks>
        /// Base <see cref="Cursor.Show"/> is counted. Two shows and one hide does not hide the cursor.
        /// </remarks>
        static public void Show()
        {
            if (!show)
            {
                Cursor.Show();
                show = true;
            }
        }

        /// <summary>
        /// Hide cursor, if it is shown.
        /// </summary>
        /// <remarks>
        /// Base <see cref="Cursor.Hide"/> is counted. Two shows and one hide does not hide the cursor.
        /// </remarks>
        static public void Hide()
        {
            if (show)
            {
                Cursor.Hide();
                show = false;
            }
        }

        private const int SM_CXDRAG = 68;
        private const int SM_CYDRAG = 69;
        /// <summary>
        /// Retrieves the specified system metric or system configuration setting.
        /// </summary>
        /// <param name="index"></param>
        /// <returns> If the function succeeds, the return value is the requested system metric or configuration setting. </returns>
        [DllImport("user32.dll")]
        private static extern int GetSystemMetrics(int index);

        /// <summary>
        /// Returns system specified minimum number of pixels on either side of a mouse-down point that the mouse pointer can move before a drag operation begins.
        /// </summary>
        /// <returns> Returns Point of minimum distance. </returns>
        static public Point GetDragThreshold()
        {
            return new Point(GetSystemMetrics(SM_CXDRAG), GetSystemMetrics(SM_CYDRAG));
        }
    }
}
