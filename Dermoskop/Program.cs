﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Security.Permissions;
using Dermoskop.Devices;

using Dermoskop.Database;
using Dermoskop.Database.Models;
using System.Drawing;

namespace Dermoskop
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread, SecurityPermission(SecurityAction.Demand, Flags = SecurityPermissionFlag.ControlAppDomain)]
        static void Main()
        {
            AppDomain currentDomain = AppDomain.CurrentDomain;
            currentDomain.UnhandledException += new UnhandledExceptionEventHandler(HandleUnhandledExceptionEvent);

            // Initialize Eos
            EosFramework.Initialize();
#if DEBUG
            // If you wish to delete the records you currently have in db
            DatabaseConnector.DeleteDatabase();
#endif
            DatabaseConnector.CreateDatabase();
            DatabaseConnector.CreateDatabaseScheme();
            DatabaseConnector.CreateSuperUser();

#if DEBUG
            // DatabaseConnector.TestModels();
            DatabaseInjector.InjectData(patientCount: 1, exemCount: 2, userCount: 0, birthmarkCount: 2, birthmarkImageCount: 2);
#endif

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());

            EosFramework.ReleaseCameraController();

            // Terminate Eos
            EosFramework.Terminate();
        }

        static void HandleUnhandledExceptionEvent(object sender, UnhandledExceptionEventArgs args)
        {
            if (Globals.SerialController != null)
            {
                Globals.SerialController.RequestAction(new ActionRequest(ActionRequest.Command.SHUT_DOWN, ActionRequestArgs.Empty));
            }
            if (Globals.CameraController != null)
            {
                Globals.CameraController.RequestAction(new ActionRequest(ActionRequest.Command.SHUT_DOWN, ActionRequestArgs.Empty));
            }
            if (Globals.MicroscopeController != null)
            {
                Globals.MicroscopeController.RequestAction(new ActionRequest(ActionRequest.Command.SHUT_DOWN, ActionRequestArgs.Empty));
            }
        }
    }
}
