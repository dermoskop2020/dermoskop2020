﻿namespace Dermoskop
{
    partial class uc_NewExamination
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel_Buttons = new System.Windows.Forms.TableLayoutPanel();
            this.btn_SaveContinue = new System.Windows.Forms.Button();
            this.btn_ImageCapture = new System.Windows.Forms.Button();
            this.groupBox_Comment = new System.Windows.Forms.GroupBox();
            this.textBox_Comment = new System.Windows.Forms.TextBox();
            this.label_Comment = new System.Windows.Forms.Label();
            this.progressBar_NewExam = new System.Windows.Forms.ProgressBar();
            this.groupBox_Position = new System.Windows.Forms.GroupBox();
            this.label_PositionInfo = new System.Windows.Forms.Label();
            this.label_Position = new System.Windows.Forms.Label();
            this.pictureBox_NewExam = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel_Buttons.SuspendLayout();
            this.groupBox_Comment.SuspendLayout();
            this.groupBox_Position.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_NewExam)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.pictureBox_NewExam, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1250, 600);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel_Buttons, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.groupBox_Comment, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.progressBar_NewExam, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.groupBox_Position, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 4;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(619, 594);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // tableLayoutPanel_Buttons
            // 
            this.tableLayoutPanel_Buttons.ColumnCount = 2;
            this.tableLayoutPanel_Buttons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_Buttons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_Buttons.Controls.Add(this.btn_SaveContinue, 1, 0);
            this.tableLayoutPanel_Buttons.Controls.Add(this.btn_ImageCapture, 0, 0);
            this.tableLayoutPanel_Buttons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel_Buttons.Location = new System.Drawing.Point(3, 497);
            this.tableLayoutPanel_Buttons.Name = "tableLayoutPanel_Buttons";
            this.tableLayoutPanel_Buttons.RowCount = 1;
            this.tableLayoutPanel_Buttons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel_Buttons.Size = new System.Drawing.Size(613, 94);
            this.tableLayoutPanel_Buttons.TabIndex = 2;
            // 
            // btn_SaveContinue
            // 
            this.btn_SaveContinue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_SaveContinue.Enabled = false;
            this.btn_SaveContinue.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_SaveContinue.Location = new System.Drawing.Point(309, 3);
            this.btn_SaveContinue.Name = "btn_SaveContinue";
            this.btn_SaveContinue.Size = new System.Drawing.Size(301, 88);
            this.btn_SaveContinue.TabIndex = 3;
            this.btn_SaveContinue.Text = "Continue";
            this.btn_SaveContinue.UseVisualStyleBackColor = true;
            this.btn_SaveContinue.Click += new System.EventHandler(this.btn_SaveContinue_Click);
            // 
            // btn_ImageCapture
            // 
            this.btn_ImageCapture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_ImageCapture.Enabled = false;
            this.btn_ImageCapture.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_ImageCapture.Location = new System.Drawing.Point(3, 3);
            this.btn_ImageCapture.Name = "btn_ImageCapture";
            this.btn_ImageCapture.Size = new System.Drawing.Size(300, 88);
            this.btn_ImageCapture.TabIndex = 2;
            this.btn_ImageCapture.Text = "Capture Image";
            this.btn_ImageCapture.UseVisualStyleBackColor = true;
            this.btn_ImageCapture.Click += new System.EventHandler(this.btn_ImageCapture_Click);
            // 
            // groupBox_Comment
            // 
            this.groupBox_Comment.Controls.Add(this.textBox_Comment);
            this.groupBox_Comment.Controls.Add(this.label_Comment);
            this.groupBox_Comment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox_Comment.Location = new System.Drawing.Point(3, 143);
            this.groupBox_Comment.Name = "groupBox_Comment";
            this.groupBox_Comment.Size = new System.Drawing.Size(613, 348);
            this.groupBox_Comment.TabIndex = 2;
            this.groupBox_Comment.TabStop = false;
            // 
            // textBox_Comment
            // 
            this.textBox_Comment.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox_Comment.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_Comment.Location = new System.Drawing.Point(3, 35);
            this.textBox_Comment.Multiline = true;
            this.textBox_Comment.Name = "textBox_Comment";
            this.textBox_Comment.Size = new System.Drawing.Size(607, 310);
            this.textBox_Comment.TabIndex = 3;
            // 
            // label_Comment
            // 
            this.label_Comment.AutoSize = true;
            this.label_Comment.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_Comment.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_Comment.Location = new System.Drawing.Point(3, 16);
            this.label_Comment.Name = "label_Comment";
            this.label_Comment.Size = new System.Drawing.Size(188, 19);
            this.label_Comment.TabIndex = 3;
            this.label_Comment.Text = "Examination Comment";
            // 
            // progressBar_NewExam
            // 
            this.progressBar_NewExam.Dock = System.Windows.Forms.DockStyle.Top;
            this.progressBar_NewExam.Location = new System.Drawing.Point(3, 103);
            this.progressBar_NewExam.Name = "progressBar_NewExam";
            this.progressBar_NewExam.Size = new System.Drawing.Size(613, 32);
            this.progressBar_NewExam.TabIndex = 2;
            // 
            // groupBox_Position
            // 
            this.groupBox_Position.Controls.Add(this.label_PositionInfo);
            this.groupBox_Position.Controls.Add(this.label_Position);
            this.groupBox_Position.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox_Position.Location = new System.Drawing.Point(3, 3);
            this.groupBox_Position.Name = "groupBox_Position";
            this.groupBox_Position.Size = new System.Drawing.Size(613, 94);
            this.groupBox_Position.TabIndex = 2;
            this.groupBox_Position.TabStop = false;
            // 
            // label_PositionInfo
            // 
            this.label_PositionInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_PositionInfo.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_PositionInfo.Location = new System.Drawing.Point(3, 35);
            this.label_PositionInfo.Name = "label_PositionInfo";
            this.label_PositionInfo.Size = new System.Drawing.Size(607, 42);
            this.label_PositionInfo.TabIndex = 3;
            this.label_PositionInfo.Text = "Position the patient so that he is facing the camere, as is demonstrated on the i" +
    "mage.";
            // 
            // label_Position
            // 
            this.label_Position.AutoSize = true;
            this.label_Position.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_Position.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_Position.Location = new System.Drawing.Point(3, 16);
            this.label_Position.Name = "label_Position";
            this.label_Position.Size = new System.Drawing.Size(46, 19);
            this.label_Position.TabIndex = 3;
            this.label_Position.Text = "Front";
            // 
            // pictureBox_NewExam
            // 
            this.pictureBox_NewExam.BackgroundImage = global::Dermoskop.Properties.Resources.front_profile;
            this.pictureBox_NewExam.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox_NewExam.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox_NewExam.Location = new System.Drawing.Point(628, 3);
            this.pictureBox_NewExam.Name = "pictureBox_NewExam";
            this.pictureBox_NewExam.Size = new System.Drawing.Size(619, 594);
            this.pictureBox_NewExam.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_NewExam.TabIndex = 1;
            this.pictureBox_NewExam.TabStop = false;
            // 
            // uc_NewExamination
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "uc_NewExamination";
            this.Size = new System.Drawing.Size(1250, 600);
            this.Load += new System.EventHandler(this.uc_NewExamination_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel_Buttons.ResumeLayout(false);
            this.groupBox_Comment.ResumeLayout(false);
            this.groupBox_Comment.PerformLayout();
            this.groupBox_Position.ResumeLayout(false);
            this.groupBox_Position.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_NewExam)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.PictureBox pictureBox_NewExam;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_Buttons;
        private System.Windows.Forms.Button btn_SaveContinue;
        private System.Windows.Forms.Button btn_ImageCapture;
        private System.Windows.Forms.GroupBox groupBox_Comment;
        private System.Windows.Forms.TextBox textBox_Comment;
        private System.Windows.Forms.Label label_Comment;
        private System.Windows.Forms.ProgressBar progressBar_NewExam;
        private System.Windows.Forms.GroupBox groupBox_Position;
        private System.Windows.Forms.Label label_PositionInfo;
        private System.Windows.Forms.Label label_Position;
    }
}
