﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dermoskop
{
    public partial class uc_Login : UserControl
    {
        public uc_Login()
        {
            InitializeComponent();
            InitData();
        }

        private void InitData()
        {
            if (Properties.Settings.Default.Username != string.Empty)
            {
                if (Properties.Settings.Default.Rememberme == "yes")
                {
                    UserNameText.Text = Properties.Settings.Default.Username;
                    checkBoxRememberMe.Checked = true;
                }
                else
                    UserNameText.Text = Properties.Settings.Default.Username;
            }
        }

        private void SaveData()
        {
            if (checkBoxRememberMe.Checked)
            {
                Properties.Settings.Default.Username = UserNameText.Text;
                Properties.Settings.Default.Rememberme = "yes";
                Properties.Settings.Default.Save();
            }
            else
            {
                Properties.Settings.Default.Username = "";
                Properties.Settings.Default.Rememberme = "no";
                Properties.Settings.Default.Save();
            }
        }

        private void NextButton_Click(object sender, EventArgs e)
        {
            var user = Database.DatabaseConnector.LoginModel(UserNameText.Text, Database.HashClass.HashModel(PasswordText.Text));
            
            if (user != null)
            {
                SaveData();
                Globals.CurentUser = user;
                Globals.UserName = user.Username;
                Globals.UserID = user.Id;
                ErrorLabel.Visible = false;
                UserNameText.Clear();
                PasswordText.Clear();
                if (!Form1.Instance.PnlContainer.Controls.ContainsKey("uc_PatientSelect"))
                {
                    uc_PatientSelect ps = new uc_PatientSelect();
                    ps.Dock = DockStyle.Fill;
                    Form1.Instance.PnlContainer.Controls.Add(ps);
                }
                Form1.Instance.PnlContainer.Controls["uc_PatientSelect"].BringToFront();
                Form1.Instance.PnlContainer.Controls.RemoveByKey("uc_Login");
                /*
                Form1.Instance.SignOutButton.Visible = true;
                Form1.Instance.PatientSelectButton.Visible = true;
                Form1.Instance.NewPatientButton.Visible = true;
                Form1.Instance.ExamViewButton.Visible = true;
                Form1.Instance.NewExamButton.Visible = true;
                Form1.Instance.MarkViewButton.Visible = true;
                Form1.Instance.NewMarkButton.Visible = true;

                Form1.Instance.ExamViewButton.Enabled = false;
                Form1.Instance.NewExamButton.Enabled = false;
                Form1.Instance.MarkViewButton.Enabled = false;
                Form1.Instance.NewMarkButton.Enabled = false;*/
            }
            else
            {
                ErrorLabel.Visible = true;
                PasswordText.Select();
            }
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            ErrorLabel.Visible = false;
            UserNameText.Clear();
            PasswordText.Clear();
            if (!Form1.Instance.PnlContainer.Controls.ContainsKey("uc_Welcome"))
            {
                uc_Welcome ps = new uc_Welcome();
                ps.Dock = DockStyle.Fill;
                Form1.Instance.PnlContainer.Controls.Add(ps);
            }
            Form1.Instance.PnlContainer.Controls["uc_Welcome"].BringToFront();
            Form1.Instance.PnlContainer.Controls.RemoveByKey("uc_Login");
        }


        private void Password_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                NextButton_Click(this, new EventArgs());
                if (Form1.Instance.PnlContainer.Controls.ContainsKey("uc_PatientSelect")) // meaning our password and username were correct
                {
                    e.Handled = true;
                    e.SuppressKeyPress = true;
                }
            }
        }
    }
}
