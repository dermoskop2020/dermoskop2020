﻿using System.Diagnostics;

namespace Dermoskop
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panelContainer = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btn_BarSignOut = new FontAwesome.Sharp.IconButton();
            this.btn_BarPatientSelect = new FontAwesome.Sharp.IconButton();
            this.btn_BarNewPatient = new FontAwesome.Sharp.IconButton();
            this.btn_BarExamView = new FontAwesome.Sharp.IconButton();
            this.btn_BarNewExam = new FontAwesome.Sharp.IconButton();
            this.btn_BarMarkView = new FontAwesome.Sharp.IconButton();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelContainer
            // 
            this.panelContainer.AutoSize = true;
            this.panelContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelContainer.Location = new System.Drawing.Point(3, 3);
            this.panelContainer.Name = "panelContainer";
            this.panelContainer.Size = new System.Drawing.Size(1002, 574);
            this.panelContainer.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.panelContainer, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 62);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1008, 580);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // btn_BarSignOut
            // 
            this.btn_BarSignOut.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_BarSignOut.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btn_BarSignOut.IconChar = FontAwesome.Sharp.IconChar.SignOutAlt;
            this.btn_BarSignOut.IconColor = System.Drawing.Color.Black;
            this.btn_BarSignOut.IconSize = 54;
            this.btn_BarSignOut.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_BarSignOut.Location = new System.Drawing.Point(3, 3);
            this.btn_BarSignOut.Name = "btn_BarSignOut";
            this.btn_BarSignOut.Rotation = 0D;
            this.btn_BarSignOut.Size = new System.Drawing.Size(54, 54);
            this.btn_BarSignOut.TabIndex = 10;
            this.toolTip1.SetToolTip(this.btn_BarSignOut, "Sign Out");
            this.btn_BarSignOut.UseVisualStyleBackColor = true;
            this.btn_BarSignOut.Click += new System.EventHandler(this.btn_BarSignOut_Click);
            // 
            // btn_BarPatientSelect
            // 
            this.btn_BarPatientSelect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_BarPatientSelect.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btn_BarPatientSelect.IconChar = FontAwesome.Sharp.IconChar.Users;
            this.btn_BarPatientSelect.IconColor = System.Drawing.Color.Black;
            this.btn_BarPatientSelect.IconSize = 54;
            this.btn_BarPatientSelect.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_BarPatientSelect.Location = new System.Drawing.Point(3, 3);
            this.btn_BarPatientSelect.Name = "btn_BarPatientSelect";
            this.btn_BarPatientSelect.Rotation = 0D;
            this.btn_BarPatientSelect.Size = new System.Drawing.Size(54, 54);
            this.btn_BarPatientSelect.TabIndex = 11;
            this.toolTip1.SetToolTip(this.btn_BarPatientSelect, "Patient Select");
            this.btn_BarPatientSelect.UseVisualStyleBackColor = true;
            this.btn_BarPatientSelect.Click += new System.EventHandler(this.btn_BarPatientSelect_Click);
            // 
            // btn_BarNewPatient
            // 
            this.btn_BarNewPatient.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_BarNewPatient.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btn_BarNewPatient.IconChar = FontAwesome.Sharp.IconChar.UserPlus;
            this.btn_BarNewPatient.IconColor = System.Drawing.Color.Black;
            this.btn_BarNewPatient.IconSize = 54;
            this.btn_BarNewPatient.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_BarNewPatient.Location = new System.Drawing.Point(63, 3);
            this.btn_BarNewPatient.Name = "btn_BarNewPatient";
            this.btn_BarNewPatient.Rotation = 0D;
            this.btn_BarNewPatient.Size = new System.Drawing.Size(54, 54);
            this.btn_BarNewPatient.TabIndex = 12;
            this.toolTip1.SetToolTip(this.btn_BarNewPatient, "New Patient");
            this.btn_BarNewPatient.UseVisualStyleBackColor = true;
            this.btn_BarNewPatient.Click += new System.EventHandler(this.btn_BarNewPatient_Click);
            // 
            // btn_BarExamView
            // 
            this.btn_BarExamView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_BarExamView.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btn_BarExamView.IconChar = FontAwesome.Sharp.IconChar.Images;
            this.btn_BarExamView.IconColor = System.Drawing.Color.Black;
            this.btn_BarExamView.IconSize = 54;
            this.btn_BarExamView.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_BarExamView.Location = new System.Drawing.Point(123, 3);
            this.btn_BarExamView.Name = "btn_BarExamView";
            this.btn_BarExamView.Rotation = 0D;
            this.btn_BarExamView.Size = new System.Drawing.Size(54, 54);
            this.btn_BarExamView.TabIndex = 13;
            this.toolTip1.SetToolTip(this.btn_BarExamView, "Examination View");
            this.btn_BarExamView.UseVisualStyleBackColor = true;
            this.btn_BarExamView.Click += new System.EventHandler(this.btn_BarExamView_Click);
            // 
            // btn_BarNewExam
            // 
            this.btn_BarNewExam.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_BarNewExam.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btn_BarNewExam.IconChar = FontAwesome.Sharp.IconChar.Camera;
            this.btn_BarNewExam.IconColor = System.Drawing.Color.Black;
            this.btn_BarNewExam.IconSize = 54;
            this.btn_BarNewExam.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_BarNewExam.Location = new System.Drawing.Point(183, 3);
            this.btn_BarNewExam.Name = "btn_BarNewExam";
            this.btn_BarNewExam.Rotation = 0D;
            this.btn_BarNewExam.Size = new System.Drawing.Size(54, 54);
            this.btn_BarNewExam.TabIndex = 14;
            this.toolTip1.SetToolTip(this.btn_BarNewExam, "New Examination");
            this.btn_BarNewExam.UseVisualStyleBackColor = true;
            this.btn_BarNewExam.Click += new System.EventHandler(this.btn_BarNewExam_Click);
            // 
            // btn_BarMarkView
            // 
            this.btn_BarMarkView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_BarMarkView.Flip = FontAwesome.Sharp.FlipOrientation.Normal;
            this.btn_BarMarkView.IconChar = FontAwesome.Sharp.IconChar.Disease;
            this.btn_BarMarkView.IconColor = System.Drawing.Color.Black;
            this.btn_BarMarkView.IconSize = 54;
            this.btn_BarMarkView.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_BarMarkView.Location = new System.Drawing.Point(243, 3);
            this.btn_BarMarkView.Name = "btn_BarMarkView";
            this.btn_BarMarkView.Rotation = 0D;
            this.btn_BarMarkView.Size = new System.Drawing.Size(54, 54);
            this.btn_BarMarkView.TabIndex = 15;
            this.toolTip1.SetToolTip(this.btn_BarMarkView, "Birthmarks");
            this.btn_BarMarkView.UseVisualStyleBackColor = true;
            this.btn_BarMarkView.Click += new System.EventHandler(this.btn_BarMarkView_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Controls.Add(this.btn_BarPatientSelect, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btn_BarNewPatient, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.btn_BarExamView, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.btn_BarNewExam, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.btn_BarMarkView, 4, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(300, 60);
            this.tableLayoutPanel2.TabIndex = 16;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Controls.Add(this.btn_BarSignOut, 0, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(945, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(60, 60);
            this.tableLayoutPanel3.TabIndex = 17;
            // 
            // toolTip1
            // 
            this.toolTip1.Popup += new System.Windows.Forms.PopupEventHandler(this.toolTip1_Popup);
            // 
            // Form1
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1008, 641);
            this.Controls.Add(this.tableLayoutPanel3);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1024, 680);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dermoskop";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelContainer;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private FontAwesome.Sharp.IconButton btn_BarSignOut;
        private FontAwesome.Sharp.IconButton btn_BarPatientSelect;
        private FontAwesome.Sharp.IconButton btn_BarNewPatient;
        private FontAwesome.Sharp.IconButton btn_BarExamView;
        private FontAwesome.Sharp.IconButton btn_BarNewExam;
        private FontAwesome.Sharp.IconButton btn_BarMarkView;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}

