﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity.Core.Metadata.Edm;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing.Text;
using System.Linq;
using System.Security.AccessControl;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace Dermoskop
{
    /// <summary>
    /// Control za opazovanje slik.
    /// Omogoča premikanje in približevanje.
    /// </summary>
    public class ImageViewer : PictureBox
    {
        #region Variables

        private Point cameraLocation = Point.Empty;
        private Rectangle viewport;
        private RectangleF camera;
        private float aspectRatio;
        private float zoom = 1;
        private float zoomSpeed = 0.0005f;
        private bool mouseDown = false;
        private bool panning = false;
        private Point startingPoint;
        private Point anchorPoint; // Location to reset mouse position to after drag.
        private Birthmark.Mode birthmarkMode = Birthmark.Mode.HIDE;
        private bool showInfo = false;
        // Ref: https://en.wikipedia.org/wiki/Mipmap
        private Mipmap[] mipmaps;
        private int miplevels = 1;
        private bool generateMipmaps = true;
        private List<Birthmark> birthmarks;

        #endregion

        #region Event delegets
        /// <summary> Triggers when camera location changes. </summary>
        public event EventHandler<CameraEventArgs> CameraLocationChanged = delegate { };
        /// <summary> Triggers when camera zoom changes. </summary>
        public event EventHandler<CameraEventArgs> CameraZoomChanged = delegate { };
        /// <summary> Triggers when new birthmark is selected </summary>
        public event EventHandler<BirthmarkEventArgs> BirthmarkSelected = delegate { };
        /// <summary> Triggers when new birthamrk is inserted </summary>
        public event EventHandler<BirthmarkInsertEventArgs> BirthmarkInserted = delegate { };
        #endregion

        #region Properties
        // TODO: Dodaj descriptione??

        /// <summary> Camera location. </summary>
        [Category("Camera"), Description("Camera location")]
        public Point CameraLocation
        {
            get { return cameraLocation; }
            set
            {
                cameraLocation = value;
                CalculateCameraRect();
                this.Invalidate();
                CameraLocationChanged(this, new CameraEventArgs(this.Location, this.Zoom));
            }
        }
         
        /// <summary> Camera zoom. </summary>
        [Category("Camera"), Description("Camera zoom")]
        public float Zoom
        {
            get { return zoom; }
            set
            {
                zoom = value;
                CalculateCameraRect();
                this.Invalidate();
                CameraZoomChanged(this, new CameraEventArgs(this.Location, this.Zoom));
            }
        }

        [Category("Camera"), Description("")]
        public float ZoomSpeed
        {
            get { return zoomSpeed; }
            set { zoomSpeed = value; }
        }

        /// <summary> Is camera currently panning. </summary>
        public bool Panning
        {
            get { return panning; }
        }

        /// <summary> List of birthmarks to display. </summary>
        public List<Birthmark> Birthmarks
        {
            get { return birthmarks; }
            set
            {
                birthmarks = value;
                if (birthmarks != null && birthmarks.Count > 0 && this.birthmarkMode == Birthmark.Mode.SELECT)
                {
                    var mark = birthmarks[0];
                    mark.IsSelected = true;
                    BirthmarkSelected.Invoke(this, new BirthmarkEventArgs(mark));
                }
                else
                    BirthmarkSelected.Invoke(this, new BirthmarkEventArgs(null));

                this.Invalidate();
            }
        }

        /// <summary> Determines current interaction with birthmarks. </summary>
        public Birthmark.Mode BirthmarkMode
        {
            get { return birthmarkMode; }
            set
            {
                birthmarkMode = value;
                this.Invalidate();
            }
        }

        public bool GenerateMipmaps
        {
            get { return generateMipmaps; }
            set
            {
                generateMipmaps = value;
                if (generateMipmaps)
                {
                    if (this.Image != null)
                    {
                        MakeMipmaps();
                        this.Invalidate();
                    }
                }
                else
                {
                    if (mipmaps != null)
                    {
                        Array.Clear(mipmaps, 0, miplevels);
                    }
                    this.Invalidate();
                }
            }
        }

        public new Image Image
        {
            get
            {
                return base.Image;
            }
            set
            {
                base.Image = value;
                if (base.Image != null)
                {
                    MakeMipmaps();
                    cameraLocation.X = value.Width/2;
                    cameraLocation.Y = value.Height/2;
                    zoom = Math.Min((float)Width / this.Image.Width, (float)Height / this.Image.Height);
                    CalculateCameraRect();
                    this.Invalidate();
                }
                else
                {
                    // Remove mipmaps
                    if (mipmaps != null)
                        Array.Clear(mipmaps, 0, miplevels);
                    this.Invalidate();
                }
            }
        }
        #endregion

        #region Constructors
        /// <summary> Default constructor. </summary>
        public ImageViewer()
        {
        }

        public ImageViewer(int x, int y) : base()
        {
            this.cameraLocation = new Point(x, y);
            CalculateCameraRect();
        }

        public ImageViewer(int x, int y, float zoom) : this(x, y)
        {
            this.zoom = zoom;
            CalculateCameraRect();
        }

        public ImageViewer(Point cameraLocation) : base()
        {
            this.cameraLocation = cameraLocation;
            CalculateCameraRect();
        }

        public ImageViewer(Point cameraLocation, float zoom) : this(cameraLocation)
        {
            this.zoom = zoom;
            CalculateCameraRect();
        }

        public ImageViewer(float zoom) : base()
        {
            this.zoom = zoom;
            CalculateCameraRect();
        }
        #endregion

        #region Member functions

        #region Event overrides
        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);

            if (this.Image != null)
            {
                pe.Graphics.Clear(Color.Black);

                pe.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                pe.Graphics.CompositingQuality = CompositingQuality.HighQuality;
                pe.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
                pe.Graphics.TextRenderingHint = TextRenderingHint.AntiAliasGridFit;

                RectangleF srcRect = camera;

                Bitmap image;
                if (camera.Width < this.Width || !generateMipmaps)
                {
                    image = (Bitmap)this.Image;
                }
                else
                {
                    float r = camera.Width / this.Image.Width;
                    int i = (int)Math.Round(r);
                    int miplevel = i;
                    if (miplevel > miplevels)
                        miplevel = miplevels;
                    if (miplevel < 1)
                        miplevel = 1;
                    Mipmap mipmap = mipmaps[miplevel - 1];
                    image = mipmap.Image;
                    srcRect.Width = srcRect.Width / (mipmap.Multiplyer * miplevel);
                    srcRect.Height = srcRect.Height / (mipmap.Multiplyer * miplevel);
                    srcRect.X = srcRect.X / (mipmap.Multiplyer * miplevel);
                    srcRect.Y = srcRect.Y / (mipmap.Multiplyer * miplevel);
                }

                pe.Graphics.DrawImage(
                    image,
                    viewport,
                    srcRect,
                    GraphicsUnit.Pixel);

                if (birthmarkMode != Birthmark.Mode.HIDE)
                {
                    DrawBirthmarks(pe.Graphics, viewport, camera);
                }

                if (showInfo)
                {
                    string koordinate = "Koordinate kamere: " + this.CameraLocation.ToString() + "\nZoom: " + this.Zoom.ToString() + "\nŠirina prikazane slike v pikslih: " + ((int)(camera.Width)).ToString();
                    pe.Graphics.FillRectangle(new SolidBrush(Color.FromArgb(168, 0, 0, 0)), 0, 0, 300, 60);
                    pe.Graphics.DrawString(koordinate, new Font(new FontFamily("Microsoft Sans Serif"), 12.0f, FontStyle.Regular), new SolidBrush(Color.White), new PointF(0.0f, 0.0f));
                }

                if (birthmarkMode == Birthmark.Mode.INSERT)
                {
                    DrawCursor(pe.Graphics, viewport, camera);
                }
            } else
            {
                // show the default error image
                base.OnPaint(pe);
            }
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);

            aspectRatio = (float)Height / (float)Width;

            CalculateCameraRect();
            viewport = new Rectangle(0, 0, Width, Height);

            this.Invalidate();
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            mouseDown = true;
            startingPoint = e.Location;
            anchorPoint = e.Location;

            base.OnMouseDown(e);
        }

        protected override void OnMouseUp(MouseEventArgs e)
        {
            // catch null image exception
            if (Image == null)
            {
                base.OnMouseUp(e);
                return;
            }

            if (panning)
            {
                CursorHelper.Show();
                panning = false;

                // Reset mouse position
                Cursor.Position = this.PointToScreen(anchorPoint);
            }
            else if (birthmarkMode == Birthmark.Mode.SELECT)
            {
                // TODO: only deselect if a mark was selected!!
                Point mouseLocation = e.Location;

                var marksLocations = birthmarks.Select(mark => TransformToViewportSpace(
                    new RectangleF(mark.Location.X - mark.Diameter / 2,
                        mark.Location.Y - mark.Diameter / 2,
                        mark.Diameter, mark.Diameter),
                    camera,
                    zoom).Contains(mouseLocation)
                ).ToList();

                // check if mouse is clicked at the mark
                if (marksLocations.Any(x => x))
                {
                    // mouse was clicked at the mouse

                    // find previously selected marks
                    var index = birthmarks.FindIndex(x => x.IsSelected);
                    if (index >= 0)
                        birthmarks[index].IsSelected = false;

                    // change the birthmark that was selected to true
                    index = marksLocations.FindIndex(x => x);
                    if (index >= 0)
                        birthmarks[index].IsSelected = true;
                    
                    // raise event, redraw
                    BirthmarkSelected.Invoke(this, new BirthmarkEventArgs(birthmarks[index]));
                    this.Invalidate();
                }
            }
            else if (birthmarkMode == Birthmark.Mode.INSERT)
            {
                var d = Birthmark.DefaultRadius;
                // get the center of the image into the image coordinates
                Point location = new Point((int)Math.Round(camera.X + camera.Width / 2), (int)Math.Round(camera.Y + camera.Height / 2));
                bool valid = new Rectangle(new Point(0, 0), new Size(this.Image.Width, this.Image.Height)).Contains(location) &&
                             !birthmarks.Select(bm => (Math.Pow(location.X - bm.Location.X, 2) + Math.Pow(location.Y - bm.Location.Y, 2)) < ((d/2) * (d/2))).Any(x => x);

                // raise event
                BirthmarkInserted.Invoke(this, new BirthmarkInsertEventArgs(location, valid));
            }
            mouseDown = false;

            base.OnMouseUp(e);
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            if (Image != null && mouseDown)
            {
                if (!panning)
                {
                    Point threshold = CursorHelper.GetDragThreshold();
                    bool x = Math.Abs(e.Location.X - anchorPoint.X) > threshold.X;
                    bool y = Math.Abs(e.Location.Y - anchorPoint.Y) > threshold.Y;
                    if (x || y)
                    {
                        panning = true;
                        CursorHelper.Hide();
                    }
                }
                else
                {
                    Point newPoint = e.Location;

                    if (newPoint == startingPoint)
                        return;

                    int deltaX = (int)((newPoint.X - startingPoint.X));
                    int deltaY = (int)((newPoint.Y - startingPoint.Y));

                    if (zoom < 1)
                    {
                        deltaX = (int)(deltaX / zoom);
                        deltaY = (int)(deltaY / zoom);
                    }

                    Point location = new Point(cameraLocation.X - deltaX, cameraLocation.Y - deltaY);

                    if (location.X < 0)
                    {
                        location.X = 0;
                    }
                    else if (location.X > Image.Width)
                    {
                        location.X = Image.Width;
                    }

                    if (location.Y < 0)
                    {
                        location.Y = 0;
                    }
                    else if (location.Y > Image.Height)
                    {
                        location.Y = Image.Height;
                    }

                    cameraLocation = location;

                    CalculateCameraRect();

                    startingPoint = newPoint;

                    this.Invalidate();

                    CameraLocationChanged(this, new CameraEventArgs(this.Location, this.Zoom));
                }
            }

            base.OnMouseMove(e);
        }

        protected override void OnMouseWheel(MouseEventArgs e)
        {
            // catch null image exception
            if (Image == null)
            {
                base.OnMouseWheel(e);
                return;
            }

            float newZoom = zoom + e.Delta * zoomSpeed;

            if (newZoom < 0.05F)
                newZoom = 0.05F;
            else if (newZoom > 2.0F)
                newZoom = 2.0F;

            if (newZoom == zoom)
                return;

            zoom = newZoom;

            CalculateCameraRect();

            this.Invalidate();

            CameraZoomChanged(this, new CameraEventArgs(this.Location, this.Zoom));

            base.OnMouseWheel(e);
        }
        #endregion
        
        /// <summary> Generat mipmaps to be used. </summary>
        private void MakeMipmaps()
        {
            if (this.Image == null)
                return;
            if (!generateMipmaps)
                return;
            if (mipmaps != null)
                Array.Clear(mipmaps, 0, miplevels);
            mipmaps = new Mipmap[miplevels];
            for (int i = 0; i < miplevels; i++)
            {
                mipmaps[i] = new Mipmap((Bitmap)this.Image, i+1);
            }
        }

        /// <summary> Calculate camera rectangle. </summary>
        private void CalculateCameraRect()
        {
            float width = Width / zoom;
            float height = width * aspectRatio;

            float x0 = cameraLocation.X - width / 2;
            float y0 = cameraLocation.Y - height / 2;
            float x1 = cameraLocation.X + width / 2;
            float y1 = cameraLocation.Y + height / 2;
            
            camera = new RectangleF(x0, y0, x1 - x0, y1 - y0);
        }

        private PointF TransformToViewportSpace(PointF point, RectangleF camera, float zoom)
        {
            return new PointF((point.X - camera.X) * zoom, (point.Y - camera.Y) * zoom);
        }

        private RectangleF TransformToViewportSpace(RectangleF rect, RectangleF camera, float zoom)
        {
            float x = (rect.X - camera.X) * zoom;
            float y = (rect.Y - camera.Y) * zoom;
            float width = rect.Width * zoom;
            float height = rect.Height * zoom;
            return new RectangleF(x, y, width, height);
        }

        private PointF TranformToCameraSpace(PointF point, Rectangle camera, float zoon)
        {
            return new PointF(point.X / Zoom + camera.X, point.Y / zoom + camera.Y);
        }

        private void DrawCursor(Graphics g, Rectangle viewport, RectangleF camera)
        {
            float radius = 12;
            // Top vertical line top point
            PointF vT_T = new PointF(viewport.Width / 2, viewport.Height / 2 - 100);
            // Top vertical line bottom point
            PointF vT_B = new PointF(viewport.Width / 2, (camera.Height / 2 - radius) * Zoom);
            // Bottom vertical line top point
            PointF vB_T = new PointF(viewport.Width / 2, viewport.Height / 2 + 100);
            // Bottom vertical line bottom point
            PointF vB_B = new PointF(viewport.Width / 2, (camera.Height / 2 + radius) * Zoom);
            // Left horizontal line left point
            PointF vL_L = new PointF(viewport.Width / 2 - 100, viewport.Height / 2);
            // Left horizontal line right point
            PointF vL_R = new PointF((camera.Width / 2 - radius) * Zoom, viewport.Height / 2);
            // Right horizontal line left point
            PointF vR_L = new PointF((camera.Width / 2 + radius) * Zoom, viewport.Height / 2);
            // Right horizontal line right point
            PointF vR_R = new PointF(viewport.Width / 2 + 100, viewport.Height / 2);

            // Draw lines
            Pen foregroundPen = new Pen(Color.FromArgb(255, 0, 0, 0), 1.0f);
            Pen backgroundPen = new Pen(Color.FromArgb(255, 255, 255, 255), 2.0f);
            // Vertical lines
            g.DrawLine(backgroundPen, vT_T, vT_B);
            g.DrawLine(backgroundPen, vB_T, vB_B);
            g.DrawLine(foregroundPen, vT_T.X, vT_T.Y + 1, vT_B.X, vT_B.Y - 1);
            g.DrawLine(foregroundPen, vB_T.X, vB_T.Y - 1 , vB_B.X, vB_B.Y + 1);

            // Horizontal line
            g.DrawLine(backgroundPen, vL_L, vL_R);
            g.DrawLine(backgroundPen, vR_L, vR_R);
            g.DrawLine(foregroundPen, vL_L.X + 1, vL_L.Y, vL_R.X - 1, vL_R.Y);
            g.DrawLine(foregroundPen, vR_L.X - 1, vR_L.Y, vR_R.X + 1, vR_R.Y);
        }

        private void DrawBirthmarks(Graphics g, RectangleF viewport, RectangleF camera)
        {
            Pen markPen = new Pen(Color.FromArgb(255, 0, 0, 255), 1.0F);
            Pen markSelectedPen = new Pen(Color.FromArgb(255, 0, 255, 0), 2.0F);

            Brush textBrush = Brushes.Black;
            Pen textOutlinePen = new Pen(Color.White, 1.0f);
            Brush textBrushSelected = Brushes.Black;
            Pen textOutlinePenSelected = new Pen(Color.FromArgb(255, 0, 255, 0), 1.0f);
            int fontSize = 12;
            int fontSizeSelected = 15;

            birthmarks.Sort(); // Sort birthmarks, so selected ones will be on top.

            foreach (Birthmark mark in birthmarks)
            {
                RectangleF markBounds = new RectangleF(
                    mark.Location.X - mark.Diameter / 2,
                    mark.Location.Y - mark.Diameter / 2,
                    mark.Diameter, mark.Diameter);
                if (camera.IntersectsWith(markBounds))
                {
                    PointF transformedMarkLocation = TransformToViewportSpace(mark.Location, camera, zoom);
                    //RectangleF transformedMarkBounds = TransformToViewportSpace(markBounds, camera, zoom);

                    GraphicsPath textPath = new GraphicsPath();

                    RectangleF transformedMarkBounds = new RectangleF(
                        transformedMarkLocation.X - mark.Diameter / 2,
                        transformedMarkLocation.Y - mark.Diameter / 2,
                        mark.Diameter,
                        mark.Diameter);

                    if (mark.IsSelected)
                    {
                        g.DrawEllipse(markSelectedPen, transformedMarkBounds);
                    }
                    else
                    {
                        g.DrawEllipse(markPen, transformedMarkBounds);
                    }

                    int fs = (mark.IsSelected) ? fontSizeSelected : fontSize;
                    textPath.AddString(
                        mark.Name,                              // text to draw
                        FontFamily.GenericSansSerif,            // or any other font family
                        (int)FontStyle.Regular,                 // font style (bold, italic, etc.)
                        g.DpiY * fs / 72,                       // em size
                        new PointF(
                            transformedMarkBounds.Right - 7,
                            transformedMarkBounds.Bottom - 7),  // location where to draw text
                        StringFormat.GenericTypographic);       // set options here (e.g. center alignment)

                    if (mark.IsSelected)
                    {
                        g.DrawPath(textOutlinePenSelected, textPath);
                        g.FillPath(textBrushSelected, textPath);
                    }
                    else
                    {
                        g.DrawPath(textOutlinePen, textPath);
                        g.FillPath(textBrush, textPath);
                    }
                }
            }
        }

        #endregion
    }
}
