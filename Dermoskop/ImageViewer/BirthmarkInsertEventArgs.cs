﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dermoskop
{
    public class BirthmarkInsertEventArgs : EventArgs
    {
        public Point Location { get; protected set; }
        public bool Valid { get; protected set; }

        public BirthmarkInsertEventArgs(Point Location, bool isValid)
        {
            this.Location = Location;
            this.Valid = isValid;
        }
    }
}
