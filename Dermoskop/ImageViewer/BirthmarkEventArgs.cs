﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dermoskop
{
    /// <summary>
    /// 
    /// </summary>
    public class BirthmarkEventArgs : EventArgs
    {
        public Birthmark Mark { get; protected set; }

        public BirthmarkEventArgs(Birthmark mark)
        {
            this.Mark = mark;
        }
    }
}
