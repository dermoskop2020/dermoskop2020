﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dermoskop
{
    /// <summary>
    /// Contains <see cref="ImageViewer"/> camera event data.
    /// </summary>
    /// <remarks>
    /// Sent by events <see cref="ImageViewer.CameraLocationChanged"/> and <see cref="ImageViewer.CameraZoomChanged"/>.
    /// </remarks>
    public class CameraEventArgs : EventArgs
    {
        public Point Location { get; protected set; }
        public float Zoom { get; protected set; }

        /// <summary> Default constructor. </summary>
        public CameraEventArgs(Point location, float zoom)
        {
            Location = location;
            Zoom = zoom;
        }
    }
}
