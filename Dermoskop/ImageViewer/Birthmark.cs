﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dermoskop
{
    /// <summary>
    /// Holds birthmark data, to be used by <see cref="ImageViewer"/>.
    /// </summary>
    /// <remarks>
    /// Birthmarks can be sorted based on if they are selected. 
    /// For example, selected birthmarks will be in the back of the <see cref="List{T}"/>.
    /// </remarks>
    public class Birthmark : IComparable<Birthmark>
    {
        /// <summary>
        /// Possible interactions whit birthmarks within <see cref="ImageViewer"/>.
        /// </summary>
        /// <remarks>
        /// <see cref="Mode.INSERT"/>: birthmarks can be added,
        /// <see cref="Mode.SELECT"/>: birthmarks can be selected,
        /// <see cref="Mode.HIDE"/>: hide birthmarks.
        /// </remarks>
        public enum Mode
        {
            INSERT,
            SELECT,
            HIDE,
        }

        public Point Location { get; set; }
        public string Name { get; set; }
        public float Diameter { get; set; }
        public bool IsSelected { get; set; }

        public static float DefaultRadius = 10f;

        public Birthmark(Point location, string name)
        {
            this.Location = location;
            this.Name = name;
            this.Diameter = 2*DefaultRadius;
            this.IsSelected = false;
        }

        public override string ToString()
        {
            return Name;
        }

        /// <summary>
        /// Compare birthmarks based on <see cref="IsSelected"/>.
        /// </summary>
        /// <param name="compareBirthmark"></param>
        /// <returns></returns>
        public int CompareTo(Birthmark compareBirthmark)
        {
            // A null value means that this object is greater.
            if (compareBirthmark == null)
                return 1;

            else
                return this.IsSelected.CompareTo(compareBirthmark.IsSelected);
        }
    }
}
