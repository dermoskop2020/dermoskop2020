﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dermoskop
{
    /// <summary>
    /// Creates and holds image mipmap data
    /// </summary>
    class Mipmap
    {
        public Bitmap Image { get; protected set; }

        public int Level { get; protected set; }

        public int Multiplyer { get; protected set; }

        /// <summary> Default constructor. </summary>
        public Mipmap()
        {
        }

        /// <summary>
        /// Construct mipmap from <see cref="Bitmap"/> whit specified level.
        /// </summary>
        public Mipmap(Bitmap srcImage, int level) : base()
        {
            Multiplyer = 4;
            Level = level;
            int width = (int)((float)srcImage.Width / (Multiplyer * level));
            Generate(srcImage, width);
        }

        /// <summary>
        /// Generates actual mipmap and stores it into <see cref="Mipmap.Image"/>.
        /// </summary>
        public void Generate(Bitmap srcImage, int width)
        {
            Size size = srcImage.Size;

            float aspect = (float)size.Height / (float)size.Width;

            float mipmapWidth = width;
            float mipmapHeight = aspect * mipmapWidth;
            Image = new Bitmap(srcImage, new Size((int)mipmapWidth, (int)mipmapHeight));
        }
    }
}
