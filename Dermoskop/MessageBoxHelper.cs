﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;

namespace Dermoskop
{
    static class MessageBoxHelper
    {
        private const int WM_CLOSE = 0x10;

        private static List<string> trackedMessageBoxes = new List<string>();

        [DllImport("user32.dll")]
        private static extern IntPtr FindWindow(string lpClassName, String lpWindowName);
        [DllImport("user32.dll")]
        private static extern int SendMessage(IntPtr hWnd, int wMsg, IntPtr wParam, IntPtr lParam);
        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount);

        /// <summary>
        /// Close message box.
        /// </summary>
        /// <param name="messageBoxName">Message box caption.</param>
        public static void CloseMessageBox(string messageBoxName)
        {
            IntPtr hwnd = FindWindow(null, messageBoxName);
            if (hwnd != IntPtr.Zero)
                SendMessage(hwnd, WM_CLOSE, IntPtr.Zero, IntPtr.Zero);
        }

        public static DialogResult ShowTrackedMessageBox(string text, string caption, MessageBoxButtons buttons, MessageBoxIcon icon)
        {
            trackedMessageBoxes.Add(caption);
            return MessageBox.Show(text, caption, buttons, icon);
        }

        public static void CloseAllTrackedMessageBoxs()
        {
            foreach (string caption in trackedMessageBoxes)
            {
                CloseMessageBox(caption);
            }
            trackedMessageBoxes.Clear();
        }
    }
}
