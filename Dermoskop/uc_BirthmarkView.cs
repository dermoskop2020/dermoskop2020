﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Dermoskop.Database;
using Dermoskop.Database.Models;
using Dermoskop.Database.Enums;
using Dermoskop.Devices;
using System.Drawing.Drawing2D;
using System.Management.Instrumentation;

namespace Dermoskop
{
    public partial class uc_BirthmarkView : UserControl
    {
        private enum Mode
        {
            SCAN,
            CONFIRM,
            NORMAL,
            INSERT,
        }

        private dbExamination examination = null;
        private dbBirthmark[] birthmarks = null;
        private dbBirthmark currentBirthmark = null;
        private dbBirthmarkImage[] allImages = null;
        private dbBirthmarkImage[] compareImages = null;
        private dbBirthmarkImage birthmarkImageLeft = null;
        private dbBirthmarkImage birthmarkImageRight = null;
        private ImagePositions position = ImagePositions.Front;
        private Mode mode = Mode.NORMAL;

        private VideoController videoController;

        public uc_BirthmarkView()
        {
            InitializeComponent();

            // get the first examination for the current patient
            examination = DatabaseConnector.Query<dbExamination>().Where(x => x.PatientId == Globals.CurrentPatient.Id).OrderBy(x => x.ExaminationTime).FirstOrDefault();

            // fill combobox position items from database
            this.comboBoxPositions.Items.AddRange(Enum.GetNames(typeof(ImagePositions)).Skip(1).ToArray());
            this.comboBoxPositions.SelectedIndex = 0;

            this.imageViewer2.GenerateMipmaps = false;
            this.imageViewer3.GenerateMipmaps = false;

            SetPatientImage();
        }

        private void uc_BirthmarkView_Load(object sender, EventArgs e)
        {
            // load the appropriate exam image
            imageViewer1.BirthmarkMode = Birthmark.Mode.SELECT;

            // set the visibility of the toolbar buttons
            Form1.Instance.SignOutButton.Visible = true;
            Form1.Instance.PatientSelectButton.Visible = true;
            Form1.Instance.NewPatientButton.Visible = true;
            Form1.Instance.ExamViewButton.Visible = true;
            Form1.Instance.NewExamButton.Visible = true;
            Form1.Instance.MarkViewButton.Visible = true;
        }

        //temporary, just to demonstrate the process of adding a new mark
        public static int messageFlag = 0;

        private void btn_NewBirthmark_Click(object sender, EventArgs e)
        {
            if (mode == Mode.NORMAL)
            {
                videoController = Globals.MicroscopeController;
                if (videoController == null)
                {
                    Form1.Instance.FindMicroscopeDevice();
                    if (Globals.MicroscopeController == null)
                    {
                        MessageBox.Show("Could not find microscope. Check the connection and try again.", "Microscope device error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                videoController = Globals.MicroscopeController;

                EnterInsertMode();
            }
            else if(mode == Mode.CONFIRM)
            {
                DialogResult res = MessageBox.Show("Do you want to save the scan?", "Save scan", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (res == DialogResult.Yes)
                {
                    DateTime currentDateTime = DateTime.UtcNow;
                    string hashedFileName = HashClass.HashModel(currentDateTime.ToString()) + ".jpg";
                    dbBirthmarkImage newBirthmarkImage = new dbBirthmarkImage(currentBirthmark.Id, hashedFileName, commentRight.Text, currentDateTime);
                    DatabaseConnector.Insert(newBirthmarkImage);
                    Image bmImage = imageViewer3.Image;
                    string filePath = Path.Combine(Globals.ImageFolder, hashedFileName);
                    bmImage.Save(filePath);
                    EnterNormalMode();
                    if (comboBoxLeft.Items.Count == 1)
                        comboBoxLeft.SelectedIndex = comboBoxLeft.Items.Count - 1;
                    else
                        comboBoxLeft.SelectedIndex = comboBoxLeft.Items.Count - 2;
                    comboBoxRight.SelectedIndex = comboBoxLeft.Items.Count - 2;
                }
            }
            else if(mode == Mode.INSERT)
            {
                EnterNormalMode();
            }
        }

        private void btn_RescanBirthmark_Click(object sender, EventArgs e)
        {
            if (mode == Mode.SCAN)
            {
                DialogResult res = MessageBox.Show("Do you want to exit scan mode?", "Exit scan mode", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (res == DialogResult.Yes)
                {
                    EnterNormalMode();
                }
            }
            else if(mode == Mode.CONFIRM)
            {
                DialogResult res = MessageBox.Show("Do you want to rescan the birthmark?", "Rescan birthmark", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                if (res == DialogResult.Yes)
                {
                    EnterScanMode();
                }
            }
            else
            {
                videoController = Globals.MicroscopeController;
                if (videoController == null)
                {
                    Form1.Instance.FindMicroscopeDevice();
                    if (Globals.MicroscopeController == null)
                    {
                        MessageBox.Show("Could not find microscope. Check the connection and try again.", "Microscope device error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                videoController = Globals.MicroscopeController;

                EnterScanMode();
            }
        }

        private void SetPatientImage()
        {
            dbImage image = DatabaseConnector.Query<dbImage>().Where(x => x.ExaminationId == this.examination.Id && x.Position == this.position).FirstOrDefault();
            var filePath = Path.Combine(Globals.ImageFolder, image.FileName);
            if (File.Exists(filePath))
            {
                if (imageViewer1.Image != null)
                {
                    imageViewer1.Image.Dispose();
                    imageViewer1.Image = null;
                    GC.Collect();
                }
                imageViewer1.Image = Image.FromFile(filePath);
            }
        }

        private void SetLeftImage()
        {
            if (birthmarkImageLeft != null)
            {
                // commentLeft.Text = birthmarkImageLeft.Comment;
                var filePath = Path.Combine(Globals.ImageFolder, birthmarkImageLeft.FileName);
                if (File.Exists(filePath))
                {
                    if (imageViewer2.Image != null)
                    {
                        imageViewer2.Image.Dispose();
                        imageViewer2.Image = null;
                        GC.Collect();
                    }
                    imageViewer2.Image = System.Drawing.Image.FromFile(filePath);
                    commentLeft.Text = birthmarkImageLeft.Comment;
                }
            }
            else
            {
                if (imageViewer2.Image != null)
                {
                    imageViewer2.Image.Dispose();
                }
                imageViewer2.Image = null;
                commentLeft.Text = "";
                GC.Collect();
            }
        }

        private void SetRightImage()
        {
            if (birthmarkImageRight != null)
            {
                // commentLeft.Text = birthmarkImageLeft.Comment;
                var filePath = Path.Combine(Globals.ImageFolder, birthmarkImageRight.FileName);
                if (File.Exists(filePath))
                {
                    if (imageViewer3.Image != null)
                    {
                        imageViewer3.Image.Dispose();
                        imageViewer3.Image = null;
                        GC.Collect();
                    }
                    imageViewer3.Image = System.Drawing.Image.FromFile(filePath);
                    commentRight.Text = birthmarkImageRight.Comment;
                }
            }
            else
            {
                if (imageViewer3.Image != null)
                {
                    imageViewer3.Image.Dispose();
                }
                imageViewer3.Image = null;
                commentRight.Text = "";
                GC.Collect();
            }
        }
        
        private void getBirthmarks()
        {
            // get all birthmarks for a certain person
            birthmarks = DatabaseConnector.Query<dbBirthmark>().Where(x => x.PatientId == Globals.CurrentPatient.Id && x.Position == position).OrderBy(x => x.Id).ToArray();

            imageViewer1.Birthmarks = birthmarks.Select(p => new Birthmark(new Point(p.xLocation, p.yLocation), p.Id.ToString())).ToList();
        }

        private void get_birthmarkimages()
        {
            // set images for the left and the right
            Console.WriteLine(currentBirthmark?.Id);
            allImages = DatabaseConnector.Query<dbBirthmarkImage>().Where(x => x.BirthmarkId == currentBirthmark?.Id).ToArray();


            if (allImages.Any())
            {
                birthmarkImageLeft = (dbBirthmarkImage)comboBoxLeft.SelectedItem;

                comboBoxLeft.DataSource = allImages;
                comboBoxLeft.DisplayMember = "BirthmarkTime";

                compareImages = allImages.Where(x => x != birthmarkImageLeft).ToArray();

                comboBoxRight.DataSource = compareImages;
                comboBoxRight.DisplayMember = "BirthmarkTime";

                birthmarkImageRight = (dbBirthmarkImage)comboBoxRight.SelectedItem;
            } 
            else
            {
                compareImages = null;

                comboBoxLeft.DataSource = null;
                comboBoxLeft.Items.Clear();
                comboBoxRight.DataSource = null;
                comboBoxRight.Items.Clear();

                birthmarkImageRight = null;
            }

            SetLeftImage();
            SetRightImage();
        }

        private void comboBoxPositions_SelectedIndexChanged(object sender, EventArgs e)
        {
            // read the position from the combobox
            position = (ImagePositions)(comboBoxPositions.SelectedIndex + 1);

            // set the patient Image
            SetPatientImage();
            getBirthmarks();
        }

        private void Datetime_Format(object sender, ListControlConvertEventArgs e)
        {
            e.Value = ((dbBirthmarkImage)e.ListItem).BirthmarkTime?.ToString(Globals.DateTimeFormat) ?? string.Empty;
        }

        private void comboBoxLeft_SelectedIndexChanged(object sender, EventArgs e)
        {
            birthmarkImageLeft = (dbBirthmarkImage)comboBoxLeft.SelectedItem;

            if (allImages.Any())
            {
                compareImages = allImages.Where(x => x != birthmarkImageLeft).ToArray();

                comboBoxRight.DataSource = compareImages;
                comboBoxRight.DisplayMember = "BirthmarkTime";

                birthmarkImageRight = (dbBirthmarkImage)comboBoxRight.SelectedItem;
            }

            SetLeftImage();
            SetRightImage();
        }

        private void comboBoxRight_SelectedIndexChanged(object sender, EventArgs e)
        {
            birthmarkImageRight = (dbBirthmarkImage)comboBoxRight.SelectedItem;

            SetRightImage();
        }

        private void imageViewer1_BirthmarkSelected(object sender, BirthmarkEventArgs e)
        {
            if(e.Mark != null)
            {
                var id = int.Parse(e.Mark.Name);
                currentBirthmark = birthmarks.Where(x => x.Id == id).FirstOrDefault();
            }
            else
            {
                currentBirthmark = null;
            }

            this.btn_RescanBirthmark.Enabled = currentBirthmark != null;
            btn_RemoveMark.Enabled = currentBirthmark != null;

            get_birthmarkimages();
        }

        private void imageViewer1_BirthmarkInserted(object sender, BirthmarkInsertEventArgs e)
        {
            // TODO: validate position
            if (e.Valid)
            {
                // save new birthmark to the database
                dbBirthmark newMark = new dbBirthmark(Globals.CurrentPatient.Id, position, e.Location.X, e.Location.Y, DateTime.Now);
                DatabaseConnector.Insert(newMark);

                // reload the marks into the image
                getBirthmarks();

                // has to come after getBirhtmarks, as currentBirthmark is set to null otherwise
                currentBirthmark = newMark;

                // if mark was added ok, then we can switch the mode back to select
                imageViewer1.BirthmarkMode = Birthmark.Mode.SELECT;

                EnterScanMode();
            }
            else
            {
                DialogResult res = MessageBox.Show("Invalid position", "Mark Insert", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void EnterInsertMode()
        {
            imageViewer1.BirthmarkMode = Birthmark.Mode.INSERT;

            comboBoxLeft.DataSource = null;
            comboBoxLeft.Items.Clear();
            comboBoxRight.DataSource = null;
            comboBoxRight.Items.Clear();
            comboBoxRight.Enabled = false;
            comboBoxPositions.Enabled = false;

            btn_RescanBirthmark.Enabled = false;
            btn_NewBirthmark.Enabled = true;
            btn_NewBirthmark.Text = "Cancle insert";
            this.btn_RemoveMark.Enabled = false;

            imageViewer2.Image = null;
            imageViewer3.Image = null;

            mode = Mode.INSERT;
        }

        private void EnterScanMode()
        {
            VideoDevice videoDevice = (VideoDevice)videoController.Device;
            videoDevice.Download += VideoDevice_Download;
            videoDevice.NewVideoFrame += VideoDevice_NewVideoFrame;
            videoController.Run();

            videoController.RequestAction(new ActionRequest(ActionRequest.Command.CAMERA_VIDEO_START, ActionRequestArgs.Empty));

            imageViewer1.Enabled = false;
            imageViewer2.Enabled = false;
            imageViewer3.Enabled = false;

            comboBoxRight.Enabled = false;
            comboBoxPositions.Enabled = false;

            btn_RescanBirthmark.Enabled = true;
            btn_RescanBirthmark.Text = "Cancle scan";
            btn_NewBirthmark.Enabled = false;
            btn_NewBirthmark.Text = "Save scan";
            btn_RemoveMark.Enabled = false;

            mode = Mode.SCAN;

            commentRight.Text = "";

            imageViewer2.Image = null;
            if (comboBoxLeft.Items.Count > 0)
            {
                comboBoxLeft.SelectedIndex = comboBoxLeft.Items.Count - 1;
                comboBoxLeft_SelectedIndexChanged(comboBoxLeft, EventArgs.Empty);
            }
        }

        private void _EnterConfirmScanMode()
        {
            VideoDevice videoDevice = (VideoDevice)videoController.Device;
            videoDevice.Download -= VideoDevice_Download;
            videoDevice.NewVideoFrame -= VideoDevice_NewVideoFrame;

            videoController.RequestAction(new ActionRequest(ActionRequest.Command.CAMERA_VIDEO_STOP, ActionRequestArgs.Empty));

            btn_NewBirthmark.Enabled = true;
            btn_NewBirthmark.Text = "Confirm scan";
            btn_RescanBirthmark.Enabled = true;
            btn_RescanBirthmark.Text = "Retake scan";
            btn_RemoveMark.Enabled = false;

            mode = Mode.CONFIRM;

            commentRight.ReadOnly = false;
            commentRight.Text = "";
            commentRight.BackColor = Color.White;
            commentRight.Focus();
        }

        private void EnterConfirmScanMode()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new Action(() => _EnterConfirmScanMode()));
            }
            else
            {
                _EnterConfirmScanMode();
            }
        }

        private void _EnterNormalMode()
        {
            VideoDevice videoDevice = (VideoDevice)videoController.Device;
            videoDevice.Download -= VideoDevice_Download;
            videoDevice.NewVideoFrame -= VideoDevice_NewVideoFrame;

            videoController.RequestAction(new ActionRequest(ActionRequest.Command.CAMERA_VIDEO_STOP, ActionRequestArgs.Empty));
            videoController.RequestAction(new ActionRequest(ActionRequest.Command.CLOSING, ActionRequestArgs.Empty));

            imageViewer1.Enabled = true;
            imageViewer2.Enabled = true;
            imageViewer3.Enabled = true;

            comboBoxRight.Enabled = true;
            comboBoxLeft.Enabled = true;
            comboBoxPositions.Enabled = true;

            btn_RescanBirthmark.Enabled = true;
            btn_RescanBirthmark.Text = "New Scan";
            btn_NewBirthmark.Enabled = true;
            btn_NewBirthmark.Text = "New Mark";
            btn_RemoveMark.Enabled = currentBirthmark != null;

            commentRight.ReadOnly = true;
            commentRight.BackColor = SystemColors.Control;

            imageViewer1.BirthmarkMode = Birthmark.Mode.SELECT;

            mode = Mode.NORMAL;

            imageViewer3.Image = null;

            get_birthmarkimages();
        }

        private void EnterNormalMode()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new Action(() => _EnterNormalMode()));
            }
            else
            {
                _EnterNormalMode();
            }
        }

        private void VideoDevice_NewVideoFrame(object sender, DownloadEventArgs e)
        {
            imageViewer3.Image = e.Bitmap;
        }

        private void VideoDevice_Download(object sender, DownloadEventArgs e)
        {
            EnterConfirmScanMode();
            imageViewer3.Image = e.Bitmap;
        }

        public void DestroyMarkImages()
        {
            imageViewer1.Image?.Dispose();
            imageViewer1.Image = null;

            imageViewer2.Image?.Dispose();
            imageViewer2.Image = null;
                
            imageViewer3.Image?.Dispose();
            imageViewer3.Image = null;
            GC.Collect();
        }

        private void tableLayoutPanel4_Paint(object sender, PaintEventArgs e)
        {
            Rectangle paddingRectangle = tableLayoutPanel4.ClientRectangle;
            var width = tableLayoutPanel4.Padding.Bottom;

            using (Brush aGradientBrush = new LinearGradientBrush(new Point(0, paddingRectangle.Height - width), new Point(0, paddingRectangle.Height), Color.White, Color.Black))
            {
                using (Pen aGradientPen = new Pen(aGradientBrush, width))
                {
                    e.Graphics.DrawLine(aGradientPen,
                        new Point(paddingRectangle.X, paddingRectangle.Y + paddingRectangle.Height),
                        new Point(paddingRectangle.X + paddingRectangle.Width, paddingRectangle.Y + paddingRectangle.Height));
                }
            }
        }

        private void tableLayoutPanel2_Paint_1(object sender, PaintEventArgs e)
        {
            Rectangle paddingRectangle = tableLayoutPanel2.ClientRectangle;

            using (Pen pen = new Pen(Color.DarkGray, 2.0f))
                e.Graphics.DrawLine(pen,
                    new Point(paddingRectangle.X + paddingRectangle.Width, paddingRectangle.Y + 30),
                    new Point(paddingRectangle.X + paddingRectangle.Width, paddingRectangle.Y + paddingRectangle.Height - 5));
        }

        private void btn_RemoveMark_Click(object sender, EventArgs e)
        {
            if (currentBirthmark != null)
            {
                // we have a birthmark to remove

                DialogResult res = MessageBox.Show("Are you sure you want to remove the birthmark?", "Remove Birthmark", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (res == DialogResult.Yes)
                {
                    // remove birthmark images for the current birthmark
                    var images = DatabaseConnector.Query<dbBirthmarkImage>().Where(x => x.BirthmarkId == currentBirthmark.Id);

                    imageViewer2.Image?.Dispose();
                    imageViewer2.Image = null;
                    imageViewer3.Image?.Dispose();
                    imageViewer3.Image = null;

                    GC.Collect();

                    foreach (var image in images)
                    {
                        var filepath = Path.Combine(Globals.ImageFolder, image.FileName);
                        if (File.Exists(filepath))
                            File.Delete(filepath);

                        DatabaseConnector.Delete(image);
                    }

                    // remove current birthmark
                    DatabaseConnector.Delete(currentBirthmark);

                    // refresh the GUI
                    getBirthmarks();
                }

            }
        }
    }
}
