﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Dermoskop.Database.Models;
using Dermoskop.Devices;

namespace Dermoskop
{
    public static class Globals
    {
        // current user info
        public static dbUser CurentUser;
        public static string UserName;
        public static long UserID;

        // current patient info
        public static dbPatient CurrentPatient;

        // datetime format
        public const string DateTimeFormat = "dd. MM. yyyy";

        // device controllers
        public static SerialController SerialController = null;
        public static CameraController CameraController = null;
        public static VideoController MicroscopeController = null;

        // image folder
        public static string ImageFolder = "./Images/";
        public static string TempFolder = "./Temp/";
    }
}
