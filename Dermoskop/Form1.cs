﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dermoskop.Devices;
using Dermoskop;
using FontAwesome.Sharp;

namespace Dermoskop
{
    public partial class Form1 : Form
    {
        static Form1 _obj;
        public static Form1 Instance
        {
            get
            {
                if (_obj == null)
                    _obj = new Form1();
                return _obj;
            }
        }

        static Type previousControl;

        #region Buttons

        public Panel PnlContainer
        {
            get { return panelContainer; }
            set { panelContainer = value; }
        }

        //public Button BackButton
        //{
        //    get { return btn_Back; }
        //    set { btn_Back = value; }
        //}

        public IconButton SignOutButton
        {
            get { return btn_BarSignOut; }
            set { btn_BarSignOut = value; }
        }

        public IconButton PatientSelectButton
        {
            get { return btn_BarPatientSelect; }
            set { btn_BarPatientSelect = value; }
        }
        public IconButton NewPatientButton
        {
            get { return btn_BarNewPatient; }
            set { btn_BarNewPatient = value; }
        }
        public IconButton ExamViewButton
        {
            get { return btn_BarExamView; }
            set { btn_BarExamView = value; }
        }
        public IconButton NewExamButton
        {
            get { return btn_BarNewExam; }
            set { btn_BarNewExam = value; }
        }

        public IconButton MarkViewButton
        {
            get { return btn_BarMarkView; }
            set { btn_BarMarkView = value; }
        }


        #endregion

        public Form1()
        {
            InitializeComponent();
        }

        private void WelcomeText_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

            if (!Directory.Exists(Globals.ImageFolder))
                Directory.CreateDirectory(Globals.ImageFolder);

            if (!Directory.Exists(Globals.TempFolder))
                Directory.CreateDirectory(Globals.TempFolder);

            //btn_Back.Visible = false;
            btn_BarSignOut.Visible = false;
            btn_BarPatientSelect.Visible = false;
            btn_BarNewPatient.Visible = false;
            btn_BarExamView.Visible = false;
            btn_BarNewExam.Visible = false;
            btn_BarMarkView.Visible = false;
            _obj = this;

            uc_Welcome wlcm = new uc_Welcome();
            wlcm.Dock = DockStyle.Fill;
            panelContainer.Controls.Add(wlcm);

            // find devices
            FindCameraDevice();
            FindMicroscopeDevice();
            // Serial devices are found asynchronously, so we need to setup the event handling.
            SerialFramework.SerialDevicesFound += SerialFramework_SerialDevicesFound;
            FindSerialDevice();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Directory.Exists(Globals.TempFolder))
                Directory.Delete(Globals.TempFolder, true);

            if (PnlContainer.Controls.ContainsKey("uc_NewExamination"))
            {
                ((uc_NewExamination)PnlContainer.Controls["uc_NewExamination"]).Destroy();
            }

            if (Globals.SerialController != null)
            {
                Globals.SerialController.RequestAction(new ActionRequest(ActionRequest.Command.SHUT_DOWN, ActionRequestArgs.Empty));
            }
            if (Globals.CameraController != null)
            {
                Globals.CameraController.RequestAction(new ActionRequest(ActionRequest.Command.SHUT_DOWN, ActionRequestArgs.Empty));
            }
            if (Globals.MicroscopeController != null)
            {
                Globals.MicroscopeController.RequestAction(new ActionRequest(ActionRequest.Command.SHUT_DOWN, ActionRequestArgs.Empty));
            }
        }

        #region Device functions
        private void SerialFramework_SerialDevicesFound(object sender, EventArgs e)
        {
            // Set Globals on main thread, just in case.
            this.Invoke((MethodInvoker)delegate
            {
                // First device found is our used device.
                if (Globals.SerialController == null)
                {
                    Globals.SerialController = new SerialController((SerialDevice)sender);
                    Globals.SerialController.Run();
                    Globals.SerialController.RequestAction(new ActionRequest(ActionRequest.Command.SERIAL_CAPTURE_STOP, ActionRequestArgs.Empty));
                    Globals.SerialController.RequestAction(new ActionRequest(ActionRequest.Command.CLOSING, ActionRequestArgs.Empty));
                }
                else
                {
                    ((SerialDevice)sender).Port.Close();
                }
            });
        }

        public void FindCameraDevice()
        { 
            try
            {
                EosCameraDevice cameraDevice = EosFramework.GetCameraDevices()[0];
                Globals.CameraController = new EosCameraController(cameraDevice);
                EosFramework.SetCameraController((EosCameraController)Globals.CameraController);
            }
            catch
            {
                Globals.CameraController = null;
            }
        }

        public void FindMicroscopeDevice()
        {
            try
            {
                DSCameraDevice microscopeDevice = DSFramework.GetCameraDevices()[0];
                Globals.MicroscopeController = new DSCameraController(microscopeDevice);
            }
            catch
            {
                Globals.MicroscopeController = null;
            }
        }

        public void FindSerialDevice()
        {
            SerialFramework.FindSerialDevice();
        }

        #endregion

        #region Button Functions

        private void btn_BarSignOut_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Are you sure you want to sign out? All unsaved progress will be lost.", "Return to Login", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (res == DialogResult.Yes)
            {
                if (PnlContainer.Controls.ContainsKey("uc_NewExamination"))
                {
                    ((uc_NewExamination)PnlContainer.Controls["uc_NewExamination"]).Destroy();
                }

                PnlContainer.Controls.Clear();

                uc_Login wlcm = new uc_Login();
                wlcm.Dock = DockStyle.Fill;
                panelContainer.Controls.Add(wlcm);
                PnlContainer.Controls["uc_Login"].BringToFront();


                SignOutButton.Visible = false;
                PatientSelectButton.Visible = false;
                NewPatientButton.Visible = false;
                ExamViewButton.Visible = false;
                NewExamButton.Visible = false;
                MarkViewButton.Visible = false;
            }
        }

        private void btn_BarPatientSelect_Click(object sender, EventArgs e)
        {
            if (btn_ToolbarButton_BeforeClick())
            {
                // if you want any more action to be done here
                btn_ToolbarButton_AfterClick<uc_PatientSelect>();
            }
        }

        private void btn_BarNewPatient_Click(object sender, EventArgs e)
        {
            if (btn_ToolbarButton_BeforeClick())
            {
                // if you want any more action to be done here
                btn_ToolbarButton_AfterClick<uc_NewPatient>();
            }
        }

        private void btn_BarExamView_Click(object sender, EventArgs e)
        {
            if (btn_ToolbarButton_BeforeClick())
            {
                // if you want any more action to be done here
                btn_ToolbarButton_AfterClick<uc_ExaminationView>();
            }
        }

        private void btn_BarNewExam_Click(object sender, EventArgs e)
        {
            if (btn_ToolbarButton_BeforeClick())
            {
                // if you want any more action to be done here
                var ucs = PnlContainer.Controls;
                btn_ToolbarButton_AfterClick<uc_NewExamination>();
            }
        }

        private void btn_BarMarkView_Click(object sender, EventArgs e)
        {
            if (btn_ToolbarButton_BeforeClick())
            {
                // if you want any more action to be done here
                btn_ToolbarButton_AfterClick<uc_BirthmarkView>();
            }
        }


        private bool btn_ToolbarButton_BeforeClick()
        {
            var ucs = PnlContainer.Controls;
            bool safe = true;

            // destroy all user controls that are not welcome 
            if (ucs.ContainsKey("uc_PatientSelect"))
            {
                previousControl = typeof(uc_PatientSelect);
                ucs.RemoveByKey("uc_PatientSelect");
            }
            if (ucs.ContainsKey("uc_NewPatient"))
            {
                // carefully destroy
                DialogResult res = MessageBox.Show("Are you sure you do not want to add a new patient? All unsaved progress will be lost.", "Return to PatientSelect", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                if (res == DialogResult.Yes)
                {
                    previousControl = typeof(uc_NewPatient);
                    PnlContainer.Controls.RemoveByKey("uc_NewPatient");
                }
                else
                {
                    safe = false;
                }
            }
            if (ucs.ContainsKey("uc_ExaminationView"))
            {
                
                previousControl = typeof(uc_ExaminationView);
                ((uc_ExaminationView)ucs["uc_ExaminationView"]).DestroyImages();
                ucs.RemoveByKey("uc_ExaminationView");
            }
            if (ucs.ContainsKey("uc_NewExamination"))
            {
                DialogResult res = MessageBox.Show("Are you sure you want to stop the current examination and begin a new one? All unsaved progress will be lost", "Start a new examination", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                if (res == DialogResult.Yes)
                {
                    previousControl = typeof(uc_NewExamination);
                    uc_NewExamination ucne = (uc_NewExamination)ucs["uc_NewExamination"];
                    ucne.Destroy();
                    ucs.RemoveByKey("uc_NewExamination");
                }
                else
                {
                    safe = false;
                }
            }
            if (ucs.ContainsKey("uc_BirthmarkView"))
            {
                
                previousControl = typeof(uc_BirthmarkView);
                ((uc_BirthmarkView)ucs["uc_BirthmarkView"]).DestroyMarkImages();
                ucs.RemoveByKey("uc_BirthmarkView");
                

            }

            return safe;
        }

        private void btn_ToolbarButton_AfterClick<T>() where T: UserControl, new()
        {
            T bmv = new T
            {
                Dock = DockStyle.Fill
            };
            PnlContainer.Controls.Add(bmv);

            PnlContainer.Controls[typeof(T).Name].BringToFront();
        }

        /// <summary>
        /// Switch back to previus control.
        /// </summary>
        public void SwitchToPreviusControl()
        {
            if (btn_ToolbarButton_BeforeClick())
            {
                Control pc = (Control)Activator.CreateInstance(previousControl);
                pc.Dock = DockStyle.Fill;
                PnlContainer.Controls.Add(pc);
                PnlContainer.Controls[previousControl.Name].BringToFront();
            }
        }

        public void AddControlFromType(Type controlType)
        {
            Control previousControl = (Control)Activator.CreateInstance(controlType);
        }

        private void toolTip1_Popup(object sender, PopupEventArgs e)
        {

        }
    }

    #endregion
}
