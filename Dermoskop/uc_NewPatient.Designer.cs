﻿using System;

namespace Dermoskop
{
    partial class uc_NewPatient
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            NameText.Focus();
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SurnameText = new System.Windows.Forms.MaskedTextBox();
            this.NameText = new System.Windows.Forms.TextBox();
            this.btn_NewPatientConfirm = new System.Windows.Forms.Button();
            this.fileSystemWatcher1 = new System.IO.FileSystemWatcher();
            this.BirthDatePicker = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.HealthInsuranceNumberText = new System.Windows.Forms.MaskedTextBox();
            this.SexDropdown = new System.Windows.Forms.DomainUpDown();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.labelNameReq = new System.Windows.Forms.Label();
            this.labelSurnameReq = new System.Windows.Forms.Label();
            this.labelSexRequired = new System.Windows.Forms.Label();
            this.labelHINReq = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(40, 144);
            this.label3.Margin = new System.Windows.Forms.Padding(10, 0, 0, 5);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 21);
            this.label3.TabIndex = 4;
            this.label3.Text = "Surname:";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(40, 224);
            this.label2.Margin = new System.Windows.Forms.Padding(10, 0, 0, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 21);
            this.label2.TabIndex = 6;
            this.label2.Text = "Birth Date:";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(40, 64);
            this.label1.Margin = new System.Windows.Forms.Padding(10, 0, 2, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 21);
            this.label1.TabIndex = 6;
            this.label1.Text = "Name:";
            // 
            // SurnameText
            // 
            this.SurnameText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SurnameText.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SurnameText.Location = new System.Drawing.Point(40, 175);
            this.SurnameText.Margin = new System.Windows.Forms.Padding(10, 5, 10, 2);
            this.SurnameText.Name = "SurnameText";
            this.SurnameText.Size = new System.Drawing.Size(280, 27);
            this.SurnameText.TabIndex = 2;
            this.SurnameText.TextChanged += new System.EventHandler(this.uc_TextChanged);
            // 
            // NameText
            // 
            this.NameText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.NameText.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NameText.Location = new System.Drawing.Point(40, 95);
            this.NameText.Margin = new System.Windows.Forms.Padding(10, 5, 10, 2);
            this.NameText.Name = "NameText";
            this.NameText.Size = new System.Drawing.Size(280, 27);
            this.NameText.TabIndex = 1;
            this.NameText.TextChanged += new System.EventHandler(this.uc_TextChanged);
            // 
            // btn_NewPatientConfirm
            // 
            this.btn_NewPatientConfirm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_NewPatientConfirm.FlatAppearance.BorderSize = 0;
            this.btn_NewPatientConfirm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_NewPatientConfirm.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_NewPatientConfirm.Location = new System.Drawing.Point(34, 470);
            this.btn_NewPatientConfirm.Margin = new System.Windows.Forms.Padding(4, 20, 4, 4);
            this.btn_NewPatientConfirm.Name = "btn_NewPatientConfirm";
            this.btn_NewPatientConfirm.Size = new System.Drawing.Size(292, 62);
            this.btn_NewPatientConfirm.TabIndex = 6;
            this.btn_NewPatientConfirm.Text = "Confirm";
            this.btn_NewPatientConfirm.UseVisualStyleBackColor = true;
            this.btn_NewPatientConfirm.Click += new System.EventHandler(this.btn_NewPatientConfirm_Click);
            // 
            // fileSystemWatcher1
            // 
            this.fileSystemWatcher1.EnableRaisingEvents = true;
            this.fileSystemWatcher1.SynchronizingObject = this;
            // 
            // BirthDatePicker
            // 
            this.BirthDatePicker.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.BirthDatePicker.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.BirthDatePicker.CalendarTitleBackColor = System.Drawing.SystemColors.ControlText;
            this.BirthDatePicker.CalendarTitleForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.BirthDatePicker.CustomFormat = "dd. MM. yyyy";
            this.BirthDatePicker.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.BirthDatePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.BirthDatePicker.Location = new System.Drawing.Point(40, 255);
            this.BirthDatePicker.Margin = new System.Windows.Forms.Padding(10, 5, 10, 2);
            this.BirthDatePicker.MinimumSize = new System.Drawing.Size(0, 25);
            this.BirthDatePicker.Name = "BirthDatePicker";
            this.BirthDatePicker.Size = new System.Drawing.Size(280, 25);
            this.BirthDatePicker.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(40, 304);
            this.label4.Margin = new System.Windows.Forms.Padding(10, 0, 0, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 21);
            this.label4.TabIndex = 6;
            this.label4.Text = "Sex:";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(40, 384);
            this.label5.Margin = new System.Windows.Forms.Padding(10, 0, 0, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(214, 21);
            this.label5.TabIndex = 6;
            this.label5.Text = "Health Insurance Number:";
            // 
            // HealthInsuranceNumberText
            // 
            this.HealthInsuranceNumberText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.HealthInsuranceNumberText.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.HealthInsuranceNumberText.Location = new System.Drawing.Point(40, 415);
            this.HealthInsuranceNumberText.Margin = new System.Windows.Forms.Padding(10, 5, 10, 2);
            this.HealthInsuranceNumberText.Name = "HealthInsuranceNumberText";
            this.HealthInsuranceNumberText.Size = new System.Drawing.Size(280, 27);
            this.HealthInsuranceNumberText.TabIndex = 5;
            this.HealthInsuranceNumberText.TextChanged += new System.EventHandler(this.uc_TextChanged);
            this.HealthInsuranceNumberText.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.hin_KeyPress);
            // 
            // SexDropdown
            // 
            this.SexDropdown.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SexDropdown.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.SexDropdown.Items.Add("Male");
            this.SexDropdown.Items.Add("Female");
            this.SexDropdown.Location = new System.Drawing.Point(40, 335);
            this.SexDropdown.Margin = new System.Windows.Forms.Padding(10, 5, 10, 2);
            this.SexDropdown.Name = "SexDropdown";
            this.SexDropdown.Size = new System.Drawing.Size(280, 26);
            this.SexDropdown.TabIndex = 4;
            this.SexDropdown.Text = "Choose";
            this.SexDropdown.SelectedItemChanged += new System.EventHandler(this.SexDropdown_SelectedItemChanged);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1008, 562);
            this.tableLayoutPanel1.TabIndex = 8;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 300F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel2.Controls.Add(this.btn_NewPatientConfirm, 0, 10);
            this.tableLayoutPanel2.Controls.Add(this.HealthInsuranceNumberText, 0, 9);
            this.tableLayoutPanel2.Controls.Add(this.label5, 0, 8);
            this.tableLayoutPanel2.Controls.Add(this.SexDropdown, 0, 7);
            this.tableLayoutPanel2.Controls.Add(this.label4, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.BirthDatePicker, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.SurnameText, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.NameText, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.labelNameReq, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.labelSurnameReq, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.labelSexRequired, 1, 7);
            this.tableLayoutPanel2.Controls.Add(this.labelHINReq, 1, 9);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(288, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.Padding = new System.Windows.Forms.Padding(30, 50, 10, 20);
            this.tableLayoutPanel2.RowCount = 11;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(432, 556);
            this.tableLayoutPanel2.TabIndex = 0;
            this.tableLayoutPanel2.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel2_Paint);
            // 
            // labelNameReq
            // 
            this.labelNameReq.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelNameReq.AutoSize = true;
            this.labelNameReq.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.labelNameReq.ForeColor = System.Drawing.Color.Red;
            this.labelNameReq.Location = new System.Drawing.Point(333, 100);
            this.labelNameReq.Name = "labelNameReq";
            this.labelNameReq.Size = new System.Drawing.Size(82, 19);
            this.labelNameReq.TabIndex = 7;
            this.labelNameReq.Text = "* Required";
            this.labelNameReq.Visible = false;
            // 
            // labelSurnameReq
            // 
            this.labelSurnameReq.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelSurnameReq.AutoSize = true;
            this.labelSurnameReq.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.labelSurnameReq.ForeColor = System.Drawing.Color.Red;
            this.labelSurnameReq.Location = new System.Drawing.Point(333, 180);
            this.labelSurnameReq.Name = "labelSurnameReq";
            this.labelSurnameReq.Size = new System.Drawing.Size(82, 19);
            this.labelSurnameReq.TabIndex = 8;
            this.labelSurnameReq.Text = "* Required";
            this.labelSurnameReq.Visible = false;
            // 
            // labelSexRequired
            // 
            this.labelSexRequired.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelSexRequired.AutoSize = true;
            this.labelSexRequired.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.labelSexRequired.ForeColor = System.Drawing.Color.Red;
            this.labelSexRequired.Location = new System.Drawing.Point(333, 340);
            this.labelSexRequired.Name = "labelSexRequired";
            this.labelSexRequired.Size = new System.Drawing.Size(82, 19);
            this.labelSexRequired.TabIndex = 10;
            this.labelSexRequired.Text = "* Required";
            this.labelSexRequired.Visible = false;
            // 
            // labelHINReq
            // 
            this.labelHINReq.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelHINReq.AutoSize = true;
            this.labelHINReq.Font = new System.Drawing.Font("Century Gothic", 10F);
            this.labelHINReq.ForeColor = System.Drawing.Color.Red;
            this.labelHINReq.Location = new System.Drawing.Point(333, 420);
            this.labelHINReq.Name = "labelHINReq";
            this.labelHINReq.Size = new System.Drawing.Size(82, 19);
            this.labelHINReq.TabIndex = 11;
            this.labelHINReq.Text = "* Required";
            this.labelHINReq.Visible = false;
            // 
            // uc_NewPatient
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "uc_NewPatient";
            this.Size = new System.Drawing.Size(1008, 562);
            this.Load += new System.EventHandler(this.uc_NewPatient_Load);
            ((System.ComponentModel.ISupportInitialize)(this.fileSystemWatcher1)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MaskedTextBox SurnameText;
        private System.Windows.Forms.TextBox NameText;
        private System.Windows.Forms.Button btn_NewPatientConfirm;
        private System.IO.FileSystemWatcher fileSystemWatcher1;
        private System.Windows.Forms.MaskedTextBox HealthInsuranceNumberText;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker BirthDatePicker;
        private System.Windows.Forms.DomainUpDown SexDropdown;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label labelNameReq;
        private System.Windows.Forms.Label labelSexRequired;
        private System.Windows.Forms.Label labelSurnameReq;
        private System.Windows.Forms.Label labelHINReq;
    }
}
