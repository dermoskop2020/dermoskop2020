﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Dermoskop.Database;
using Dermoskop.Database.Models;
using Dermoskop.Database.Enums;

namespace Dermoskop
{
    public partial class uc_NewPatient : UserControl
    {
        public uc_NewPatient()
        {
            InitializeComponent();
        }

        private void hin_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void btn_NewPatientConfirm_Click(object sender, EventArgs e)
        {
            // check if form is filled in
            if (!this.CheckRequirements())
                return;

            // add new patient to the database
            dbPatient NewPatient = new dbPatient(NameText.Text, SurnameText.Text, (Sexes)(SexDropdown.SelectedIndex + 1), BirthDatePicker.Value, HealthInsuranceNumberText.Text);
            DatabaseConnector.Insert(NewPatient);

            // set new patient as a current patient
            Globals.CurrentPatient = NewPatient;

            if (!Form1.Instance.PnlContainer.Controls.ContainsKey("uc_ExaminationView"))
            {
                uc_ExaminationView ev = new uc_ExaminationView();
                ev.Dock = DockStyle.Fill;
                Form1.Instance.PnlContainer.Controls.Add(ev);
            }
            Form1.Instance.PnlContainer.Controls["uc_ExaminationView"].BringToFront();
            Form1.Instance.PnlContainer.Controls.RemoveByKey("uc_NewPatient");

            Form1.Instance.ExamViewButton.Enabled = true;
            Form1.Instance.NewExamButton.Enabled = true;
            Form1.Instance.MarkViewButton.Enabled = true;
        }

        private void uc_NewPatient_Load(object sender, EventArgs e)
        {
            // set initial focus
            BirthDatePicker.Value = DateTime.Now.AddYears(-30);

            // set the visibility of the toolbar buttons
            Form1.Instance.SignOutButton.Visible = true;
            Form1.Instance.PatientSelectButton.Visible = true;
            Form1.Instance.NewPatientButton.Visible = true;
            Form1.Instance.ExamViewButton.Visible = false;
            Form1.Instance.NewExamButton.Visible = false;
            Form1.Instance.MarkViewButton.Visible = false;
        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {
            Rectangle paddingRectangle = tableLayoutPanel2.ClientRectangle;
            Padding p = tableLayoutPanel2.Padding;
            paddingRectangle.X += p.Left;
            paddingRectangle.Y += p.Top;
            paddingRectangle.Width -= (p.Left + p.Right + 90);
            paddingRectangle.Height -= (p.Bottom + p.Top);
            Pen pen = new Pen(Color.DarkGray, 2.0f);
            Pen pen2 = new Pen(Color.Black, 2.0f);
            e.Graphics.DrawLine(pen, new Point(paddingRectangle.X, paddingRectangle.Y), new Point(paddingRectangle.X + paddingRectangle.Width, paddingRectangle.Y));
            e.Graphics.DrawLines(pen2, new PointF[] {
                new PointF(paddingRectangle.X, paddingRectangle.Y),
                new PointF(paddingRectangle.X, paddingRectangle.Y + paddingRectangle.Height),
                new PointF(paddingRectangle.X + paddingRectangle.Width, paddingRectangle.Y + paddingRectangle.Height),
                new PointF(paddingRectangle.X + paddingRectangle.Width, paddingRectangle.Y)
            });
        }
        
        private bool CheckRequirements(bool ShowLabels = true)
        {
            bool FormCorrect = true;

            ShowLabels = (labelNameReq.Visible || labelSurnameReq.Visible || labelSexRequired.Visible || labelHINReq.Visible || ShowLabels);

            if (ShowLabels)
            {
                // hide all labels
                this.labelNameReq.Visible = false;
                this.labelSurnameReq.Visible = false;
                this.labelSexRequired.Visible = false;
                this.labelHINReq.Visible = false;
            }

            // check if name consist of at least two characters
            if (this.NameText.Text.Length < 2)
            {
                FormCorrect = false;
                if (ShowLabels)
                    this.labelNameReq.Visible = true;
            }

            // check if surname consist of at least two characters
            if (this.SurnameText.Text.Length < 2)
            {
                FormCorrect = false;
                if (ShowLabels)
                    this.labelSurnameReq.Visible = true;
            }

            // check if sex is not none
            if (this.SexDropdown.SelectedItem == null)
            {
                FormCorrect = false;
                if (ShowLabels)
                    this.labelSexRequired.Visible = true;
            }

            // check if HIN is at least 3 digits long
            if (this.HealthInsuranceNumberText.Text.Length < 3)
            {
                FormCorrect = false;
                if (ShowLabels)
                    this.labelHINReq.Visible = true;
            }

            return FormCorrect;
        }

        private void ActivateConfirmButton()
        {
            this.btn_NewPatientConfirm.Enabled = CheckRequirements(false);
        }

        private void uc_TextChanged(object sender, EventArgs e)
        {
            // ActivateConfirmButton();
            CheckRequirements(false);
        }

        private void SexDropdown_SelectedItemChanged(object sender, EventArgs e)
        {
            // ActivateConfirmButton();
            CheckRequirements(false);
        }
    }
}
