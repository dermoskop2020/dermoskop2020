﻿namespace Dermoskop
{
    partial class uc_BirthmarkView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_NewBirthmark = new System.Windows.Forms.Button();
            this.btn_RescanBirthmark = new System.Windows.Forms.Button();
            this.lbl_commentLeft = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.commentLeft = new System.Windows.Forms.RichTextBox();
            this.commentRight = new System.Windows.Forms.RichTextBox();
            this.comboBoxLeft = new System.Windows.Forms.ComboBox();
            this.comboBoxRight = new System.Windows.Forms.ComboBox();
            this.comboBoxPositions = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btn_RemoveMark = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.imageViewer2 = new Dermoskop.ImageViewer();
            this.imageViewer3 = new Dermoskop.ImageViewer();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.imageViewer1 = new Dermoskop.ImageViewer();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageViewer2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageViewer3)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageViewer1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_NewBirthmark
            // 
            this.btn_NewBirthmark.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_NewBirthmark.FlatAppearance.BorderSize = 0;
            this.btn_NewBirthmark.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_NewBirthmark.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_NewBirthmark.Location = new System.Drawing.Point(0, 0);
            this.btn_NewBirthmark.Margin = new System.Windows.Forms.Padding(0);
            this.btn_NewBirthmark.Name = "btn_NewBirthmark";
            this.btn_NewBirthmark.Size = new System.Drawing.Size(120, 44);
            this.btn_NewBirthmark.TabIndex = 56;
            this.btn_NewBirthmark.Text = "New Mark";
            this.btn_NewBirthmark.UseVisualStyleBackColor = true;
            this.btn_NewBirthmark.Click += new System.EventHandler(this.btn_NewBirthmark_Click);
            // 
            // btn_RescanBirthmark
            // 
            this.btn_RescanBirthmark.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_RescanBirthmark.Enabled = false;
            this.btn_RescanBirthmark.FlatAppearance.BorderSize = 0;
            this.btn_RescanBirthmark.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_RescanBirthmark.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_RescanBirthmark.Location = new System.Drawing.Point(120, 0);
            this.btn_RescanBirthmark.Margin = new System.Windows.Forms.Padding(0);
            this.btn_RescanBirthmark.Name = "btn_RescanBirthmark";
            this.btn_RescanBirthmark.Size = new System.Drawing.Size(120, 44);
            this.btn_RescanBirthmark.TabIndex = 57;
            this.btn_RescanBirthmark.Text = "New Scan";
            this.btn_RescanBirthmark.UseVisualStyleBackColor = true;
            this.btn_RescanBirthmark.Click += new System.EventHandler(this.btn_RescanBirthmark_Click);
            // 
            // lbl_commentLeft
            // 
            this.lbl_commentLeft.AutoSize = true;
            this.lbl_commentLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_commentLeft.Location = new System.Drawing.Point(3, 340);
            this.lbl_commentLeft.Name = "lbl_commentLeft";
            this.lbl_commentLeft.Size = new System.Drawing.Size(97, 20);
            this.lbl_commentLeft.TabIndex = 58;
            this.lbl_commentLeft.Text = "Comment:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(330, 340);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 20);
            this.label1.TabIndex = 59;
            this.label1.Text = "Comment:";
            // 
            // commentLeft
            // 
            this.commentLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.commentLeft.Location = new System.Drawing.Point(3, 363);
            this.commentLeft.Name = "commentLeft";
            this.commentLeft.ReadOnly = true;
            this.commentLeft.Size = new System.Drawing.Size(321, 114);
            this.commentLeft.TabIndex = 60;
            this.commentLeft.Text = "";
            // 
            // commentRight
            // 
            this.commentRight.BackColor = System.Drawing.SystemColors.Control;
            this.commentRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.commentRight.Location = new System.Drawing.Point(330, 363);
            this.commentRight.Name = "commentRight";
            this.commentRight.ReadOnly = true;
            this.commentRight.Size = new System.Drawing.Size(321, 114);
            this.commentRight.TabIndex = 61;
            this.commentRight.Text = "";
            // 
            // comboBoxLeft
            // 
            this.comboBoxLeft.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxLeft.FormattingEnabled = true;
            this.comboBoxLeft.Location = new System.Drawing.Point(3, 3);
            this.comboBoxLeft.Name = "comboBoxLeft";
            this.comboBoxLeft.Size = new System.Drawing.Size(321, 24);
            this.comboBoxLeft.TabIndex = 62;
            this.comboBoxLeft.SelectedIndexChanged += new System.EventHandler(this.comboBoxLeft_SelectedIndexChanged);
            this.comboBoxLeft.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.Datetime_Format);
            // 
            // comboBoxRight
            // 
            this.comboBoxRight.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxRight.FormattingEnabled = true;
            this.comboBoxRight.Location = new System.Drawing.Point(330, 3);
            this.comboBoxRight.Name = "comboBoxRight";
            this.comboBoxRight.Size = new System.Drawing.Size(321, 24);
            this.comboBoxRight.TabIndex = 63;
            this.comboBoxRight.SelectedIndexChanged += new System.EventHandler(this.comboBoxRight_SelectedIndexChanged);
            this.comboBoxRight.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.Datetime_Format);
            // 
            // comboBoxPositions
            // 
            this.comboBoxPositions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxPositions.FormattingEnabled = true;
            this.comboBoxPositions.Location = new System.Drawing.Point(3, 33);
            this.comboBoxPositions.Name = "comboBoxPositions";
            this.comboBoxPositions.Size = new System.Drawing.Size(318, 24);
            this.comboBoxPositions.TabIndex = 64;
            this.comboBoxPositions.SelectedIndexChanged += new System.EventHandler(this.comboBoxPositions_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(2, 5);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 5, 2, 5);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 20);
            this.label5.TabIndex = 65;
            this.label5.Text = "Position:";
            // 
            // btn_RemoveMark
            // 
            this.btn_RemoveMark.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btn_RemoveMark.Enabled = false;
            this.btn_RemoveMark.FlatAppearance.BorderSize = 0;
            this.btn_RemoveMark.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_RemoveMark.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_RemoveMark.Location = new System.Drawing.Point(240, 0);
            this.btn_RemoveMark.Margin = new System.Windows.Forms.Padding(0);
            this.btn_RemoveMark.Name = "btn_RemoveMark";
            this.btn_RemoveMark.Size = new System.Drawing.Size(120, 44);
            this.btn_RemoveMark.TabIndex = 66;
            this.btn_RemoveMark.Text = "Remove Mark";
            this.btn_RemoveMark.UseVisualStyleBackColor = true;
            this.btn_RemoveMark.Click += new System.EventHandler(this.btn_RemoveMark_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 67F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1008, 562);
            this.tableLayoutPanel1.TabIndex = 67;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel5, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(335, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.Padding = new System.Windows.Forms.Padding(5);
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(670, 556);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 4;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Controls.Add(this.btn_NewBirthmark, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.btn_RemoveMark, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.btn_RescanBirthmark, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(8, 8);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.Padding = new System.Windows.Forms.Padding(0, 0, 0, 10);
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(654, 54);
            this.tableLayoutPanel4.TabIndex = 0;
            this.tableLayoutPanel4.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel4_Paint);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.commentRight, 1, 3);
            this.tableLayoutPanel5.Controls.Add(this.comboBoxRight, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.label1, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.imageViewer2, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.lbl_commentLeft, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.comboBoxLeft, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.imageViewer3, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.commentLeft, 0, 3);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(8, 68);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 4;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(654, 480);
            this.tableLayoutPanel5.TabIndex = 1;
            // 
            // imageViewer2
            // 
            this.imageViewer2.BirthmarkMode = Dermoskop.Birthmark.Mode.HIDE;
            this.imageViewer2.Birthmarks = null;
            this.imageViewer2.CameraLocation = new System.Drawing.Point(0, 0);
            this.imageViewer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imageViewer2.GenerateMipmaps = true;
            this.imageViewer2.Image = null;
            this.imageViewer2.Location = new System.Drawing.Point(3, 33);
            this.imageViewer2.Name = "imageViewer2";
            this.imageViewer2.Size = new System.Drawing.Size(321, 304);
            this.imageViewer2.TabIndex = 54;
            this.imageViewer2.TabStop = false;
            this.imageViewer2.Zoom = 1F;
            this.imageViewer2.ZoomSpeed = 0.0005F;
            // 
            // imageViewer3
            // 
            this.imageViewer3.BirthmarkMode = Dermoskop.Birthmark.Mode.HIDE;
            this.imageViewer3.Birthmarks = null;
            this.imageViewer3.CameraLocation = new System.Drawing.Point(0, 0);
            this.imageViewer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imageViewer3.GenerateMipmaps = true;
            this.imageViewer3.Image = null;
            this.imageViewer3.Location = new System.Drawing.Point(330, 33);
            this.imageViewer3.Name = "imageViewer3";
            this.imageViewer3.Size = new System.Drawing.Size(321, 304);
            this.imageViewer3.TabIndex = 55;
            this.imageViewer3.TabStop = false;
            this.imageViewer3.Zoom = 1F;
            this.imageViewer3.ZoomSpeed = 0.0005F;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.comboBoxPositions, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.imageViewer1, 0, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.Padding = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(326, 556);
            this.tableLayoutPanel2.TabIndex = 2;
            this.tableLayoutPanel2.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel2_Paint_1);
            // 
            // imageViewer1
            // 
            this.imageViewer1.BirthmarkMode = Dermoskop.Birthmark.Mode.SELECT;
            this.imageViewer1.Birthmarks = null;
            this.imageViewer1.CameraLocation = new System.Drawing.Point(0, 0);
            this.imageViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imageViewer1.GenerateMipmaps = true;
            this.imageViewer1.Image = null;
            this.imageViewer1.Location = new System.Drawing.Point(3, 63);
            this.imageViewer1.Name = "imageViewer1";
            this.imageViewer1.Size = new System.Drawing.Size(318, 490);
            this.imageViewer1.TabIndex = 0;
            this.imageViewer1.TabStop = false;
            this.imageViewer1.Zoom = 1F;
            this.imageViewer1.ZoomSpeed = 0.0005F;
            this.imageViewer1.BirthmarkSelected += new System.EventHandler<Dermoskop.BirthmarkEventArgs>(this.imageViewer1_BirthmarkSelected);
            this.imageViewer1.BirthmarkInserted += new System.EventHandler<Dermoskop.BirthmarkInsertEventArgs>(this.imageViewer1_BirthmarkInserted);
            // 
            // uc_BirthmarkView
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "uc_BirthmarkView";
            this.Size = new System.Drawing.Size(1008, 562);
            this.Load += new System.EventHandler(this.uc_BirthmarkView_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageViewer2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageViewer3)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageViewer1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ImageViewer imageViewer1;
        private ImageViewer imageViewer2;
        private ImageViewer imageViewer3;
        private System.Windows.Forms.Button btn_NewBirthmark;
        private System.Windows.Forms.Button btn_RescanBirthmark;
        private System.Windows.Forms.Label lbl_commentLeft;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox commentLeft;
        private System.Windows.Forms.RichTextBox commentRight;
        private System.Windows.Forms.ComboBox comboBoxLeft;
        private System.Windows.Forms.ComboBox comboBoxRight;
        private System.Windows.Forms.ComboBox comboBoxPositions;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btn_RemoveMark;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
    }
}
