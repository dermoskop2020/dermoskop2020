﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dermoskop
{
    public partial class uc_Welcome : UserControl
    {
        public uc_Welcome()
        {
            InitializeComponent();
        }

        private void uc_Welcome_Load(object sender, EventArgs e)
        {

        }

        private void btn_SignIn_Click(object sender, EventArgs e)
        {
            if(!Form1.Instance.PnlContainer.Controls.ContainsKey("uc_Login"))
            {
                uc_Login ps = new uc_Login();
                ps.Dock = DockStyle.Fill;
                Form1.Instance.PnlContainer.Controls.Add(ps);
            }
            Form1.Instance.PnlContainer.Controls["uc_Login"].BringToFront();

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://dermoskop.eu/");
        }
    }
}
