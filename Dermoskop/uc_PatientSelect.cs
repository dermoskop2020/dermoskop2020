﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Media;

using Dermoskop.Database;
using Dermoskop.Database.Models;
using System.Drawing.Drawing2D;
using System.IO;

namespace Dermoskop
{
    public partial class uc_PatientSelect : UserControl
    {
        public uc_PatientSelect()
        {
            InitializeComponent();
            
            var patients = DatabaseConnector.Query<dbPatient>();
            listView1.Items.Clear();
            // fill listview with patients from database
            listView1.Items.AddRange(patients.Select(p => new ListViewItem(new string[]
            {
                p.Name,
                p.Surname,
                p.Age.ToString(),
                p.Sex.ToString(),
                p.BirthDate.ToString(),
                p.HealthInsuranceNumber,
            })
            {
                Tag = p.Id
            }).ToArray());
            comboBox1.Text = "Name";
        }

        private void uc_PatientSelect_Load(object sender, EventArgs e)
        {
            // set initial focus
            if (listView1.Items.Count > 0)
                listView1.Items[0].Selected = true;
            listView1.Select();

            // set the visibility of the toolbar buttons
            Form1.Instance.SignOutButton.Visible = true;
            Form1.Instance.PatientSelectButton.Visible = true;
            Form1.Instance.NewPatientButton.Visible = true;
            Form1.Instance.ExamViewButton.Visible = false;
            Form1.Instance.NewExamButton.Visible = false;
            Form1.Instance.MarkViewButton.Visible = false;
        }

        private void btn_NewPatient_Click(object sender, EventArgs e)
        {
            if (!Form1.Instance.PnlContainer.Controls.ContainsKey("uc_NewPatient"))
            {
                uc_NewPatient np = new uc_NewPatient();
                np.Dock = DockStyle.Fill;
                Form1.Instance.PnlContainer.Controls.Add(np);
            }
            Form1.Instance.PnlContainer.Controls["uc_NewPatient"].BringToFront();
            Form1.Instance.PnlContainer.Controls.RemoveByKey("uc_PatientSelect");
        }

        private void btn_PatientSelectConfirm_Click(object sender, EventArgs e)
        {
            // save chosen patient to global current patient
            if (listView1.SelectedItems.Count < 1)
            {
                SystemSounds.Beep.Play();
                return;
            }

            Globals.CurrentPatient = DatabaseConnector.Query<dbPatient>().Where(x => x.Id == (long)listView1.SelectedItems[0].Tag).First();

            // switch to examination view
            if (!Form1.Instance.PnlContainer.Controls.ContainsKey("uc_ExaminationView"))
            {
                uc_ExaminationView ev = new uc_ExaminationView();
                ev.Dock = DockStyle.Fill;
                Form1.Instance.PnlContainer.Controls.Add(ev);
            }
            Form1.Instance.PnlContainer.Controls["uc_ExaminationView"].BringToFront();
            Form1.Instance.PnlContainer.Controls.RemoveByKey("uc_PatientSelect");

            Form1.Instance.ExamViewButton.Enabled = true;
            Form1.Instance.NewExamButton.Enabled = true;
            Form1.Instance.MarkViewButton.Enabled = true;
        }

        // The column we are currently using for sorting.
        private ColumnHeader SortingColumn = null;

        // Sort on this column.
        private void listView1_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            // Get the new sorting column.
            ColumnHeader new_sorting_column = listView1.Columns[e.Column];

            // Figure out the new sorting order.
            SortOrder sort_order;
            if (SortingColumn == null)
            {
                // New column. Sort ascending.
                sort_order = SortOrder.Ascending;
            }
            else
            {
                // See if this is the same column.
                if (new_sorting_column == SortingColumn)
                {
                    // Same column. Switch the sort order.
                    if (SortingColumn.Text.StartsWith("↗ "))
                    {
                        sort_order = SortOrder.Descending;
                    }
                    else
                    {
                        sort_order = SortOrder.Ascending;
                    }
                }
                else
                {
                    // New column. Sort ascending.
                    sort_order = SortOrder.Ascending;
                }

                // Remove the old sort indicator.
                SortingColumn.Text = SortingColumn.Text.Substring(2);
            }

            // Display the new sort order.
            SortingColumn = new_sorting_column;
            if (sort_order == SortOrder.Ascending)
            {
                SortingColumn.Text = "↗ " + SortingColumn.Text;
            }
            else
            {
                SortingColumn.Text = "↘ " + SortingColumn.Text;
            }

            // Create a comparer.
            listView1.ListViewItemSorter = new ListViewComparer(e.Column, sort_order);

            // Sort.
            listView1.Sort();
        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {
            Rectangle paddingRectangle = tableLayoutPanel2.ClientRectangle;
            Padding p = tableLayoutPanel2.Padding;
            paddingRectangle.X += p.Left;
            paddingRectangle.Width -= (p.Left + p.Right);
            paddingRectangle.Height -= (p.Bottom);

            using (Brush aGradientBrush = new LinearGradientBrush(new Point(0, paddingRectangle.Height - 4), new Point(0, paddingRectangle.Height), Color.White, Color.Black))
            {
                using (Pen aGradientPen = new Pen(aGradientBrush, 6.0f))
                {
                    e.Graphics.DrawLine(aGradientPen,
                        new Point(paddingRectangle.X, paddingRectangle.Y + paddingRectangle.Height),
                        new Point(paddingRectangle.X + paddingRectangle.Width, paddingRectangle.Y + paddingRectangle.Height));
                }
            }
        }

        private void listView1_Resize(object sender, EventArgs e)
        {
            ListView lv = (ListView)sender;
            var columnMinWidths = new int[]{ 102, 123, 81, 67, 146, 138 };
            var width = columnMinWidths.Sum();
            var realWidth = lv.Width - 5 - SystemInformation.VerticalScrollBarWidth;
            var colWidth = 0;

            for(int i = 0; i < columnMinWidths.Length; i++)
            {
                colWidth = (int)Math.Round(realWidth * ((float)columnMinWidths[i] / width));
                lv.Columns[i].Width = colWidth > columnMinWidths[i] ? colWidth : columnMinWidths[i];
            }
        }

        private void listView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                btn_PatientSelectConfirm_Click(sender, (EventArgs)e);
        }

        private void listView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            btn_PatientSelectConfirm_Click(sender, (EventArgs)e);
        }
        private void FilterPatients()
        {
            if (!String.IsNullOrEmpty(textBox1.Text))
            {
                var patients = DatabaseConnector.Query<dbPatient>();
                listView1.Items.Clear();

                System.Collections.Generic.IEnumerable<dbPatient> filterPatients = null;
                switch (comboBox1.Text)
                {
                    case "Name":
                        filterPatients = patients.Where(x => x.Name.ToLower().Contains(textBox1.Text.ToLower()));
                        break;
                    case "Surname":
                        filterPatients = patients.Where(x => x.Surname.ToLower().Contains(textBox1.Text.ToLower()));
                        break;
                    case "Age":
                        filterPatients = patients.Where(x => x.Age.ToString().ToLower().Contains(textBox1.Text.ToLower()));
                        break;
                    case "Birth Date":
                        filterPatients = patients.Where(x => x.BirthDate.ToString().ToLower().Contains(textBox1.Text.ToLower()));
                        break;
                    case "HI Number":
                        filterPatients = patients.Where(x => x.HealthInsuranceNumber.ToLower().Contains(textBox1.Text.ToLower()));
                        break;
                }
                listView1.Items.AddRange(filterPatients.Select(p => new ListViewItem(new string[]
                        {
                            p.Name,
                            p.Surname,
                            p.Age.ToString(),
                            p.Sex.ToString(),
                            p.BirthDate.ToString(),
                            p.HealthInsuranceNumber,
                        })
                {
                    Tag = p.Id
                }).ToArray()); 

            }
            else
            {
                var patients = DatabaseConnector.Query<dbPatient>();
                listView1.Items.Clear();
                // fill listview with patients from database
                listView1.Items.AddRange(patients.Select(p => new ListViewItem(new string[]
                {
                    p.Name,
                    p.Surname,
                    p.Age.ToString(),
                    p.Sex.ToString(),
                    p.BirthDate.ToString(),
                    p.HealthInsuranceNumber,
                })
                {
                    Tag = p.Id
                }).ToArray());
            }
        }


        private void TextChangeUpdate(object sender, EventArgs e)
        {
            FilterPatients();
        }

        private void Dropdown_Update(object sender, EventArgs e)
        {
            FilterPatients();
            textBox1.Focus();
        }

        private void buttonDeletePatient_Click(object sender, EventArgs e)
        {
            // check if there is any patient selected
            if (listView1.SelectedItems.Count < 1)
            {
                SystemSounds.Beep.Play();
                return;
            }

            var item = listView1.SelectedItems[0];

            // prepare the patient that you want to delete from the database
            dbPatient del = new dbPatient((long)item.Tag);

            // open a dialog before you delete it from the database
            DialogResult res = MessageBox.Show($"Are you sure you want to remove patient {item.SubItems[0].Text} {item.SubItems[1].Text}?", "Delete patient", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
            if (res == DialogResult.Yes)
            {
                // delete all the examinations
                var exams = DatabaseConnector.Query<dbExamination>().Where(x => x.PatientId == del.Id);

                // delete exam data for the user
                foreach(var exam in exams)
                {
                    var images = DatabaseConnector.Query<dbImage>().Where(x => x.ExaminationId == exam.Id);

                    foreach(var image in images)
                    {
                        var filepath = Path.Combine(Globals.ImageFolder, image.FileName);
                        if (File.Exists(filepath))
                            File.Delete(filepath);
                        DatabaseConnector.Delete(image);
                    }

                    DatabaseConnector.Delete(exam);
                }

                // delete BM data for the user
                var bms = DatabaseConnector.Query<dbBirthmark>().Where(x => x.PatientId == del.Id);

                foreach(var bm in bms)
                {
                    var bmimgs = DatabaseConnector.Query<dbBirthmarkImage>().Where(x => x.BirthmarkId == bm.Id);

                    foreach(var image in bmimgs)
                    {
                        var filepath = Path.Combine(Globals.ImageFolder, image.FileName);
                        if (File.Exists(filepath))
                            File.Delete(filepath);
                        DatabaseConnector.Delete(image);
                    }

                    DatabaseConnector.Delete(bm);
                }

                // delete patient from the database
                DatabaseConnector.Delete(del);

                // repopulate the listview
                FilterPatients();

                // select the first one in the view
                if (listView1.Items.Count > 0)
                    listView1.Items[0].Selected = true;
                listView1.Select();

                // disable delete button if you run out of patients
                buttonDeletePatient.Enabled = listView1.Items.Count > 0;
            }
        }
    }

    // Compares two ListView items based on a selected column.
    public class ListViewComparer : System.Collections.IComparer
    {
        private int ColumnNumber;
        private SortOrder SortOrder;

        public ListViewComparer(int column_number,
            SortOrder sort_order)
        {
            ColumnNumber = column_number;
            SortOrder = sort_order;
        }

        // Compare two ListViewItems.
        public int Compare(object object_x, object object_y)
        {
            // Get the objects as ListViewItems.
            ListViewItem item_x = object_x as ListViewItem;
            ListViewItem item_y = object_y as ListViewItem;

            // Get the corresponding sub-item values.
            string string_x;
            if (item_x.SubItems.Count <= ColumnNumber)
            {
                string_x = "";
            }
            else
            {
                string_x = item_x.SubItems[ColumnNumber].Text;
            }

            string string_y;
            if (item_y.SubItems.Count <= ColumnNumber)
            {
                string_y = "";
            }
            else
            {
                string_y = item_y.SubItems[ColumnNumber].Text;
            }

            // Compare them.
            int result;
            double double_x, double_y;
            if (double.TryParse(string_x, out double_x) &&
                double.TryParse(string_y, out double_y))
            {
                // Treat as a number.
                result = double_x.CompareTo(double_y);
            }
            else
            {
                DateTime date_x, date_y;
                if (DateTime.TryParse(string_x, out date_x) &&
                    DateTime.TryParse(string_y, out date_y))
                {
                    // Treat as a date.
                    result = date_x.CompareTo(date_y);
                }
                else
                {
                    // Treat as a string.
                    result = string_x.CompareTo(string_y);
                }
            }

            // Return the correct result depending on whether
            // we're sorting ascending or descending.
            if (SortOrder == SortOrder.Ascending)
            {
                return result;
            }
            else
            {
                return -result;
            }
        }
    }
}
