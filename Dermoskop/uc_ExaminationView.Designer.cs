﻿using System;
using System.Linq;
using System.Windows.Forms;

using Dermoskop.Database.Enums;

namespace Dermoskop
{
    partial class uc_ExaminationView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_NewExamEditComment = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_NewExam = new System.Windows.Forms.Button();
            this.commentRight = new System.Windows.Forms.RichTextBox();
            this.commentLeft = new System.Windows.Forms.RichTextBox();
            this.comboBoxPosition = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btn_ImportExam = new System.Windows.Forms.Button();
            this.btn_ExportExam = new System.Windows.Forms.Button();
            this.examinationsLeft = new System.Windows.Forms.ComboBox();
            this.examinationsRight = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_ViewBirthmark = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.labelPatientName = new System.Windows.Forms.Label();
            this.labelBirthDate = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.imageViewerRight = new Dermoskop.ImageViewer();
            this.imageViewerLeft = new Dermoskop.ImageViewer();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageViewerRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageViewerLeft)).BeginInit();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_NewExamEditComment
            // 
            this.btn_NewExamEditComment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_NewExamEditComment.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_NewExamEditComment.Location = new System.Drawing.Point(5, 388);
            this.btn_NewExamEditComment.Margin = new System.Windows.Forms.Padding(5, 5, 5, 25);
            this.btn_NewExamEditComment.Name = "btn_NewExamEditComment";
            this.btn_NewExamEditComment.Size = new System.Drawing.Size(77, 34);
            this.btn_NewExamEditComment.TabIndex = 10;
            this.btn_NewExamEditComment.Text = "Edit";
            this.btn_NewExamEditComment.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(308, 274);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(302, 20);
            this.label4.TabIndex = 42;
            this.label4.Text = "Comments:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Left;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(2, 274);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 20);
            this.label3.TabIndex = 41;
            this.label3.Text = "Comments:";
            // 
            // btn_NewExam
            // 
            this.btn_NewExam.FlatAppearance.BorderSize = 0;
            this.btn_NewExam.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_NewExam.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_NewExam.Location = new System.Drawing.Point(0, 162);
            this.btn_NewExam.Margin = new System.Windows.Forms.Padding(2);
            this.btn_NewExam.Name = "btn_NewExam";
            this.btn_NewExam.Size = new System.Drawing.Size(282, 33);
            this.btn_NewExam.TabIndex = 4;
            this.btn_NewExam.Text = "New Examination";
            this.btn_NewExam.UseVisualStyleBackColor = true;
            this.btn_NewExam.Click += new System.EventHandler(this.btn_NewExam_Click);
            // 
            // commentRight
            // 
            this.commentRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.commentRight.Location = new System.Drawing.Point(308, 296);
            this.commentRight.Margin = new System.Windows.Forms.Padding(2);
            this.commentRight.Name = "commentRight";
            this.commentRight.ReadOnly = true;
            this.commentRight.Size = new System.Drawing.Size(302, 126);
            this.commentRight.TabIndex = 9;
            this.commentRight.Text = "";
            // 
            // commentLeft
            // 
            this.commentLeft.BackColor = System.Drawing.SystemColors.Control;
            this.commentLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.commentLeft.Location = new System.Drawing.Point(2, 296);
            this.commentLeft.Margin = new System.Windows.Forms.Padding(2);
            this.commentLeft.Name = "commentLeft";
            this.commentLeft.ReadOnly = true;
            this.commentLeft.Size = new System.Drawing.Size(302, 126);
            this.commentLeft.TabIndex = 8;
            this.commentLeft.Text = "";
            // 
            // comboBoxPosition
            // 
            this.comboBoxPosition.Dock = System.Windows.Forms.DockStyle.Fill;
            this.comboBoxPosition.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxPosition.FormattingEnabled = true;
            this.comboBoxPosition.Location = new System.Drawing.Point(83, 23);
            this.comboBoxPosition.Name = "comboBoxPosition";
            this.comboBoxPosition.Size = new System.Drawing.Size(520, 33);
            this.comboBoxPosition.TabIndex = 3;
            this.comboBoxPosition.SelectedIndexChanged += new System.EventHandler(this.comboBoxPosition_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(2, 25);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 5, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 22);
            this.label5.TabIndex = 49;
            this.label5.Text = "Position:";
            // 
            // btn_ImportExam
            // 
            this.btn_ImportExam.Enabled = false;
            this.btn_ImportExam.FlatAppearance.BorderSize = 0;
            this.btn_ImportExam.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_ImportExam.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ImportExam.Location = new System.Drawing.Point(2, 37);
            this.btn_ImportExam.Margin = new System.Windows.Forms.Padding(2);
            this.btn_ImportExam.Name = "btn_ImportExam";
            this.btn_ImportExam.Size = new System.Drawing.Size(280, 31);
            this.btn_ImportExam.TabIndex = 5;
            this.btn_ImportExam.TabStop = false;
            this.btn_ImportExam.Text = "Import Examination";
            this.btn_ImportExam.UseVisualStyleBackColor = true;
            // 
            // btn_ExportExam
            // 
            this.btn_ExportExam.Enabled = false;
            this.btn_ExportExam.FlatAppearance.BorderSize = 0;
            this.btn_ExportExam.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_ExportExam.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ExportExam.Location = new System.Drawing.Point(2, 72);
            this.btn_ExportExam.Margin = new System.Windows.Forms.Padding(2);
            this.btn_ExportExam.Name = "btn_ExportExam";
            this.btn_ExportExam.Size = new System.Drawing.Size(280, 33);
            this.btn_ExportExam.TabIndex = 6;
            this.btn_ExportExam.TabStop = false;
            this.btn_ExportExam.Text = "Export Examination";
            this.btn_ExportExam.UseVisualStyleBackColor = true;
            // 
            // examinationsLeft
            // 
            this.examinationsLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.examinationsLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.examinationsLeft.FormattingEnabled = true;
            this.examinationsLeft.Location = new System.Drawing.Point(3, 10);
            this.examinationsLeft.Name = "examinationsLeft";
            this.examinationsLeft.Size = new System.Drawing.Size(297, 33);
            this.examinationsLeft.TabIndex = 1;
            this.examinationsLeft.SelectionChangeCommitted += new System.EventHandler(this.ExaminationLeft_SelectedIndexChanged);
            this.examinationsLeft.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.Examination_Format);
            // 
            // examinationsRight
            // 
            this.examinationsRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.examinationsRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.examinationsRight.FormattingEnabled = true;
            this.examinationsRight.Location = new System.Drawing.Point(306, 10);
            this.examinationsRight.Name = "examinationsRight";
            this.examinationsRight.Size = new System.Drawing.Size(297, 33);
            this.examinationsRight.TabIndex = 2;
            this.examinationsRight.SelectionChangeCommitted += new System.EventHandler(this.ExaminationRight_SelectedIndexChanged);
            this.examinationsRight.Format += new System.Windows.Forms.ListControlConvertEventHandler(this.Examination_Format);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.btn_ViewBirthmark);
            this.panel1.Controls.Add(this.tableLayoutPanel1);
            this.panel1.Controls.Add(this.btn_NewExam);
            this.panel1.Controls.Add(this.tableLayoutPanel2);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(284, 562);
            this.panel1.TabIndex = 55;
            // 
            // btn_ViewBirthmark
            // 
            this.btn_ViewBirthmark.FlatAppearance.BorderSize = 0;
            this.btn_ViewBirthmark.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_ViewBirthmark.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ViewBirthmark.Location = new System.Drawing.Point(1, 265);
            this.btn_ViewBirthmark.Margin = new System.Windows.Forms.Padding(2);
            this.btn_ViewBirthmark.Name = "btn_ViewBirthmark";
            this.btn_ViewBirthmark.Size = new System.Drawing.Size(282, 33);
            this.btn_ViewBirthmark.TabIndex = 57;
            this.btn_ViewBirthmark.Text = "View Birthmark";
            this.btn_ViewBirthmark.UseVisualStyleBackColor = true;
            this.btn_ViewBirthmark.Click += new System.EventHandler(this.btn_ViewBirthmark_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelPatientName, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelBirthDate, 0, 2);
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tableLayoutPanel1.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.FixedSize;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 60);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(5, 5, 30, 5);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.Padding = new System.Windows.Forms.Padding(20, 5, 30, 5);
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(284, 95);
            this.tableLayoutPanel1.TabIndex = 0;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(22, 5);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 29);
            this.label1.TabIndex = 37;
            this.label1.Text = "Patient Info";
            // 
            // labelPatientName
            // 
            this.labelPatientName.AutoSize = true;
            this.labelPatientName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.labelPatientName.Location = new System.Drawing.Point(23, 39);
            this.labelPatientName.Name = "labelPatientName";
            this.labelPatientName.Size = new System.Drawing.Size(100, 25);
            this.labelPatientName.TabIndex = 38;
            this.labelPatientName.Text = "Full Name";
            // 
            // labelBirthDate
            // 
            this.labelBirthDate.AutoSize = true;
            this.labelBirthDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.labelBirthDate.Location = new System.Drawing.Point(23, 64);
            this.labelBirthDate.Name = "labelBirthDate";
            this.labelBirthDate.Size = new System.Drawing.Size(118, 25);
            this.labelBirthDate.TabIndex = 39;
            this.labelBirthDate.Text = "Date of Birth";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.btn_ImportExam, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.btn_ExportExam, 0, 2);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 162);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(284, 107);
            this.tableLayoutPanel2.TabIndex = 56;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.AutoSize = true;
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.imageViewerRight, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.imageViewerLeft, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.commentLeft, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.commentRight, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.label4, 1, 1);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 115);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.Padding = new System.Windows.Forms.Padding(0, 0, 0, 20);
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(612, 444);
            this.tableLayoutPanel3.TabIndex = 59;
            // 
            // imageViewerRight
            // 
            this.imageViewerRight.BirthmarkMode = Dermoskop.Birthmark.Mode.HIDE;
            this.imageViewerRight.Birthmarks = null;
            this.imageViewerRight.CameraLocation = new System.Drawing.Point(0, 0);
            this.imageViewerRight.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imageViewerRight.GenerateMipmaps = true;
            this.imageViewerRight.Image = null;
            this.imageViewerRight.Location = new System.Drawing.Point(309, 3);
            this.imageViewerRight.Name = "imageViewerRight";
            this.imageViewerRight.Size = new System.Drawing.Size(300, 268);
            this.imageViewerRight.TabIndex = 58;
            this.imageViewerRight.TabStop = false;
            this.imageViewerRight.Zoom = 1F;
            this.imageViewerRight.ZoomSpeed = 0.0005F;
            // 
            // imageViewerLeft
            // 
            this.imageViewerLeft.BirthmarkMode = Dermoskop.Birthmark.Mode.HIDE;
            this.imageViewerLeft.Birthmarks = null;
            this.imageViewerLeft.CameraLocation = new System.Drawing.Point(0, 0);
            this.imageViewerLeft.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imageViewerLeft.GenerateMipmaps = true;
            this.imageViewerLeft.Image = null;
            this.imageViewerLeft.ImageLocation = "";
            this.imageViewerLeft.Location = new System.Drawing.Point(3, 3);
            this.imageViewerLeft.Name = "imageViewerLeft";
            this.imageViewerLeft.Size = new System.Drawing.Size(300, 268);
            this.imageViewerLeft.TabIndex = 57;
            this.imageViewerLeft.TabStop = false;
            this.imageViewerLeft.Zoom = 1F;
            this.imageViewerLeft.ZoomSpeed = 0.0005F;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel3, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.panel3, 1, 1);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(290, 0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.92883F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80.07117F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(718, 562);
            this.tableLayoutPanel4.TabIndex = 60;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tableLayoutPanel5);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(612, 106);
            this.panel2.TabIndex = 0;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel6, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel7, 0, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(612, 106);
            this.tableLayoutPanel5.TabIndex = 50;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Controls.Add(this.comboBoxPosition, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.Padding = new System.Windows.Forms.Padding(0, 20, 0, 0);
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(606, 47);
            this.tableLayoutPanel6.TabIndex = 0;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Controls.Add(this.examinationsRight, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.examinationsLeft, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 56);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.Padding = new System.Windows.Forms.Padding(0, 7, 0, 5);
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(606, 47);
            this.tableLayoutPanel7.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btn_NewExamEditComment);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(621, 115);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(94, 444);
            this.panel3.TabIndex = 60;
            // 
            // uc_ExaminationView
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tableLayoutPanel4);
            this.Name = "uc_ExaminationView";
            this.Size = new System.Drawing.Size(1008, 562);
            this.Load += new System.EventHandler(this.uc_ExaminationView_Load);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imageViewerRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageViewerLeft)).EndInit();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btn_NewExamEditComment;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_NewExam;
        private System.Windows.Forms.RichTextBox commentRight;
        private System.Windows.Forms.RichTextBox commentLeft;
        private System.Windows.Forms.ComboBox comboBoxPosition;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btn_ImportExam;
        private System.Windows.Forms.Button btn_ExportExam;
        private System.Windows.Forms.ComboBox examinationsLeft;
        private System.Windows.Forms.ComboBox examinationsRight;
        private Panel panel1;
        private TableLayoutPanel tableLayoutPanel1;
        private Label label1;
        private Label labelPatientName;
        private Label labelBirthDate;
        private TableLayoutPanel tableLayoutPanel2;
        private TableLayoutPanel tableLayoutPanel3;
        private TableLayoutPanel tableLayoutPanel4;
        private Panel panel2;
        private TableLayoutPanel tableLayoutPanel5;
        private TableLayoutPanel tableLayoutPanel6;
        private TableLayoutPanel tableLayoutPanel7;
        private Panel panel3;
        private Button btn_ViewBirthmark;
        private ImageViewer imageViewerLeft;
        private ImageViewer imageViewerRight;
    }
}
