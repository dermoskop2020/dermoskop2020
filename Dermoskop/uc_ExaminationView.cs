﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Dermoskop.Database;
using Dermoskop.Database.Models;
using Dermoskop.Database.Enums;

using Dermoskop.Devices;

namespace Dermoskop
{
    public partial class uc_ExaminationView : UserControl
    {

        private List<dbExamination> examinations = null;
        private List<dbExamination> examinationsToCompare = null;
        private dbExamination LeftExamination = null; // examination on the left
        private dbExamination RightExamination = null;
        private ImagePositions position;

        public uc_ExaminationView()
        {
            InitializeComponent();

            Console.WriteLine(Form1.Instance.PnlContainer.Controls.Count);

            // gotta do it here, otherwise designer overwrites it
            this.comboBoxPosition.Items.AddRange(Enum.GetNames(typeof(ImagePositions)).Skip(1).ToArray());
            comboBoxPosition.SelectedIndex = 0;

            // set patient info into the left menu bar
            labelPatientName.Text = Globals.CurrentPatient.FullName;
            labelBirthDate.Text = Globals.CurrentPatient.BirthDate?.ToString(Globals.DateTimeFormat) ?? string.Empty;

            // get exams for the person picked
            examinations = DatabaseConnector.Query<dbExamination>().Where(x => x.PatientId == Globals.CurrentPatient.Id).ToList();
            examinationsLeft.DataSource = examinations;
            examinationsLeft.DisplayMember = "ExaminationTime";

            LeftExamination = (dbExamination)examinationsLeft.SelectedItem;

            examinationsToCompare = examinations.Where(x => x != LeftExamination).ToList();
            examinationsRight.DataSource = examinationsToCompare;
            examinationsRight.DisplayMember = "ExaminationTime";

            if (examinationsLeft.Items.Count > 0)
            {
                if (examinationsLeft.Items.Count == 1)
                    examinationsLeft.SelectedIndex = examinationsLeft.Items.Count - 1;
                else
                {
                    examinationsLeft.SelectedIndex = examinationsLeft.Items.Count - 2;
                    examinationsRight.SelectedIndex = examinationsRight.Items.Count - 1;
                }
            }

            RightExamination = (dbExamination)examinationsRight.SelectedItem;

            SetLeftExamination();
            SetRightExamination();
        }

        private void btn_NewExam_Click(object sender, EventArgs e)
        {   /*
            if (Form1.Instance.PnlContainer.Controls.ContainsKey("uc_NewExamination"))
            {
                Form1.Instance.PnlContainer.Controls.RemoveByKey("uc_NewExamination");
            }
            if (Form1.Instance.PnlContainer.Controls.ContainsKey("uc_ConfirmScan"))
            {   
                Form1.Instance.PnlContainer.Controls.RemoveByKey("uc_ConfirmScan");
            }
            */

            //Check for connection with Devices
            while (Globals.SerialController == null || Globals.CameraController == null)
            {
                if (Globals.SerialController == null)
                {
                    if (SerialFramework.IsSearchInProgress())
                    {
                        MessageBox.Show("Arduino search is currently in progress.", "Search in progress", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        DialogResult res2 = MessageBox.Show("Arduino is not connected! Retry connection or abort?", "Connection error", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);
                        if (res2 == DialogResult.Retry)
                        {
                            Form1.Instance.FindSerialDevice();
                        }
                        else return;
                    }
                }
                else if (Globals.CameraController == null)
                {
                    DialogResult res2 = MessageBox.Show("Camera is not connected! Retry connection or abort?", "Connection error", MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);
                    if (res2 == DialogResult.Retry)
                    {
                        Form1.Instance.FindCameraDevice();
                        if (Globals.CameraController != null)
                            break;
                    }
                    else return;
                }
            }

            //Devices are connected, now open form NewExamination
            if (!Form1.Instance.PnlContainer.Controls.ContainsKey("uc_NewExamination"))
            {
                uc_NewExamination ne = new uc_NewExamination();
                ne.Dock = DockStyle.Fill;
                Form1.Instance.PnlContainer.Controls.Add(ne);
            }
            Form1.Instance.PnlContainer.Controls["uc_NewExamination"].BringToFront();
            Form1.Instance.PnlContainer.Controls.RemoveByKey("uc_ExaminationView");
        }

        private void btn_Back_Click(object sender, EventArgs e)
        {
            if (!Form1.Instance.PnlContainer.Controls.ContainsKey("uc_PatientSelect"))
            {
                uc_PatientSelect np = new uc_PatientSelect();
                np.Dock = DockStyle.Fill;
                Form1.Instance.PnlContainer.Controls.Add(np);
            }
            Form1.Instance.PnlContainer.Controls["uc_PatientSelect"].BringToFront();
            Form1.Instance.PnlContainer.Controls.RemoveByKey("uc_ExaminationView");
        }

        private void Examination_Format(object sender, ListControlConvertEventArgs e)
        {
            e.Value = ((dbExamination)e.ListItem).ExaminationTime?.ToString(Globals.DateTimeFormat) ?? string.Empty;
        }

        private void ExaminationLeft_SelectedIndexChanged(object sender, EventArgs e)
        {
            // TODO: change the selection of the right examination to the previous left one
            LeftExamination = (dbExamination)examinationsLeft.SelectedItem;

            examinationsToCompare = examinations.Where(x => x != LeftExamination).ToList();
            examinationsRight.DataSource = examinationsToCompare;
            examinationsRight.DisplayMember = "ExaminationTime";

            RightExamination = (dbExamination)examinationsRight.SelectedItem;

            SetLeftExamination();
            SetRightExamination();
        }

        private void ExaminationRight_SelectedIndexChanged(object sender, EventArgs e)
        {
            RightExamination = (dbExamination)examinationsRight.SelectedItem;

            SetRightExamination();
        }

        private void SetLeftExamination()
        {
            if (LeftExamination != null)
            {
                commentLeft.Text = LeftExamination.Comment;

                dbImage imLeft = DatabaseConnector.Query<dbImage>().Where(x => x.ExaminationId == LeftExamination.Id && x.Position == position).FirstOrDefault();
                var filePath = Path.Combine(Globals.ImageFolder, imLeft.FileName);
                if (File.Exists(filePath))
                {
                    if (imageViewerLeft.Image != null)
                    {
                        imageViewerLeft.Image.Dispose();
                        imageViewerLeft.Image = null;
                        GC.Collect();
                    }
                    imageViewerLeft.Image = Image.FromFile(filePath);

                }
            }
        }

        private void SetRightExamination()
        {
            if (RightExamination != null)
            {
                commentRight.Text = RightExamination.Comment;

                dbImage imRight = DatabaseConnector.Query<dbImage>().Where(x => x.ExaminationId == RightExamination.Id && x.Position == position).FirstOrDefault();
                var filePath = Path.Combine(Globals.ImageFolder, imRight.FileName);
                if (File.Exists(filePath))
                {
                    if(imageViewerRight.Image != null)
                    {
                        imageViewerRight.Image.Dispose();
                        imageViewerRight.Image = null;
                        GC.Collect();
                    }

                    imageViewerRight.Image = Image.FromFile(filePath);


                }
            }
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {
            Rectangle paddingRectangle = tableLayoutPanel1.ClientRectangle;
            Padding p = tableLayoutPanel1.Padding;
            paddingRectangle.X += p.Left;
            paddingRectangle.Width -= (p.Left + p.Right);
            paddingRectangle.Height -= (p.Bottom);
            Pen pen = new Pen(Color.Black, 2.0f);
            e.Graphics.DrawRectangle(pen, paddingRectangle);
        }

        private void comboBoxPosition_SelectedIndexChanged(object sender, EventArgs e)
        {
            position = (ImagePositions)(comboBoxPosition.SelectedIndex + 1);

            SetLeftExamination();
            SetRightExamination();
        }

        private void uc_ExaminationView_Load(object sender, EventArgs e)
        {
            // set initial focus
            examinationsLeft.Focus();

            // set the visibility of the toolbar buttons
            Form1.Instance.SignOutButton.Visible = true;
            Form1.Instance.PatientSelectButton.Visible = true;
            Form1.Instance.NewPatientButton.Visible = true;
            Form1.Instance.ExamViewButton.Visible = true;
            Form1.Instance.NewExamButton.Visible = true;
            Form1.Instance.MarkViewButton.Visible = true;
        }

        private void btn_ViewBirthmark_Click(object sender, EventArgs e)
        {
            if (!Form1.Instance.PnlContainer.Controls.ContainsKey("uc_BirthmarkView"))
            {
                uc_BirthmarkView bv = new uc_BirthmarkView();
                bv.Dock = DockStyle.Fill;
                Form1.Instance.PnlContainer.Controls.Add(bv);
            }
            Form1.Instance.PnlContainer.Controls["uc_BirthmarkView"].BringToFront();
        }

        public void DestroyImages()
        {
            imageViewerRight.Image?.Dispose();
            imageViewerRight.Image = null;
            imageViewerLeft.Image?.Dispose();
            imageViewerLeft.Image = null;
            GC.Collect();
        }
    }
}
