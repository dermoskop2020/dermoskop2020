﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dermoskop.Devices;
using Dermoskop.Database;
using Dermoskop.ImageProcessing;
using Dermoskop.Properties;
using Dermoskop.Database.Enums;
using Dermoskop.Database.Models;
using System.Threading;

namespace Dermoskop
{
    public partial class uc_NewExamination : UserControl
    {
        private SerialController serialController;
        private CameraController cameraController;

        private int positionCount = 1; //Should be: 1, 2, 3, 4 as value in ImagePositions Enum
        private int imageCount = 0;
        List<Bitmap> bitmaps = new List<Bitmap>();
        private string[] finalImages = new string[4];
        private string outputImage = null;

        private bool destroyed = false;
        private float downloadTimoutTimer = 4.0f;

        private HuginToolsInterface hugin = new HuginToolsInterface();

        //Constructor
        public uc_NewExamination()
        {
            InitializeComponent();
        }

        #region Button Clicks

        private void btn_ImageCapture_Click(object sender, EventArgs e)
        {
            if (outputImage != null)
            {
                DialogResult res = MessageBoxHelper.ShowTrackedMessageBox("Are you sure you want to retake the full body scan? Previous scan will be lost.", "Recapture Image", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (res == DialogResult.Yes)
                {
                    outputImage = null;
                    btn_ImageCapture.Enabled = false;
                    btn_SaveContinue.Enabled = false;
                    cameraController.RequestAction(new ActionRequest(ActionRequest.Command.CAMERA_TAKE_PICTURE, ActionRequestArgs.Empty)); //Start full body scan
                    pictureBox_NewExam.Image?.Dispose();
                    pictureBox_NewExam.Image = null;
                    GC.Collect();
                }
            }
            else
            {
                btn_ImageCapture.Enabled = false;
                btn_SaveContinue.Enabled = false; 
                Thread.Sleep(100); // Make sure serial commands get executed beforehand.
                if (imageCount == 0)
                {
                    Serial_Error(serialController.Device, 
                        new DeviceErrorEventArgs("Failed to obtain the number of positions from serial device"));
                }
                else
                {
                    cameraController.RequestAction(new ActionRequest(ActionRequest.Command.CAMERA_TAKE_PICTURE, ActionRequestArgs.Empty));
                }
            }
        }

        private void btn_SaveContinue_Click(object sender, EventArgs e)
        {
            if (this.positionCount >= 4)
            {
                DialogResult res = MessageBoxHelper.ShowTrackedMessageBox("Are you sure you want to save and finalize this examination?", "Save New Examination", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (res == DialogResult.Yes)
                {
                    this.finalImages[this.positionCount-1] = outputImage;
                    outputImage = null;

                    //Insert in Database
                    DateTime now = DateTime.Now;
                    dbExamination NewExamination = new dbExamination(Globals.CurrentPatient.Id, Globals.UserID, now, textBox_Comment.Text);
                    DatabaseConnector.Insert(NewExamination);

                    for (int i = 1; i <= 4; i++)
                    {
                        string imageFileName = this.finalImages[i - 1];
                        dbImage newImage = new dbImage(NewExamination.Id, imageFileName, (ImagePositions)i);
                        DatabaseConnector.Insert(newImage);

                        // Copy image
                        string finalImage = Path.Combine(Globals.ImageFolder, imageFileName);
                        string tempImage = Path.Combine(Globals.TempFolder, imageFileName);
                        File.Copy(tempImage, finalImage);
                    }

                    this.Destroy();

                    //Succesfull New Examination --> Update ExaminationView
                    uc_ExaminationView np = new uc_ExaminationView();
                    np.Dock = DockStyle.Fill;
                    Form1.Instance.PnlContainer.Controls.Add(np);
                    Form1.Instance.PnlContainer.Controls["uc_ExaminationView"].BringToFront();
                    Form1.Instance.PnlContainer.Controls.RemoveByKey("uc_NewExamination");
                }    
            }
            else
            {
                DialogResult res = MessageBoxHelper.ShowTrackedMessageBox("Are you sure you want to continue? You will no longer be able to return to this position.", "Continue", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (res == DialogResult.Yes)
                {
                    this.finalImages[this.positionCount-1] = outputImage;
                    outputImage = null;

                    //Switch to next image capture profile
                    btn_ImageCapture.Text = "Image Capture";
                    btn_ImageCapture.Enabled = true;
                    btn_SaveContinue.Enabled = false;
                    this.positionCount += 1;
                    if (this.positionCount == 4) btn_SaveContinue.Text = "Save";
                    this.UpdateBackgroundImage(this.positionCount);
                    this.UpdatePositionLabel(this.positionCount);
                    pictureBox_NewExam.Image = null;
                }
            }
        }

        #endregion

        #region Respond To Events
        private void uc_NewExamination_Load(object sender, EventArgs e)
        {
            if (Globals.CameraController == null)
            {
                Form1.Instance.FindCameraDevice();
                if (Globals.CameraController == null)
                {
                    MessageBoxHelper.ShowTrackedMessageBox("Could not find camera. Check the connection and try again.", "Camera device error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Task.Delay(100).ContinueWith(t => ExitAndGoBack());
                    return;
                }
            }
            SetupCamera();
            Thread.Sleep(50);

            if (Globals.SerialController == null)
            {
                SerialFramework.SearchDone += SerialFramework_SearchDone;
                Form1.Instance.FindSerialDevice();
                MessageBoxHelper.ShowTrackedMessageBox("Searching for serial device. Wait or press ok and try again later.", "Searching", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Task.Delay(100).ContinueWith(t => ExitAndGoBack());
                return;
            }
            else
            {
                SetupSerial();
            }
        }

        private void SerialFramework_SearchDone(object sender, EventArgs e)
        {
            this.Invoke(new Action(() =>
            {
                MessageBoxHelper.CloseMessageBox("Searching");
                if (Globals.SerialController == null)
                {
                    MessageBoxHelper.ShowTrackedMessageBox("Could not find serial device. Check the connection and try again.", "Serial device error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Task.Delay(100).ContinueWith(t => ExitAndGoBack());
                }
                else
                {
                    MessageBoxHelper.ShowTrackedMessageBox("Serial device found.", "Serial device", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    SetupSerial();
                }
            }));
        }

        private void Camera_Download(object sender, DownloadEventArgs e)
        {
            if (e.Status == DownloadEventArgs.DownloadStatus.COMPLETED)
            {
                lock(bitmaps)
                {
                    Bitmap image = ImageProcessor.ScaleImage(e.Bitmap, 0.75f);
                    image.RotateFlip(RotateFlipType.Rotate90FlipNone);

                    // Save the image
                    string huginInputImage = Path.Combine(Globals.TempFolder, $"image{bitmaps.Count}.tif");
                    image.Save(huginInputImage);

                    // Keep dummy count, might be use full later, when stitching is integrated into the app itself.
                    bitmaps.Add(null);

                    image.Dispose();
                }
            }
        }

        private void Camera_PictureTaken(object sender, EventArgs e)
        {
            serialController.RequestAction(new ActionRequest(ActionRequest.Command.SERIAL_MOVE, ActionRequestArgs.Empty));
        }

        private void Serial_GetProperty(object sender, SerialDeviceEventArgs e)
        {
            this.BeginInvoke(new Action(() =>
            {
                this.imageCount = e.Message + 1;
            }));
        }

        private void Serial_MoveDone(object sender, EventArgs e)
        {
            cameraController.RequestAction(new ActionRequest(ActionRequest.Command.CAMERA_TAKE_PICTURE, ActionRequestArgs.Empty));
        }

        private void Serial_CaptureDone(object sender, EventArgs e)
        {
            double timer = 0;
            DateTime time1 = DateTime.Now;
            DateTime time2 = DateTime.Now;
            while (bitmaps.Count < imageCount)
            {
                Thread.Sleep(200);

                time2 = DateTime.Now;
                long deltaTime = (time2.Ticks - time1.Ticks);
                TimeSpan span = new TimeSpan(deltaTime);
                timer += span.TotalSeconds;

                if (downloadTimoutTimer < timer)
                {
                    MessageBoxHelper.ShowTrackedMessageBox("Image capture failed, try again.", "Image capture failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    this.Invoke(new Action(() =>
                    {
                        btn_ImageCapture.Enabled = true;
                        btn_SaveContinue.Enabled = false;
                        progressBar_NewExam.Value = 0;
                        this.bitmaps.Clear();
                    }));
                    return;
                }
                time1 = time2;
            }
            this.Invoke(new Action(() =>
            {
                DateTime now = DateTime.Now;
                int p = this.positionCount;
                string hahsedImageName = HashClass.HashModel(Globals.CurrentPatient.FullName + Enum.GetName(typeof(ImagePositions), p) + now.ToString("ddMMyyyyHHmmss"));
                outputImage = $"{hahsedImageName}.jpg";

                hugin.Exited += Hugin_Exited;
                hugin.StartStitching(Globals.TempFolder, outputImage);

                label_PositionInfo.Text = "Stitching in progress. Please wait.";

                this.bitmaps.Clear();
            }));
        }

        private void OnHuginExit(int errorCode, string output, string error)
        {
            if (errorCode > 0)
            {
                MessageBoxHelper.ShowTrackedMessageBox("Image stitching failed, try again.", "Image stitching failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                btn_ImageCapture.Enabled = true;
                btn_SaveContinue.Enabled = false;
                progressBar_NewExam.Value = 0;
                this.bitmaps.Clear();
            }
            else
            {
                label_PositionInfo.Text = "Stitching done.";

                string imagePath = Path.Combine(Globals.TempFolder, outputImage);
                Bitmap bm = (Bitmap)Bitmap.FromFile(imagePath);
                //if (positionCount == 1)
                /*bm.RotateFlip(RotateFlipType.Rotate270FlipNone);
                bm.Save(imagePath, ImageFormat.Jpeg);
                bm.RotateFlip(RotateFlipType.Rotate90FlipNone);*/

                btn_ImageCapture.Text = "Image Recapture";
                btn_ImageCapture.Enabled = true;
                btn_SaveContinue.Enabled = true;
                pictureBox_NewExam.Image = bm;
                progressBar_NewExam.Value = 0;
            }
        }

        private void Hugin_Exited(object sender, HuginExitedEventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new Action(() => OnHuginExit(e.ErrorCode, e.Output, e.Error)));
            }
            else
            {
                OnHuginExit(e.ErrorCode, e.Output, e.Error);
            }
        }

        private void Serial_ProgressReport(object sender, ProgressReportEventArgs e)
        {
            progressBar_NewExam.BeginInvoke(new Action(() => progressBar_NewExam.Value = e.Progress));
        }

        private void Serial_DeviceBusy(object sender, DeviceErrorEventArgs e)
        {
            this.BeginInvoke(new Action(() =>
            {
                MessageBoxHelper.CloseAllTrackedMessageBoxs();
                MessageBoxHelper.ShowTrackedMessageBox(e.Message + " Try again later.", "Serial warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                ExitAndGoBack();
            }));
        }

        private void Serial_Error(object sender, DeviceErrorEventArgs e)
        {
            this.BeginInvoke(new Action(() =>
            {
                MessageBoxHelper.CloseAllTrackedMessageBoxs();
                MessageBoxHelper.ShowTrackedMessageBox(e.Message, "Serial error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ExitAndGoBack();
            }));
        }

        private void Camera_Error(object sender, DeviceErrorEventArgs e)
        {
            this.BeginInvoke(new Action(() =>
            {
                MessageBoxHelper.CloseAllTrackedMessageBoxs();
                MessageBoxHelper.ShowTrackedMessageBox(e.Message, "Camera error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ExitAndGoBack();
            }));
        
        }

        #endregion

        #region Functions

        private void SetupCamera()
        {
            cameraController = Globals.CameraController;
            CameraDevice camera = (CameraDevice)cameraController.Device;
            
            camera.Download += Camera_Download;
            camera.PictureTaken += Camera_PictureTaken;
            camera.Error += Camera_Error;

            cameraController.Run();
        }

        private void SetupSerial()
        {
            serialController = Globals.SerialController;
            SerialDevice serial = (SerialDevice)serialController.Device;

            serial.MoveDone += Serial_MoveDone;
            serial.CaptureDone += Serial_CaptureDone;
            serial.ProgressReport += Serial_ProgressReport;
            serial.DeviceBusy += Serial_DeviceBusy;
            serial.GetProperty += Serial_GetProperty;
            serial.Error += Serial_Error;

            serialController.Run();

            serialController.RequestAction(
               new ActionRequest(ActionRequest.Command.SERIAL_GET_PROPERTY,
               new SerialActionRequestArgs((byte)SerialFramework.PropertyId.NUM_OF_POS)));
            serialController.RequestAction(new ActionRequest(ActionRequest.Command.SERIAL_CAPTURE_START, ActionRequestArgs.Empty));
            Thread.Sleep(100);
            btn_ImageCapture.Enabled = true;
        }

        public void Destroy()
        {
            if (cameraController != null)
            {
                CameraDevice camera = (CameraDevice)cameraController.Device;
                
                camera.Download -= Camera_Download;
                camera.PictureTaken -= Camera_PictureTaken;
                camera.Error -= Camera_Error;
                
                cameraController.RequestAction(new ActionRequest(ActionRequest.Command.CLOSING, ActionRequestArgs.Empty));
            }

            if (serialController != null)
            {
                SerialDevice serial = (SerialDevice)serialController.Device;

                serial.MoveDone -= Serial_MoveDone;
                serial.CaptureDone -= Serial_CaptureDone;
                serial.ProgressReport -= Serial_ProgressReport;
                serial.DeviceBusy -= Serial_DeviceBusy;
                serial.GetProperty -= Serial_GetProperty;

                serial.Error -= Serial_Error;

                serialController.RequestAction(new ActionRequest(ActionRequest.Command.SERIAL_CAPTURE_STOP, ActionRequestArgs.Empty));
                serialController.RequestAction(new ActionRequest(ActionRequest.Command.CLOSING, ActionRequestArgs.Empty));
            }

            hugin.StopStitching();
        }

        private void _ExitAndGoBack()
        {
            if (destroyed)
                return;

            this.Destroy();
            var ucs = Form1.Instance.PnlContainer.Controls;
            ucs.RemoveByKey("uc_NewExamination");
            Form1.Instance.SwitchToPreviusControl();
            MessageBoxHelper.CloseAllTrackedMessageBoxs();

            destroyed = true;
        }

        private void ExitAndGoBack()
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new Action(() => _ExitAndGoBack()));
            }
            else
            {
                _ExitAndGoBack();
            }
        }

        private void UpdateBackgroundImage(int i) // i = this.PositionCount
        {
            //Cases need to be in order with Values in enum ImagePosition
            switch (i)
            {
                case 1:
                    pictureBox_NewExam.BackgroundImage = Resources.front_profile;
                    break;
                case 2:
                    pictureBox_NewExam.BackgroundImage = Resources.left_profile;
                    break;
                case 3:
                    pictureBox_NewExam.BackgroundImage = Resources.back_profile;
                    break;
                case 4:
                    pictureBox_NewExam.BackgroundImage = Resources.right_profile;
                    break;
                default:
                    pictureBox_NewExam.BackgroundImage = null;
                    break;
            }
        }

        private void UpdatePositionLabel(int i) // i = this.PositionCount
        {
            //Cases need to be in order with Values in enum ImagePosition
            switch (i)
            {
                case 1:
                    label_Position.Text = Enum.GetName(typeof(ImagePositions), 1);
                    label_PositionInfo.Text = "Position the patient so that he is facing the camere, as is demonstrated on the image.";
                    break;
                case 2:
                    label_Position.Text = Enum.GetName(typeof(ImagePositions), 2);
                    label_PositionInfo.Text = "The patient should turn in a clockwise direction, so that he is facing left, as is demonstrated on the image.";
                    break;
                case 3:
                    label_Position.Text = Enum.GetName(typeof(ImagePositions), 3);
                    label_PositionInfo.Text = "The patient should turn in a clockwise direction, so that he is now facing away from the camera, as is demonstrated on the image.";
                    break;
                case 4:
                    label_Position.Text = Enum.GetName(typeof(ImagePositions), 4);
                    label_PositionInfo.Text = "The patient should turn in a clockwise direction one more time, so that he is facing right, as is demonstrated on the image.";
                    break;
                default:
                    label_Position.Text = string.Empty;
                    label_PositionInfo.Text = string.Empty;
                    break;
            }
        }

        #endregion

        
    }
}
